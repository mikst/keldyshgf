import numpy as np

"""
The fourier transform we define by::

   F(w) = int dt exp(iwt) * f(t)

and the inverse by::

   f(t) = 1 / (2 * pi) int dw exp(-iwt) * F(w)
"""


def fourierintegral(f, delta, sign=1, data=None):
    """ The fourier transform of a function represented by the array
    f. len(f) does not have to be an integer power of two!
    NOTE: Assumes that the fouriertransform is along axis 0.
    WARNING: this methods leads to a kink in the transform in the middle of
             the data range! It should only be used if the data is
             oversampled (zero-padded) by at least a factor 2.

    ``delta`` is the increment of the tranform axis.

    sign=1 indicates an inverse fft ( return F(w) from f(t) )
    sign=-1 indicates a forward fft ( return f(t) from F(w) )
    """
    N = len(f)
    if data is None:
        data = GetFourierData(N)
    W, cor = data
    shift = np.dot(cor, np.hstack((f[:4], f[-1:-5:-1])))

    if sign == 1:  
        F = W * N * np.fft.ifft(f, N, axis=0) + shift
        F *= delta
        # Without end-point corrections, equivalent to:
        # np.fft.ifft(f, axis=0) * delta * N
    else:
        F = W * np.fft.fft(f, N, axis=0) + shift
        F *= delta / (2 * np.pi)
        # Without end-point corrections, equivalent to:
        # np.fft.fft(f, axis=0) * delta / (2 * np.pi)
    return F


def GetFourierData(N):
    theta = np.arange(-N/2, N/2) * 2 * np.pi / N
    W, cor = dftcor(theta)    #end point corrections
    # FFTshift end point corrections
    return np.fft.ifftshift(W, axes=[0]), np.fft.ifftshift(cor, axes=[0])


def dftcor(theta):
    """ End point corrections for FFT
    
    Numerical Recipies in C, Chap. 13.9
    Formula 13.9.14
    """
    theta_small = 0.01
    N = len(theta)
    cor = np.zeros((N,8),complex)

    mask_s = abs(theta) < theta_small
    mask_b = abs(theta) >= theta_small
    
    tb = theta.copy()
    tb[len(tb) / 2] = 1 # Avoid division by zero!
    ts2 = theta**2 ; ts3 = theta**3 ; ts4 = theta**4 ; ts6 = theta**6 
    tb4 = tb**4

    costb = np.cos(theta) ; cos2tb = np.cos(2 * theta)
    sintb = np.sin(theta) ; sin2tb = np.sin(2 * theta)

     
    W = (1 - 11./720*ts4 + 23./15120*ts6) * mask_s + \
        (((6 + ts2) / (3 * tb4)) * (3 - 4 * costb + cos2tb)) * mask_b
    
    cor[:,0] = (-2./3 + 1./45*ts2 + 103./15120*ts4 - 169./226800*ts6 + 
                1j*theta*(2./45 + 2./105*ts2 - 8./2835*ts4 + 86./ 467775*ts6
                          ))*mask_s
    cor[:,1] = (7./24 - 7./180*ts2 + 5./3456*ts4 - 7./259200*ts6 +
                1j*theta*(7./72 - 1./168*ts2 + 11./72576*ts4 - 13./5987520*ts6
                          ))*mask_s
    cor[:,2] = (-1./6 + 1./45*ts2 - 5./6048*ts4 + 1./64800*ts6 +
                1j*theta*(-7./90 + 1./210*ts2 - 11./90720*ts4 + 13./7484400*ts6
                          ))*mask_s
    cor[:,3] = (1./24 - 1./180*ts2 + 5./24192*ts4 - 1./259200*ts6 +
                1j*theta*(7./360 - 1./840*ts2 + 11./362880*ts4-13./29937600*ts6
                          ))*mask_s
    
    cor[:,0] += ((-42 + 5*ts2 + (6+ts2)*(8*costb - cos2tb))/(6*tb4) +
                 1j*(-12*theta + 6*ts3 + sin2tb*(6 + ts2))/(6*tb4))*mask_b
   
    cor[:,1] += ((14*(3-ts2) - 7*(6+ts2)*costb)/(6*tb4) +
                 1j*(30*theta - 5*(6+ts2)*sintb)/(6*tb4))*mask_b
    cor[:,2] += ((-4*(3-ts2) + 2*(6+ts2)*costb)/(3*tb4) +
                 1j*(-12*theta + 2*(6+ts2)*sintb)/(3*tb4))*mask_b
    cor[:,3] += ((2*(3-ts2) - (6+ts2)*costb)/(6*tb4) +
                 1j*(6*theta - (6+ts2)*sintb)/(6*tb4))*mask_b

    cor[:, 4:] = cor[:, :4].conj()
   
    return W, cor


if __name__ == '__main__':
    # Run a self-test, also serving as an example.
    import pylab as pl

    def gauss(x, x0=0.0, width=2.5):
        return np.exp(-.5 * (x - x0)**2 / width**2) / (
            width * np.sqrt(2 * np.pi))


    def lorentz(x, x0=0.0, width=2.5):
        return width / ((x - x0)**2 + width**2) / np.pi

    x = np.linspace(-8, 15, 2**10)
    de, N = x[1] - x[0], len(x)
    Next = N * 2 # oversample

    # Make Lorentzian and FFT to time
    eta = 0.2
    y = lorentz(x, width=eta)
    fy = np.fft.fft(y, n=Next)
    print 'Integral of Lorentzian', np.trapz(y, x)

    # Make retarded
    r = fy.copy()
    r[len(r) // 2:] = 0.0

    # inverse FFT back to energy
    a1 = 2 * np.fft.ifft(r)[:N]
    a2 = 2 * fourierintegral(r, de, sign=1)[:N] / (Next * de)

    fig = pl.figure(1)
    pl.subplot(211)
    pl.plot(x, y.real, label='Lorentz')
    pl.plot(x, a1.real, label='Real of ret np.ifft')
    pl.plot(x, a1.imag, label='Imag of ret np.ifft')
    pl.plot(x, a2.real, label='Real of ret fourierint')
    pl.plot(x, a2.imag, label='Imag of ret fourierint')
    pl.title('Energy space')
    pl.xlabel('Energy')
    pl.axis('tight')
    pl.legend()

    pl.subplot(212)
    pl.plot(fy.real, label='Real of np.fft')
    pl.plot(fy.imag, label='Imag of np.fft')
    pl.title('Time space')
    pl.xlabel('Time')
    pl.legend()
    pl.axis('tight')

    pl.show()
