import numpy as np
import pylab as pl
from Transport.Hilbert import hilbert


def Box(a, b, energy, T):
    if T == 0:
        return np.where((a < energy) & (energy < b), 1., 0.)
    return .5 * (np.tanh(.5 * (energy - a) / T) -
                 np.tanh(.5 * (energy - b) / T))


def se_box(a, b, energy, T=0.3):
    """Box shaped Butinger probe.
    
    Kramers-kroning: real = H(imag); imag = -H(real)
    """
    se = np.empty(len(energy), complex)
    se.imag = .5 * (np.tanh(.5 * (energy - a) / T) -
                    np.tanh(.5 * (energy - b) / T))
    se.real = hilbert(se.imag)
    se.imag -= 1
    return se

energy = np.linspace(-16, 21, 300)
se = se_box(-5, 10, energy, T=0.5)

pl.figure(1, figsize=(6, 4.5))
pl.plot(energy, se.imag, lw=2, label='imag')
pl.plot(energy, se.real, lw=2, label='real')
pl.axis('tight')
pl.legend(loc='best')
pl.savefig('box_selfenergy.png')
pl.title('Box shaped Buttinger probe')
pl.xlabel('energy')
pl.show()


#analytic_hilbert_of_box_at_T0 = -np.log(abs((x - a) / (x - b))) / np.pi
