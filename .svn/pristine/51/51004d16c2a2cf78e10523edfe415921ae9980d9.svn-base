#!/usr/bin/env python

import Numeric as num
import pylab as pl
from ASE.Transport.LOE import LOE

class OneLevel:
    def __init__(self, e0, gamma_left, alpha):
        self.e0 = e0
        self.sigma_left = -.5j * gamma_left
        self.sigma_right = alpha * self.sigma_left
        self.alpha = alpha
        self.e = 0.0

    def SetEnergy(self, e):
        self.e = e

    def GetRepresentation(self):
        G = 1 / (self.e - self.e0 - self.sigma_left - self.sigma_right)
        return num.array([[G]], num.Complex)

    def GetLeftLeadSelfEnergy(self):
        return num.array([[self.sigma_left]], num.Complex)
        
    def GetRightLeadSelfEnergy(self):
        return num.array([[self.sigma_right]], num.Complex)
        
def model(a    = 1.,
          Gam1 = .5,
          e0   = -1.0,
          T    = 100 * 8.62e-5,
          w    = 200e-3,
          M    = .2):
    energy = num.arange(-.7, .7, .02)
    bias = num.arange(-2*w, 2*w, .01 * w)
    
    loe = LOE(scatteringgreenfunction=OneLevel(e0, Gam1, a),
          temperature=T,
          phononcoupling=[num.array([[M]], num.Complex),],
          frequencies=[w,],
          energyrange=energy,
          fermienergy=0.0)
    dG = loe.DifferentialConductance(bias)

    pl.plot(bias * 1e3, dG)
    pl.axis('tight')
    pl.xlabel('Bias voltage [meV]')
    pl.ylabel('Differential conductance [G0]')
    pl.show()


if __name__ == '__main__':
    model(a    = 10.,
          Gam1 = .25,
          e0   = -1.0,
          T    = 100 * 8.62e-5,
          w    = 200e-3,
          M    = .2)
