import numpy as np
import pickle
from ase.build import molecule
from gpaw.lcao.tools import makeU, makeV
from gpaw import GPAW, FermiDirac, restart
from gpaw.lcao.pwf2 import LCAOwrap
from gpaw.mpi import world, rank, MASTER, serial_comm
from gpaw.test import equal

# DFT calculation
scat = range(2)
atoms = molecule('H2')
atoms.set_cell([6.4, 6.4, 6.4])
atoms.center()
calc = GPAW(mode='lcao',
            #basis='dz(dzp)',
            occupations=FermiDirac(0.1), 
            txt='lcao.txt',
            )
atoms.set_calculator(calc)
atoms.get_potential_energy()
calc.write('lcao.gpw')


# Read DFT calculation in and write out:
# KS hamiltonian (H)
# Overlap matrix (S)
# KS XC potential
# Valecence-core exchange (F_core)
# LCAO orbitals on a grid
# PAW stuff
if rank == MASTER:
    atoms, calc = restart('lcao.gpw',
                          txt=None, communicator=serial_comm)
    lcao = LCAOwrap(calc)
    fermi = calc.get_fermi_level()
    H = lcao.get_hamiltonian()
    S = lcao.get_overlap()
    pickle.dump((H, S), open('lcao_hs.pckl', 'wb'), 2)
    symbols = atoms.get_chemical_symbols()
    # indices = get_bfi2(symbols, basis, scat)
    indices = range(len(H))  # only use the 1s orbitals
    lcao.get_xc(indices=indices).dump('lcao_xc.pckl')
    lcao.get_Fcore(indices=indices).dump('lcao_Fcore.pckl')
    w_wG = lcao.get_orbitals(indices=indices)
    P_awi = lcao.get_projections(indices=indices)
    pickle.dump((w_wG, P_awi), open('lcao_w_wG__P_awi.pckl', 'wb'), 2)

world.barrier()
# Make the rotation (U) between the full and the
# compressed representation of the Coulomb interaction
makeU('lcao.gpw',
      'lcao_w_wG__P_awi.pckl',
      'lcao_eps_q__U_pq.pckl',
      1.0e-5,
      dppname='lcao_D_pp.pckl')
world.barrier()

# Calculate the Coulomb interaction in the compressed representation
makeV('lcao.gpw',
      'lcao_w_wG__P_awi.pckl',
      'lcao_eps_q__U_pq.pckl',
      'lcao_V_qq.pckl',
      'lcao_V_qq.log',
      False)

world.barrier()

# test that shows how to go from the compressed to the full
# coulomb interaction
if len(H) == 2:
    V_qq = np.load('lcao_V_qq.pckl')
    eps_q, U_pq = np.load('lcao_eps_q__U_pq.pckl')
    assert U_pq.flags.contiguous
    Usq_pq = U_pq * np.sqrt(eps_q)
    V_pp = np.dot(np.dot(Usq_pq, V_qq), Usq_pq.T.conj())
    V_pp_ref = np.array(
        [[15.34450177,  11.12669608,  11.12669608,  12.82934563],
         [11.12669608,   8.82280293,   8.82280293,  11.12669608],
         [11.12669608,   8.82280293,   8.82280293,  11.12669608],
         [12.82934563,  11.12669608,  11.12669608,  15.34450178]])
    equal(abs(V_pp_ref-V_pp).max(), 0.0, 1.0e-5)
