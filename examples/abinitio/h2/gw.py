import numpy as np
from kgf.GreenFunctions import NonEqNonIntGreenFunction, NonEqIntGreenFunction
from kgf.ScatteringHamiltonianMatrix import ScatteringHamiltonianMatrix
from kgf.LeadHamiltonianMatrix import LeadHamiltonianMatrix
from kgf.selfenergies import NonEqConstantSelfEnergy
from kgf.selfenergies.HFSelfEnergy import Hartree, Fock
from kgf.selfenergies.GWSelfEnergy import NonEqGWSelfEnergy
from gpaw import GPAW


fermi_level = GPAW('lcao.gpw', txt=None).get_fermi_level()
# Load matrices
H, S = np.load('lcao_hs.pckl')
xc = np.load('lcao_xc.pckl')
Fcore = np.load('lcao_Fcore.pckl')
V_qq = np.load('lcao_V_qq.pckl').astype(complex)
eps_q, U_pq = np.load('lcao_eps_q__U_pq.pckl')
Usq_pq = (U_pq * np.sqrt(eps_q)).astype(complex)
del U_pq

# Setup scattering- and lead Hamiltonians
hmat = ScatteringHamiltonianMatrix(leftprincipallayer=0,
                                   rightprincipallayer=0,
                                   hamiltonianmatrix=H,
                                   overlapmatrix=S)
left = right = LeadHamiltonianMatrix(principallayer=0,
                                     hamiltonianmatrix=np.zeros((2, 2)))

# Setup noninteracting Green function
energies = np.arange(-100, 100, 0.05)
g0 = NonEqNonIntGreenFunction(hmat, left, right, E_Fermi=fermi_level,
                              energies=energies, uncoupled=True)
g0.SetInfinitesimal(0.1) # Should be at least 2 * de
print ('Total electron number:', g0.GetTotalParticleNumber())

# Setup self energies
S_null = NonEqConstantSelfEnergy(g0, 0.0 * xc, 'null')
S_xc = NonEqConstantSelfEnergy(g0, -xc, 'xc')
S_Hartree = Hartree(g0, V_qq, Usq_pq, initialhartree=None)
S_Fock = Fock(g0, V_qq, Usq_pq, Fcore=Fcore)
S_gw = NonEqGWSelfEnergy(g0, V_qq, Usq_pq, oversample=10)

# pulay parameters: tolerance, linear mixing, history
pulay = (0.05, 0.3, 1)

# DFT
gf = NonEqIntGreenFunction([S_xc, S_Hartree, S_Fock, S_gw, S_null])
for s in [S_xc, S_Hartree, S_Fock, S_gw]:
    s.SetActive(False)

gf.WriteSpectralInfoToNetCDFFile('DFT.nc', diagonalize=True)

# SC Hartree Fock
for s in [S_xc, S_Hartree, S_Fock]:
    s.SetActive(True)

gf.SelfConsistent(log='HF.log',
                  pulay=pulay,
                  )
gf.WriteSpectralInfoToNetCDFFile('HF.nc', diagonalize=True)

# SC GW
S_gw.SetActive(True)
gf.SelfConsistent(log='GW.log',
                  pulay=pulay,
                  )
gf.WriteSpectralInfoToNetCDFFile('GW.nc', diagonalize=True)
