from __future__ import print_function
from ase.build import molecule
from gpaw import GPAW

atoms = molecule('H2O')
atoms.center(vacuum=4.)
calc = GPAW(xc='PBE', mode='lcao', basis='dzp', txt='lcao.txt')
atoms.set_calculator(calc)
atoms.get_potential_energy()
calc.write('lcao.gpw', mode='all')
print (calc.get_homo_lumo().mean(), file=open('fermilevel.txt', 'w'))
