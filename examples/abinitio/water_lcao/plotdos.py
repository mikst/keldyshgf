import matplotlib.pyplot as plt
from scipy.io.netcdf import netcdf_file
import numpy as np


def get_dos(fname):
    with netcdf_file(fname, 'r') as nc:
        var = nc.variables
        energies = var['Energies'][:].copy()
        dos = var['tot_dos'][:].copy()
        del var
    return energies, dos


def plot(e, dos, label, ymax):
    plt.plot(e, dos, label=label)
    plt.xlim(-20, 10)
    plt.ylim(0, ymax)
    plt.plot((ef, ef), (0, ymax), 'k--')
    plt.legend()


ef = np.loadtxt('fermilevel.txt')
e1, e2 = -20, 10
e, dos_dft = get_dos('DFT.nc')
i1, i2 = abs(e - e1).argmin(), abs(e - e2).argmin()
ymax = dos_dft[i1: i2].max()
e, dos_hf = get_dos('HF.nc')
#e, dos_gw = get_dos('GW.nc')
plt.subplot(311)
plot(e, dos_dft, 'dft', ymax)
plt.subplot(312)
plot(e, dos_hf, 'hf', ymax)
plt.subplot(313)
#plot(e, dos_gw, 'gw', ymax)
plt.savefig('dos.png')
plt.show()
