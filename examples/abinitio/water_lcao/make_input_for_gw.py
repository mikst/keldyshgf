import numpy as np
import pickle
from ase.build import molecule
from gpaw.lcao.tools import makeU, makeV
from gpaw import GPAW, FermiDirac, restart
from gpaw.lcao.pwf2 import LCAOwrap
from gpaw.mpi import world, rank, MASTER, serial_comm
from gpaw.test import equal

if rank == MASTER:
    atoms, calc = restart('lcao.gpw',
                          txt=None, communicator=serial_comm)
    lcao = LCAOwrap(calc)
    fermi = calc.get_fermi_level()
    H = lcao.get_hamiltonian()
    S = lcao.get_overlap()
    pickle.dump((H, S), open('lcao_hs.pckl', 'wb'), 2)
    symbols = atoms.get_chemical_symbols()
    indices = range(len(H))
    lcao.get_xc(indices=indices).dump('lcao_xc.pckl')
    lcao.get_Fcore(indices=indices).dump('lcao_Fcore.pckl')
    w_wG = lcao.get_orbitals(indices=indices)
    P_awi = lcao.get_projections(indices=indices)
    pickle.dump((w_wG, P_awi), open('lcao_w_wG__P_awi.pckl', 'wb'), 2)

world.barrier()
# Make the rotation (U) between the full and the
# compressed representation of the Coulomb interaction
makeU('lcao.gpw',
      'lcao_w_wG__P_awi.pckl',
      'lcao_eps_q__U_pq.pckl',
      1.0e-5,)
world.barrier()

# Calculate the Coulomb interaction in the compressed representation
makeV('lcao.gpw',
      'lcao_w_wG__P_awi.pckl',
      'lcao_eps_q__U_pq.pckl',
      'lcao_V_qq.pckl',
      'lcao_V_qq.log',
      False)

world.barrier()

