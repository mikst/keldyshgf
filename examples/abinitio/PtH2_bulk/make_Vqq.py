#PBS -N Vqq_Pt
#PBS -m abe
#PBS -l nodes=3:ppn=8
#PBS -l walltime=14:00:00

from kgf.gpaw_tools import makeV

makeV(gpwfile='lcao_scat_szpsz.gpw',
      orbitalfile='w_wG__P_awi.pckl',
      rotationfile='eps_q__U_pq.pckl',
      coulombfile='V_qq.pckl',
      log='make_Vqq.txt',
      fft=True)
