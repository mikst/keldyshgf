#PBS -N lcao_Pt_princ
#PBS -l nodes=2:ppn=8
#PBS -l walltime=01:40:00
#PBS -m abe

from ase import *
from gpaw import *
import cPickle as pickle
from gpaw.lcao.tools import remove_pbc, get_lead_lcao_hamiltonian
from gpaw.mpi import rank, MASTER

atoms = read('principal.traj')
calc = GPAW(nbands=400,
            h=.19,
            usesymm=False,
            xc='PBE',
            txt='lcao_princ.txt',
            basis='de0.1.szp',
            stencils=(3, 3),
            mode='lcao',
            poissonsolver=PoissonSolver(nn='M', relax='GS'),
            width=.1,
            mixer=Mixer(0.1, 5, weight=80.),
            kpts=(6, 1, 1))
atoms.set_calculator(calc)
atoms.get_potential_energy()
fermi = calc.get_fermi_level()
print >> open('fermi_princ.txt', 'w'), repr(fermi)
calc.write('lcao_princ.gpw')

tkpts_kc, tweights_k, H_skMM, S_kMM = get_lead_lcao_hamiltonian(
    calc, direction='x')
if rank == MASTER:
    S = S_kMM[0]
    H = H_skMM[0, 0] - fermi * S
    remove_pbc(atoms, H, S, d=0)
    pickle.dump((H, S), open('hs_princ_lcao_szp.pckl', 'wb'), 2)
