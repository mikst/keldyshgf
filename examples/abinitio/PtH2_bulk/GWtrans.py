#!/usr/bin/env python
#PBS -N GWtrans_pt
#PBS -m abe
#PBS -l nodes=12:ppn=8
#PBS -l walltime=50:00:00

import numpy as np
import cPickle as pickle
from kgf.GreenFunctions import NonEqNonIntGreenFunction, NonEqIntGreenFunction
from kgf.ScatteringHamiltonianMatrix import ScatteringHamiltonianMatrix
from kgf.LeadHamiltonianMatrix import LeadHamiltonianMatrix
from kgf.selfenergies import NonEqConstantSelfEnergy
from kgf.selfenergies.FilterSelfEnergy import FilterXCCenter,FilterZeroCenter
from kgf.selfenergies.HFSelfEnergy import Hartree, Fock
from kgf.selfenergies.GWSelfEnergy import NonEqGWSelfEnergy

energies = np.arange(-150., 150., 0.01)
H, S = pickle.load(open('hs_lcao_scat_szpsz_ase2.pckl'))
H1, S1 = pickle.load(open('hs_princ_lcao_szp.pckl'))
Fcore = np.load('Fcore.pckl')
xc = np.load('xc.pckl')
V_qq = np.load('V_qq.pckl').astype(complex)
eps_q, U_pq = np.load('eps_q__U_pq.pckl')
Usq_pq = (U_pq * np.sqrt(eps_q)).astype(complex)
del U_pq

prin = H1.shape[-1] // 2
hmat = ScatteringHamiltonianMatrix(leftprincipallayer=prin,
                                   rightprincipallayer=prin,
                                   hamiltonianmatrix=H,
                                   overlapmatrix=S)
intern = ScatteringHamiltonianMatrix(leftprincipallayer=prin + 4*16*9,  # XXX
                                     rightprincipallayer=prin + 4*16*9, # XXX
                                     hamiltonianmatrix=H,
                                     overlapmatrix=S)
left = right = LeadHamiltonianMatrix(principallayer=prin,
                                     hamiltonianmatrix=H1,
                                     overlapmatrix=S1)

g0 = NonEqNonIntGreenFunction(hmat, left, right, E_Fermi=0., energies=energies,
                              internalscatteringhamiltonian=intern)
g0.SetInfinitesimal(0.02)
g0.SetLeadInfinitesimal(0.1)
print g0.GetTotalParticleNumber()

se_null    = NonEqConstantSelfEnergy(g0, np.zeros_like(xc), 'null')
se_xc      = NonEqConstantSelfEnergy(g0, -xc, 'xc')
se_hartree = FilterZeroCenter(
    Hartree(g0, V_qq, Usq_pq, initialhartree=None), 4 * 6) # XXX
se_fock    = FilterXCCenter(
    Fock(g0, V_qq, Usq_pq, Fcore=Fcore), xc, 4 * 6) # XXX
se_gw = FilterZeroCenter(
    NonEqGWSelfEnergy(g0, V_qq, Usq_pq, oversample=10), 4*6)

pulay = [0.05, 0.3, 1]
orbitals = range(4 * 6, 4 * 6 + 8) # XXX

dft = NonEqIntGreenFunction([se_null])
dft.WriteSpectralInfoToNetCDFFile(filename='DFT.nc', orbitals=orbitals,
                                  diagonalize=True, spectral='summed')

gf = NonEqIntGreenFunction([se_xc, se_hartree, se_fock])
gf.SelfConsistent(pulay, log='HF.log', doslog=dict(
    filename='HF_%i.nc', diagonalize=True,
    spectral='summed', orbitals=orbitals))

gf = NonEqIntGreenFunction([se_xc, se_hartree, se_fock, se_gw])
gf.SelfConsistent(pulay, log='GW.log', doslog=dict(
    filename='GW_%i.nc', diagonalize=True,
    spectral='summed', orbitals=orbitals))
