#!/usr/bin/env python
#PBS -m abe
#PBS -l nodes=1:ppn=8
#PBS -l walltime=00:25:00
#PBS -N pairorb_Pt

from kgf.gpaw_tools import makeU

makeU(gpwfile='lcao_scat_szpsz.gpw',
      orbitalfile='w_wG__P_awi.pckl',
      rotationfile='eps_q__U_pq.pckl',
      tolerance=1e-5)
