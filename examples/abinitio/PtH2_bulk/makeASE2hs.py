import cPickle as pickle
import numpy as np

H3, S3 = pickle.load(open('hs_lcao_scat_szpsz.pckl'))
Hl3, Sl3 = pickle.load(open('hs_princ_lcao_szp.pckl'))

pl = Hl3.shape[-1] // 2

# align bfs on orbital 1 (d-orb)
bf = 1
diff = (H3[bf, bf] - Hl3[bf, bf]) / S3[bf, bf]
print '# Aligning scat. H to left lead H. diff=', diff, 'eV'
H3 -= diff * S3

H2 = np.zeros((2 * pl + len(H3), 2 * pl + len(H3)), H3.dtype)
S2 = np.zeros((2 * pl + len(H3), 2 * pl + len(H3)), H3.dtype)

H2[:2 * pl, :2 * pl] = Hl3
H2[-2 * pl:, -2 * pl:] = Hl3
H2[pl:-pl, pl:-pl] = H3

S2[:2 * pl, :2 * pl] = Sl3
S2[-2 * pl:, -2 * pl:] = Sl3
S2[pl:-pl, pl:-pl] = S3

pickle.dump((H2, S2), open('hs_lcao_scat_szpsz_ase2.pckl', 'wb'), 2)
