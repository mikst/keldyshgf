#PBS -N Pt_lcao_scat_szpsz
#PBS -l nodes=1:ppn=8
#PBS -l walltime=05:00:00
#PBS -m abe

from ase import *
from gpaw import *
import cPickle as pickle
from gpaw.lcao.pwf2 import LCAOwrap
from gpaw.lcao.tools import remove_pbc, get_bfi2, get_bf_centers
from gpaw.mpi import rank, MASTER, serial_comm

scat = range(64, 74) # The scattering region
basis = {None: 'de0.1.szp'}
for a in scat:
    if a not in [68, 69]:
        basis[a] = 'de0.1.sz'

if 1:
    atoms = read('scattering.traj')
    calc = GPAW(nbands=1000,
                h=.19,
                xc='PBE',
                txt='lcao_scat_szpsz.txt',
                width=.12,
                parsize=16,
                usesymm=False,
                basis=basis,
                mode='lcao',
                poissonsolver=PoissonSolver(nn='M', relax='GS'),
                mixer=Mixer(0.05, nmaxold=5, weight=70.),
                kpts=(1, 1, 1),
                )
    atoms.set_calculator(calc)
    atoms.get_potential_energy()
    calc.write('lcao_scat_szpsz.gpw')

if rank == MASTER:
    atoms, calc = restart('lcao_scat_szpsz.gpw',
                          txt=None, communicator=serial_comm)
    lcao = LCAOwrap(calc)
    fermi = calc.get_fermi_level()
    print >> open('fermi_scat_lcao_szpsz.txt', 'w'), repr(fermi)
    H = lcao.get_hamiltonian()
    S = lcao.get_overlap()
    H -= fermi * S
    centers = None#get_bf_centers(atoms, basis)
    remove_pbc(atoms, H, S, d=0, centers_ic=centers, cutoff=None)
    pickle.dump((H, S), open('hs_lcao_scat_szpsz.pckl', 'wb'), 2)

    symbols = atoms.get_chemical_symbols()
    indices = get_bfi2(symbols, basis, scat)
    lcao.get_xc(indices=indices).dump('xc.pckl')
    lcao.get_Fcore(indices=indices).dump('Fcore.pckl')
    w_wG = lcao.get_orbitals(indices=indices)
    P_awi = lcao.get_projections(indices=indices)
    pickle.dump((w_wG, P_awi), open('w_wG__P_awi.pckl', 'wb'), 2)
