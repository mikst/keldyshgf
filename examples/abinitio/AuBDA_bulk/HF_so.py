#!/usr/bin/env python
#PBS -N HF_so
#PBS -m abe
#PBS -l nodes=4:ppn=8
#PBS -l walltime=02:00:00

import numpy as np
import cPickle as pickle
from kgf.GreenFunctions import NonEqNonIntGreenFunction, NonEqIntGreenFunction
from kgf.ScatteringHamiltonianMatrix import ScatteringHamiltonianMatrix
from kgf.LeadHamiltonianMatrix import LeadHamiltonianMatrix
from kgf.selfenergies import NonEqConstantSelfEnergy
from kgf.selfenergies.FilterSelfEnergy import FilterXCCenter, FilterZeroCenter
from kgf.selfenergies.HFSelfEnergy import Hartree, Fock
from kgf.Tools import dots

energies = np.arange(-90., 100., 0.025)
H, S = pickle.load(open('hs_scat_szpsz_ase2.pckl'))
H1, S1 = pickle.load(open('hs_princ_szp.pckl'))
Fcore = np.load('Fcore_sz.pckl')
xc = np.load('xc_sz.pckl')
V_qq = np.load('V_qq_sz.pckl').astype(complex)
eps_q, U_pq = np.load('eps_q__U_pq_sz.pckl')
Usq_pq = (U_pq * np.sqrt(eps_q)).astype(complex)
del U_pq

i = 7*16*9+4*6
h = H[i:i+40, i:i+40]
s = S[i:i+40, i:i+40]
eigs, rot = np.linalg.eig(np.linalg.solve(s, h))
rot /= np.sqrt(dots(rot.T.conj(), s, rot).diagonal())

rotocc = rot[:, eigs.real.argsort()[:21]]
soocc = np.zeros_like(xc)
soocc[24:-24, 24:-24] = -2.0 * dots(s, rotocc, rotocc.T.conj(), s)

rotall = rot[:, eigs.real.argsort()]
soall = np.zeros_like(xc)
soall[24:-24, 24:-24] = -2.0 * s

prin = H1.shape[-1] // 2
hmat = ScatteringHamiltonianMatrix(leftprincipallayer=prin,
                                   rightprincipallayer=prin,
                                   hamiltonianmatrix=H,
                                   overlapmatrix=S)
intern = ScatteringHamiltonianMatrix(leftprincipallayer=prin + 4*16*9,
                                     rightprincipallayer=prin + 4*16*9,
                                     hamiltonianmatrix=H,
                                     overlapmatrix=S)
left = right = LeadHamiltonianMatrix(principallayer=prin,
                                     hamiltonianmatrix=H1,
                                     overlapmatrix=S1)

g0 = NonEqNonIntGreenFunction(hmat, left, right, E_Fermi=0., energies=energies,
                              internalscatteringhamiltonian=intern)
g0.SetInfinitesimal(0.05)
g0.SetLeadInfinitesimal(0.1)
print g0.GetTotalParticleNumber()

se_null    = NonEqConstantSelfEnergy(g0, np.zeros_like(xc), 'null')
se_xc      = NonEqConstantSelfEnergy(g0, -xc, 'xc')
se_xcsoocc = NonEqConstantSelfEnergy(g0, soocc - xc, 'xc')
se_xcsoall = NonEqConstantSelfEnergy(g0, soall - xc, 'xc')
se_hartree = FilterZeroCenter(Hartree(g0, V_qq, Usq_pq, initialhartree=None),
                              4 * 6)
se_fock    = FilterXCCenter(Fock(g0, V_qq, Usq_pq, Fcore=Fcore), xc, 4 * 6)

pulay = [0.05, 0.4, 1]
orbitals = range(4 * 6, 4 * 6 + 40)

## dft = NonEqIntGreenFunction([se_null])
## dft.WriteSpectralInfoToNetCDFFile(filename='DFT_noso.nc', orbitals=orbitals,
##                                  diagonalize=True, spectral='summed')

## gf = NonEqIntGreenFunction([se_xcsoocc, se_hartree])
## gf.SelfConsistent(pulay, log='DFT_so_occ.log', doslog=dict(
##     filename='DFT_so_occ_%i.nc', diagonalize=True,
##     spectral='summed', orbitals=orbitals))

## gf = NonEqIntGreenFunction([se_xcsoall, se_hartree])
## gf.SelfConsistent(pulay, log='DFT_so_all.log', doslog=dict(
##     filename='DFT_so_all_%i.nc', diagonalize=True,
##     spectral='summed', orbitals=orbitals))

gf = NonEqIntGreenFunction([se_xc, se_hartree, se_fock])
gf.SelfConsistent(pulay, log='HF_noso.log', doslog=dict(
    filename='HF_noso_%i.nc', diagonalize=True,
    spectral='summed', orbitals=orbitals))

gf = NonEqIntGreenFunction([se_xcsoocc, se_hartree, se_fock])
gf.SelfConsistent(pulay, log='HF_so_occ.log', doslog=dict(
    filename='HF_so_occ_%i.nc', diagonalize=True,
    spectral='summed', orbitals=orbitals))

gf = NonEqIntGreenFunction([se_xcsoall, se_hartree, se_fock])
gf.SelfConsistent(pulay, log='HF_so_all.log', doslog=dict(
    filename='HF_so_all_%i.nc', diagonalize=True,
    spectral='summed', orbitals=orbitals))
