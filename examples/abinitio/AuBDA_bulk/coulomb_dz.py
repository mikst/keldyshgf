#PBS -N Vqq_dz
#PBS -m abe
#PBS -l nodes=6:ppn=8
#PBS -l walltime=50:00:00
from kgf.gpaw_tools import makeV

makeV(gpwfile='lcao_scat_dzp_relax.gpw',
      orbitalfile='w_wG__P_awi_dz.pckl',
      rotationfile='eps_q__U_pq_dz.pckl',
      coulombfile='V_qq_dz.pckl',
      log='coulomb_dz.txt',
      fft=True)
