#PBS -N Au_lcao_scat_hs
#PBS -l nodes=1
#PBS -l walltime=00:10:00
#PBS -m abe

from ase import *
from gpaw import *
import cPickle as pickle
from gpaw.lcao.pwf2 import LCAOwrap
from gpaw.lcao.tools import remove_pbc, get_bfi2, get_bf_centers

scat = range(64, 88)
basis = {None: 'de0.1.szp'}
for a in scat:
    basis[a] = 'de0.1.sz'

atoms, calc = restart('lcao_scat_dzp_relax.gpw',
                      basis=basis,
                      fixdensity=True,
                      txt='lcao_scat_szpsz.txt',
                      )

lcao = LCAOwrap(calc)
fermi = calc.get_fermi_level()
print >> open('fermi_scat_szpsz.txt', 'w'), repr(fermi)
H = lcao.get_hamiltonian()
S = lcao.get_overlap()
H -= fermi * S
centers = get_bf_centers(atoms, basis)
remove_pbc(atoms, H, S, d=0, centers_ic=centers, cutoff=None)
pickle.dump((H, S), open('hs_scat_szpsz.pckl', 'wb'), 2)

symbols = atoms.get_chemical_symbols()
indices = get_bfi2(symbols, basis, scat)
lcao.get_xc(indices=indices).dump('xc_sz.pckl')
lcao.get_Fcore(indices=indices).dump('Fcore_sz.pckl')
w_wG = lcao.get_orbitals(indices=indices)
P_awi = lcao.get_projections(indices=indices)
pickle.dump((w_wG, P_awi), open('w_wG__P_awi_sz.pckl', 'wb'), 2)
