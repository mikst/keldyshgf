#PBS -N AuBDA_lcao_scat_relax
#PBS -l nodes=2:ppn=8
#PBS -l walltime=01:00:00
#PBS -m abe

from ase import *
from gpaw import *
import cPickle as pickle
from ase.optimize.bfgs import BFGS

atoms = read('scattering.traj')
atoms.set_constraint(FixAtoms(range(4 * 16) +
                              range(4 * 16 + 4 + 16 + 4,
                                    2 * 4 * 16 + 4 + 16 + 4)))
calc = GPAW(nbands=1200,
            h=.19,
            xc='PBE',
            txt='lcao_scat_dzp_relax.txt',
            width=.2,
            usesymm=False,
            basis='de0.1.dzp',
            mode='lcao',
            mixer=Mixer(0.07, 5, weight=100.),
            kpts=(1, 1, 1),
            )
atoms.set_calculator(calc)
atoms.get_potential_energy()
qn = BFGS(atoms, logfile='qn.log', trajectory='relax.traj')
qn.run(fmax=0.1)

calc.write('lcao_scat_dzp_relax.gpw')
