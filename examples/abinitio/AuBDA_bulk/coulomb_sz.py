#PBS -N Vqq_sz
#PBS -m abe
#PBS -l nodes=6:ppn=8
#PBS -l walltime=22:00:00
from kgf.gpaw_tools import makeV

makeV(gpwfile='lcao_scat_dzp_relax.gpw',
      orbitalfile='w_wG__P_awi_sz.pckl',
      rotationfile='eps_q__U_pq_sz.pckl',
      coulombfile='V_qq_sz.pckl',
      log='coulomb_sz.txt',
      fft=True)
