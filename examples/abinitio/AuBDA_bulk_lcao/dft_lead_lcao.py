#PBS -N dftlead
#PBS -l nodes=1:ppn=8:xeon
#PBS -l walltime=2:00:00
#PBS -m abe

import cPickle as pickle
import numpy as np

from ase.io import read
from gpaw import GPAW, FermiDirac, Mixer, restart
from gpaw.lcao.pwf2 import LCAOwrap
from gpaw.lcao.tools import remove_pbc, get_bfi2, get_bf_centers
from gpaw.mpi import rank, MASTER, serial_comm
from gpaw.lcao.tools import dump_hamiltonian_parallel as dhp

basis = 'dzp'
atoms = read('scattering.xyz')
cell = np.loadtxt('scattering_cell.txt')
pos = atoms.get_positions()
ls = pos[16, 0] - pos[0, 0]
cell[0,0] = 3 * ls
atoms.set_cell(cell)
atoms.set_pbc(True)
atoms = atoms[:3*16]
atoms *= (4, 1, 1)


calc = GPAW(gpts=(152, 64, 56),
            xc='PBE',
            txt='dft_lead_lcao.txt',
            occupations=FermiDirac(0.1),
            kpts=(1,4,4),
            basis=basis,
            usesymm=False,
            mode='lcao',
            mixer=Mixer(0.075, 5, 100.0)
            )

atoms.set_calculator(calc)
atoms.get_potential_energy()
calc.write('dft_lead_lcao.gpw')
dhp('dft_lead_lcao_hs', atoms, direction='x')


