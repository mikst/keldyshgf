#PBS -N dftscat
#PBS -l nodes=1:ppn=8:xeon
#PBS -l walltime=2:00:00
#PBS -m abe

import cPickle as pickle
import numpy as np

from ase import io
from gpaw import GPAW, FermiDirac, Mixer, restart
from gpaw.lcao.pwf2 import LCAOwrap
from gpaw.lcao.tools import remove_pbc, get_bfi2, get_bf_centers
from gpaw.mpi import rank, MASTER, serial_comm
from gpaw.lcao.tools import dump_hamiltonian_parallel as dhp

scat = range(67, 85) # The scattering region
mol = range(68, 84)
basis = {None: 'dzp'}
for a in scat:
    basis[a] = 'szp'

for a in mol:
    basis[a] = 'dz'

atoms = io.read('scattering.xyz')
atoms.set_pbc(True)
atoms.set_cell(np.loadtxt('scattering_cell.txt'))
calc = GPAW(gpts=(184, 64, 56),
        xc='PBE',
        txt='dft_scat_lcao.txt',
        occupations=FermiDirac(0.1, maxiter=3000),
        kpts=(1,4,4),
        basis=basis,
        mode='lcao',
        mixer=Mixer(0.075, 5, 100.0)
        )
atoms.set_calculator(calc)
atoms.get_potential_energy()
calc.write('dft_scat_lcao.gpw')
dhp('dft_scat_lcao_hs', atoms, direction='x')

