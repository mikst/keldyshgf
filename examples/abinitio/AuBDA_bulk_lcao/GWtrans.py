#PBS -N GW
#PBS -m abe
#PBS -l nodes=16:ppn=8:xeon
#PBS -l walltime=50:00:00

import numpy as np
import cPickle as pickle
from kgf.GreenFunctions import NonEqNonIntGreenFunction,\
     NonEqIntGreenFunction
from kgf.ScatteringHamiltonianMatrix import ScatteringHamiltonianMatrix
from kgf.LeadHamiltonianMatrix import LeadHamiltonianMatrix
from kgf.selfenergies import NonEqConstantSelfEnergy
from kgf.selfenergies.FilterSelfEnergy import FilterXCCenter,FilterZeroCenter
from kgf.selfenergies.HFSelfEnergy import Hartree, Fock
from kgf.selfenergies.GWSelfEnergy import NonEqGWSelfEnergy
from kgf.selfenergies.SciOpSelfEnergy import SciOp
from gpaw.mpi import world

#energies = np.arange(-0.1,0.1,0.1)
energies = np.arange(-160, 160, 0.025)
de = abs(energies[1] - energies[0])
eta = 2. * de
i_zero = abs(energies).argmin()
assert abs(energies[i_zero]) < 1.0e-6
pl = 48 * 15
kpt = 2
H, S = pickle.load(open('dft_scat_ase2_hs%i.pckl' % kpt))
H1, S1 = pickle.load(open('harris_lead_hs%i.pckl' % kpt))
H1, S1 = H1[:2*pl, :2*pl].copy(), S1[:2*pl, :2*pl].copy()
Fcore = np.load('Fcore_G.pckl')
xc = np.load('xc_G.pckl')
V_qq = np.load('V_qq_G.pckl').astype(complex)
eps_q, U_pq = np.load('eps_q__U_pq_G.pckl')
Usq_pq = (U_pq * np.sqrt(eps_q)).astype(complex) # remeber sqrt
del U_pq

prin = H1.shape[-1] // 2
hmat = ScatteringHamiltonianMatrix(leftprincipallayer=prin,
                                   rightprincipallayer=prin,
                                   hamiltonianmatrix=H,
                                   overlapmatrix=S)

intern = ScatteringHamiltonianMatrix(leftprincipallayer=prin + 67*15, 
                                     rightprincipallayer=prin + 67*15,
                                     hamiltonianmatrix=H,
                                     overlapmatrix=S)

left = right = LeadHamiltonianMatrix(principallayer=prin,
                                     hamiltonianmatrix=H1,
                                     overlapmatrix=S1)

g0 = NonEqNonIntGreenFunction(hmat, left, right, E_Fermi=0., energies=energies,
                              internalscatteringhamiltonian=intern)
g0.SetInfinitesimal(eta)
g0.SetLeadInfinitesimal(0.05)
print g0.GetTotalParticleNumber()
orbitals = range(9, 9 + 80)

#se_hartree = FilterZeroCenter(
#    Hartree(g0, V_qq, Usq_pq, initialhartree=None), 1*9)
se_hartree = Hartree(g0, V_qq, Usq_pq, initialhartree=None)

se_fock    = FilterXCCenter(
    Fock(g0, V_qq, Usq_pq, Fcore=Fcore), xc, 1*9)

se_gw = FilterZeroCenter(
    NonEqGWSelfEnergy(g0, V_qq, Usq_pq, oversample=10), 1*9)

se_null    = NonEqConstantSelfEnergy(g0, np.zeros_like(xc),  
                                     'null')

se_xc      = NonEqConstantSelfEnergy(g0, -xc, 'xc')

dft = NonEqIntGreenFunction([se_null])
dft.WriteSpectralInfoToNetCDFFile(filename='DFT.nc', orbitals=orbitals,
                                  diagonalize=True, spectral='individual')

dft.WriteSpectralInfoToNetCDFFile(filename='DFT_AO.nc', #orbitals=orbitals,
                                  diagonalize=False, spectral='individual')

pulay = [0.05, 0.15, 1]
gf = NonEqIntGreenFunction([se_xc, se_hartree, se_fock])
gf.SelfConsistent(pulay, log='HF.log')
gf.WriteSpectralInfoToNetCDFFile(filename='HFSC.nc', orbitals=orbitals,
                                 diagonalize=True, spectral='individual')
gf.WriteSpectralInfoToNetCDFFile(filename='HFSC_AO.nc',# orbitals=orbitals,
                                 diagonalize=False, spectral='individual')


pulay = [0.05, 0.15, 1]
gf = NonEqIntGreenFunction([se_xc, se_hartree, se_fock, se_gw])
gf.SelfConsistent(pulay, log='GW.log', doslog=dict(
                  filename='GW_%i.nc', diagonalize=True,
                  spectral='individual', orbitals=orbitals))

gf.WriteSpectralInfoToNetCDFFile(filename='GWSC.nc', orbitals=orbitals,
                                 diagonalize=True, spectral='individual')
gf.WriteSpectralInfoToNetCDFFile(filename='GWSC_AO.nc', 
                                 diagonalize=False, spectral='individual')



