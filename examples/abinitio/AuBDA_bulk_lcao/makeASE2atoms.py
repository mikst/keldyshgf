import numpy as np
import cPickle as pickle

pl = 3 * 16 * 15
print 'USING PL of %i' % pl 

kpt = 2
h0, s0 = pickle.load(open('harris_scat_hs%i.pckl' % kpt))
h0, s0 = h0[0, 0].copy(), s0[0].copy()
h1, s1 = pickle.load(open('harris_lead_hs%i.pckl' % kpt))
h1, s1 = h1[0,0,:2*pl, :2*pl].copy(), s1[0,:2*pl, :2*pl].copy()

H = np.zeros((len(h0)+2*pl,)*2, h0.dtype)
S = np.zeros((len(h0)+2*pl,)*2, h0.dtype)

H[:2*pl , :2*pl ] = h1
H[-2*pl:, -2*pl:] = h1
S[:2*pl , :2*pl ] = s1
S[-2*pl:, -2*pl:] = s1
bf = 1
diff = (h0[bf, bf] - h1[bf, bf]) / s0[bf, bf]
print 'diff: %.3f' % diff
h0 -= diff * s0
H[pl:-pl, pl:-pl] = h0
S[pl:-pl, pl:-pl] = s0

pickle.dump((H, S), open('dft_scat_ase2_hs%i.pckl' % kpt, 'wb'), 2)
