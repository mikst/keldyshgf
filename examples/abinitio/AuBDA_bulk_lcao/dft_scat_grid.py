#PBS -N BDAGridS
#PBS -l nodes=4:ppn=8:xeon
#PBS -l walltime=24:00:00
#PBS -m abe

from ase.io import read
from gpaw import GPAW, FermiDirac, Mixer
import numpy as np

atoms = read('scattering.xyz')
atoms.set_cell(np.loadtxt('scattering_cell.txt').T)
atoms.set_pbc(True)

calc = GPAW(gpts=(184, 64, 56),
            xc='PBE',
            txt='dft_scat_grid.txt',
            occupations=FermiDirac(0.1),
            kpts=(1, 4, 4),
            mixer=Mixer(0.1, 7, 100.0),
            maxiter=200
            )
atoms.set_calculator(calc)
atoms.get_potential_energy()
calc.write('dft_scat_grid.gpw')
