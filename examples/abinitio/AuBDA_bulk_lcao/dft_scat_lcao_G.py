#PBS -N dftscat
#PBS -l nodes=1:ppn=8:xeon
#PBS -l walltime=12:00:00
#PBS -m abe

import cPickle as pickle
import numpy as np
from ase import io
from gpaw import GPAW, FermiDirac, Mixer, restart
from gpaw.lcao.pwf2 import LCAOwrap
from gpaw.lcao.tools import remove_pbc, get_bfi2, get_bf_centers
from gpaw.mpi import rank, MASTER, serial_comm
scat = range(67, 85) # The scattering region
mol = range(68, 84)
basis = {None: 'dzp'}
for a in scat:
    basis[a] = 'szp'

for a in mol:
    basis[a] = 'dz'

#basis = 'sz' # for testing
if 1:
    atoms = io.read('scattering.xyz')
    atoms.set_pbc(True)
    atoms.set_cell(np.loadtxt('scattering_cell.txt'))
    calc = GPAW(gpts=(184, 64, 56),
                xc='PBE',
                txt='dft_scat_G.txt',
                occupations=FermiDirac(0.1),
                basis=basis,
                mode='lcao',
                mixer=Mixer(0.075, 5, 100.0)
                )
    atoms.set_calculator(calc)
    atoms.get_potential_energy()
    calc.write('dft_scat_G.gpw')

if 1:
    if rank == MASTER:
        atoms, calc = restart('dft_scat_G.gpw',
                              txt=None, communicator=serial_comm)
        lcao = LCAOwrap(calc)
        fermi = calc.get_fermi_level()
        print >> open('fermi_scat_G.txt', 'w'), repr(fermi)
        H = lcao.get_hamiltonian()
        S = lcao.get_overlap()
        H -= fermi * S
        centers = None #get_bf_centers(atoms, basis)
        remove_pbc(atoms, H, S, d=0, centers_ic=centers, cutoff=None)
        pickle.dump((H, S), open('dft_scat_hs_G.pckl', 'wb'), 2)
        symbols = atoms.get_chemical_symbols()
        indices = get_bfi2(symbols, basis, scat)
        lcao.get_xc(indices=indices).dump('xc_G.pckl')
        lcao.get_Fcore(indices=indices).dump('Fcore_G.pckl')
        w_wG = lcao.get_orbitals(indices=indices)
        P_awi = lcao.get_projections(indices=indices)
        pickle.dump((w_wG, P_awi), open('w_wG__P_awi_G.pckl', 'wb'), 2)
