#PBS -N Tdzp0.01dz
#PBS -m abe
#PBS -l nodes=1:ppn=8:xeon
#PBS -l walltime=10:00:00

import numpy as np
import cPickle as pickle
from kgf.GreenFunctions import NonEqNonIntGreenFunction, NonEqIntGreenFunction
from kgf.ScatteringHamiltonianMatrix import ScatteringHamiltonianMatrix
from kgf.LeadHamiltonianMatrix import LeadHamiltonianMatrix
from kgf.selfenergies import NonEqConstantSelfEnergy
from kgf.selfenergies.FilterSelfEnergy import FilterXCCenter,FilterZeroCenter
from kgf.selfenergies.HFSelfEnergy import Hartree, Fock
from kgf.selfenergies.GWSelfEnergy import NonEqGWSelfEnergy
from kgf.selfenergies.SciOpSelfEnergy import SciOp

pl = 3 * 16 * 15 # (layers) * (natoms per layer) * (nbf per atom)
#energies = np.array([-0.1, 0.0, 0.1])
energies = np.arange(-4., 0.35, 0.1)
kpt = 0
H, S = pickle.load(open('dft_scat_ase2_hs%i.pckl' % kpt))
H1, S1 = pickle.load(open('dft_lead_lcao_hs%i.pckl' % kpt))
H1 = H1[0, 0, :2*pl, :2*pl].copy()
S1 = S1[0, :2*pl, :2*pl].copy()
xc = pickle.load(open('xc_G.pckl'))
prin = H1.shape[-1] // 2

hmat = ScatteringHamiltonianMatrix(leftprincipallayer=prin,
                                   rightprincipallayer=prin,
                                   hamiltonianmatrix=H,
                                   overlapmatrix=S)

intern = ScatteringHamiltonianMatrix(leftprincipallayer=prin + 67 * 15,  
                                     rightprincipallayer=prin + 67 * 15, 
                                     hamiltonianmatrix=H,
                                     overlapmatrix=S)

left = right = LeadHamiltonianMatrix(principallayer=prin,
                                     hamiltonianmatrix=H1,
                                     overlapmatrix=S1)

g0 = NonEqNonIntGreenFunction(hmat, left, right, E_Fermi=0., 
                              energies=energies, 
                              internalscatteringhamiltonian=intern)

de = abs(energies[1] - energies[0])
eta = 2 * de
eta = 0.000001
assert abs(energies).min()<1.0e-6
g0.SetInfinitesimal(eta)
g0.SetLeadInfinitesimal(0.00001)
print g0.GetTotalParticleNumber() # start calculation

orbitals = range(1 * 9, 1 * 9 + 80) # XXX
se_null    = NonEqConstantSelfEnergy(g0, np.zeros_like(xc, complex), 'null')
gf = NonEqIntGreenFunction([se_null])

gf.WriteSpectralInfoToNetCDFFile(filename='DFT_summed%i.nc' % kpt, 
                                  orbitals=orbitals,
                                  diagonalize=True, spectral='summed')
gf.WriteSpectralInfoToNetCDFFile(filename='DFT%i.nc' % kpt, orbitals=orbitals,
                                  diagonalize=True, spectral='individual')
