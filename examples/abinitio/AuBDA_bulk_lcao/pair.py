#PBS -N pair
#PBS -l nodes=8:ppn=8:xeon
#PBS -l walltime=12:00:00
#PBS -m abe


from kgf.gpaw_tools import makeU

makeU(gpwfile='dft_scat_G.gpw', 
      orbitalfile='w_wG__P_awi_G.pckl', 
      rotationfile='eps_q__U_pq_G.pckl', 
      tolerance=1.0e-5)
