#PBS -N vqq
#PBS -l nodes=4:ppn=8:xeon
#PBS -l walltime=50:00:00
#PBS -m abe

from kgf.gpaw_tools import makeV

makeV(gpwfile='dft_scat_lcao_G.gpw', 
      orbitalfile='w_wG__P_awi.pckl',
      rotationfile='eps_q__U_pq.pckl', 
      coulombfile='V_qq.pckl',
      log='coulomb.log',
      fft=True)

