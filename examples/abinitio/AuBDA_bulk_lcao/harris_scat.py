from gpaw import GPAW
from gpaw.mpi import world
from gpaw.lcao.tools import dump_hamiltonian_parallel as dhp

scat = range(67, 85) # The scattering region
mol = range(68, 84)
basis = {None: 'dzp'}
for a in scat:
    basis[a] = 'szp'

for a in mol:
    basis[a] = 'dz'


calc = GPAW('dft_scat_grid.gpw', 
            basis=basis, 
            mode='lcao', 
            fixdensity=True, 
            txt='harris_scat.txt', 
            kpts=(1, 4, 4))

ef = calc.get_fermi_level()
if world.rank == 0: print ef
atoms = calc.get_atoms()
atoms.get_potential_energy()
calc.write('harris_scat.gpw')
if world.rank==0: print calc.get_fermi_level()
dhp('harris_hs', atoms, direction='x')
