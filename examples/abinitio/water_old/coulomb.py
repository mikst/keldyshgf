from kgf.gpaw_tools import makeV

makeV(gpwfile='grid_PBE.gpw',
      orbitalfile='w_wG__P_awi.pckl',
      rotationfile='eps_q__U_pq.pckl',
      coulombfile='V_qq.pckl',
      log='coulomb.txt',
      fft=False) # fft=True is faster, but only allowed for pbc
