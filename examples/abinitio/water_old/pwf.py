import numpy as np
import cPickle as pickle
from gpaw.lcao.pwf2 import PWF2

# Make the PWF functions
fermi = float(np.loadtxt('fermilevel.txt'))
pwf = PWF2('grid_PBE.gpw', fixedenergy=fermi + .1, ibl=True, basis='sz')

# Dump Hamiltonian, overlap, xc-matrix and core Fock matrix.
H = pwf.get_hamiltonian()
S = pwf.get_overlap()
xc = pwf.get_xc()
Fcore = pwf.get_Fcore()
pickle.dump((H, S, xc, Fcore), open('HSxcFcore.pckl', 'wb'), 2)

# Dump the PWF orbitals
w_wG = pwf.get_orbitals()
P_awi = pwf.get_projections()
pickle.dump((w_wG, P_awi), open('w_wG__P_awi.pckl', 'wb'), 2)
