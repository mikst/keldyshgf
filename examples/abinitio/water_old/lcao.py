import numpy as np
import cPickle as pickle
from gpaw.lcao.pwf2 import LCAOwrap
from gpaw import GPAW

# Make the PWF functions
fermi = float(np.loadtxt('fermilevel.txt'))
calc = GPAW('lcao_PBE.gpw', txt=None)
lcao = LCAOwrap(calc)

# Dump Hamiltonian, overlap, xc-matrix and core Fock matrix.
H = lcao.get_hamiltonian()
S = lcao.get_overlap()
xc = lcao.get_xc()
Fcore = lcao.get_Fcore()
pickle.dump((H, S, xc, Fcore), open('HSxcFcore.pckl', 'wb'), 2)

# Dump the PWF orbitals
w_wG = lcao.get_orbitals()
P_awi = lcao.get_projections()
pickle.dump((w_wG, P_awi), open('w_wG__P_awi.pckl', 'wb'), 2)
