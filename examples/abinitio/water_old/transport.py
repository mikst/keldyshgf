#PBS -l nodes=1:ppn=4,walltime=00:10:00 -m ae
import numpy as np

from kgf.GreenFunctions import NonEqNonIntGreenFunction, NonEqIntGreenFunction
from kgf.ScatteringHamiltonianMatrix import ScatteringHamiltonianMatrix
from kgf.LeadHamiltonianMatrix import LeadHamiltonianMatrix
from kgf.selfenergies import NonEqConstantSelfEnergy
from kgf.selfenergies.HFSelfEnergy import Hartree, Fock
from kgf.selfenergies.GWSelfEnergy import NonEqGWSelfEnergy

# Load matrices
fermi = np.loadtxt('fermilevel.txt')
H, S, xc, Fcore = np.load('HSxcFcore.pckl')
V_qq = np.load('V_qq.pckl').astype(complex)
eps_q, U_pq = np.load('eps_q__U_pq.pckl')
Usq_pq = (U_pq * np.sqrt(eps_q)).astype(complex)
del U_pq

# Setup scattering- and lead Hamiltonians
hmat = ScatteringHamiltonianMatrix(leftprincipallayer=0,
                                   rightprincipallayer=0,
                                   hamiltonianmatrix=H,
                                   overlapmatrix=S)
left = right = LeadHamiltonianMatrix(principallayer=0,
                                     hamiltonianmatrix=np.zeros((2, 2)))

# Setup noninteracting Green function
energies = np.arange(-120, 120, 0.02)
g0 = NonEqNonIntGreenFunction(hmat, left, right, E_Fermi=fermi,
                              energies=energies, uncoupled=True)
g0.SetInfinitesimal(0.04) # Should be at least 2 * de
print 'Total electron number:', g0.GetTotalParticleNumber()

# Setup self energies
S_null = NonEqConstantSelfEnergy(g0, 0.0 * xc, 'null')
S_xc = NonEqConstantSelfEnergy(g0, -xc, 'xc')
S_Hartree = Hartree(g0, V_qq, Usq_pq, initialhartree=None)
S_Fock = Fock(g0, V_qq, Usq_pq, Fcore=Fcore)
S_gw = NonEqGWSelfEnergy(g0, V_qq, Usq_pq, oversample=10)

# pulay parameters: tolerance, linear mixing, history
pulay = (0.05, 0.4, 1)

# DFT
gf = NonEqIntGreenFunction([S_null])
gf.WriteSpectralInfoToNetCDFFile('DFT.nc', diagonalize=True)

# Hartree Fock
gf = NonEqIntGreenFunction([S_xc, S_Hartree, S_Fock])
gf.SelfConsistent(log='HF.log',
                  pulay=pulay,
                  #doslog=dict(filename='HF_%i.nc',
                  #            diagonalize=True),
                  )
gf.WriteSpectralInfoToNetCDFFile('HF.nc', diagonalize=True)

# GW
gf = NonEqIntGreenFunction([S_xc, S_Hartree, S_Fock, S_gw])
gf.SelfConsistent(log='GW.log',
                  pulay=pulay,
                  #doslog=dict(filename='GW_%i.nc',
                  #            diagonalize=True),
                  )
gf.WriteSpectralInfoToNetCDFFile('GW.nc', diagonalize=True)
