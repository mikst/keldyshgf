#PBS -N PBE0EXX
#PBS -l nodes=2:ppn=4:opteron285,walltime=08:00:00
#PBS -m abe

from gpaw import GPAW

conv   = {'energy': 1e-3, 'density': 1e-3, 'eigenstates': 1e-7} # 1e-9
PBE0   = {'name': 'PBE0', 'finegrid': False}
EXX    = {'name': 'EXX',  'finegrid': False}

f = open('grid_eigs.txt', 'w')

# Load old calculation
calc = GPAW('grid_PBE.gpw')#, txt='grid_PBE0EXX.txt')
for s in range(calc.wfs.nspins):
    print >>f, calc.get_eigenvalues(spin=s)

# Do non-self PBE0
eigs_skn = calc.get_nonselfconsistent_eigenvalues(PBE0)
for s in range(calc.wfs.nspins):
    print >>f, eigs_skn[s, 0]

# Do non-self EXX
eigs_skn = calc.get_nonselfconsistent_eigenvalues(EXX)
for s in range(calc.wfs.nspins):
    print >>f, eigs_skn[s, 0]

# Do selfconsistent PBE0
calc = GPAW('grid_PBE.gpw', txt='grid_PBE0EXX.txt')
calc.set(xc=PBE0, convergence=conv)
calc.get_potential_energy()
for s in range(calc.wfs.nspins):
    print >>f, calc.get_eigenvalues(spin=s)

# Do selfconsistent EXX
calc.set(xc=EXX, fixdensity=False, convergence=conv)
calc.get_potential_energy()
for s in range(calc.wfs.nspins):
    print >>f, calc.get_eigenvalues(spin=s)
