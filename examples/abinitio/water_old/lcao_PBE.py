from ase import molecule
from gpaw import GPAW

atoms = molecule('H2O')
atoms.center(vacuum=4.)
calc = GPAW(h=.19, xc='PBE', nbands=-5, txt='lcao_PBE.txt',
            mode='lcao', basis='dzp')
atoms.set_calculator(calc)
atoms.get_potential_energy()
calc.write('lcao_PBE.gpw', mode='all')
print >>open('fermilevel.txt', 'w'), calc.get_homo_lumo().mean()
