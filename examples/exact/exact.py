from math import sqrt, cos, acos, pi
import numpy as np

def gram_schmidt(U):
    """Orthonormalize columns of U according to the Gram-Schmidt procedure."""
    for i, col in enumerate(U.T):
        for col2 in U.T[:i]:
            col -= col2 * np.dot(col2.conj(), col)
        col /= np.linalg.norm(col)

U = 4.0
ep = 4.0
z = sqrt(ep * (ep + U))

x1 = (z+ep-U/4.0)/2.0 - sqrt(((z+ep+(U/4.0))/2.0)**2+3.0*ep*(U**2)/(8.0*z))
x2 = (z+ep-U/4.0)/2.0 + sqrt(((z+ep+(U/4.0))/2.0)**2+3.0*ep*(U**2)/(8.0*z))

b=-2.*ep+(U/4.0)
c=-z*(z+ep)+ep*(ep-U/4.)-3.*ep*(U**2.)/(8.*z)
d=z*(z+ep)*(ep-U/4.)+(ep-z)*ep*(U**2.)/(8.*z)

p=(3.0*c-b**2)/9.0
q=(b**3)/27.0-(b*c)/6.0+d/2.0
r=-np.sign(p)*sqrt(abs(p))
phi=acos(q/(r**3))

y1=-(b/3.)-2*r*cos(phi/3.)
y2=-(b/3.)+2*r*cos((pi-phi)/3.)
y3=-(b/3.)+2*r*cos((pi+phi)/3.)

eta=0.1

H = np.zeros([4, 4], float)
H.flat[::5] = 4.
H -= 1.0

val, vec = np.linalg.eigh(H)
ind = val.real.argsort()
val = val[ind]
vec = vec[:, ind]
gram_schmidt(vec)
vec = vec.T

energies = np.arange(-12, 15, 0.005)
Ne = len(energies)

s = np.zeros((4, 4), complex)
for i in range(1, 4):
    s += np.outer(vec[i], vec[i])

G     = np.zeros([Ne, 4, 4], complex)
sigma = np.zeros([Ne, 4, 4], complex)
chi   = np.zeros([Ne, 4, 4], complex)
w     = np.zeros([Ne, 4, 4], complex)
gnot  = np.zeros([Ne, 4, 4], complex)
G2    = np.zeros([Ne, 4, 4], complex)
for i, e in enumerate(energies):
    # Uncoupled??  Green function
    G[i] = (0.25*(e-z-ep)/((e-x1-1.0j*eta)*(e-x2+1.0j*eta)))*np.ones([4,4],complex)
    G[i] += s*(((e-z-ep)*(e+z)) /
               ((e-y1-1.0j*eta)*(e-y2+1.0j*eta)*(e-y3+1.0j*eta)))

    # GW selfenergy
    sigma[i] = -U/4.0*np.identity(4, complex)
    sigma[i] += ((3*ep*(U**2))/(8.0*z))*(np.outer(vec[0], vec[0]) /
                                         (e-z-ep+1.0j*eta))
    sigma[i] += ((ep*(U**2))/(8.0*z))*s*(2.0/(e-z-ep+1.0j*eta) +
                                         1.0/(e+z-1.0j*eta))

    # Polarization function
    chi[i] = 0.5 * s * ((1.0 / (e-ep+1.0j*eta)) - (1.0 / (e+ep-1.0j*eta)))

    # Screened electron interaction
    w[i] = U * np.identity(4, complex)
    w[i] += (ep*(U**2)/(2*z))*s*((1.0/(e-z+1.j*eta))-(1.0/(e+z-1.j*eta)))

    # G0
    gnot[i] = 0.25 * np.ones([4,4], complex) / (e - 1.j * eta)
    gnot[i] += s / (e - ep + 1.j * eta)

    # Interacting Green function ???
    G2[i] = np.linalg.inv(np.linalg.inv(gnot[i]) - sigma[i])

# Spectral functions for site 0
spec1 = abs(G[:, 0, 0].imag)
spec2 = abs(G2[:, 0, 0].imag)

# Integrate spectral functions
print np.trapz(spec1, energies)
print np.trapz(spec2, energies)

## from ase.io.pupynere import NetCDFFile
## chie = NetCDFFile('chi_exact.nc').variables['yvalues'][:]
## gwe = NetCDFFile('gw_exact.nc').variables['yvalues'][:]
## spece = NetCDFFile('spec_exact.nc').variables['yvalues'][:]
