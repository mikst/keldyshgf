#!/usr/bin/env python

#PBS -e pc.err
#PBS -o gw.log
#PBS -m ae
#PBS -q medium
#PBS -l nodes=1:opteron285



import Numeric
from ASE.Transport import CurrentCalculator,GreenFunctions
from ASE.Transport.ScatteringHamiltonianMatrix import ScatteringHamiltonianMatrix
from ASE.Transport.LeadHamiltonianMatrix import LeftLeadHamiltonianMatrix,RightLeadHamiltonianMatrix
from ASE.Transport import IntSelfEnergy as IntSelfEnergy

# Construct a lead Hamiltonian matrix describing 2 principal layers of 
# a 1d TB chain with up to second-nearest neighbor hopping                

H_lead=Numeric.zeros([4,4],Numeric.Complex)

H_lead[0,0]=H_lead[1,1]=H_lead[2,2]=H_lead[3,3]=0.0
for i in range(3):
    H_lead[i,i+1]=-10.0
    H_lead[i+1,i]=-10.0
 
H_scat=Numeric.zeros([4+4,4+4],Numeric.Complex)

# Principal layers
H_scat[:2,:2]=H_lead[:2,:2]
H_scat[-2:,-2:]=H_lead[:2,:2]


H=Numeric.zeros([4,4],Numeric.Complex)
H[0,0]=4
H[1,1]=4
H[2,2]=4
H[3,3]=4
H=H-1.0

H_scat[2:-2,2:-2]=H

V=Numeric.zeros([8,8],Numeric.Complex)
for i in range(4):
    V[i,i]=4.0
    V[i+4,i]=4.0
    V[i,i+4]=4.0
    V[i+4,i+4]=4.0
    
   

prin=2
hmat=ScatteringHamiltonianMatrix(leftprincipallayer=prin,rightprincipallayer=prin,hamiltonianmatrix=H_scat)
left=LeftLeadHamiltonianMatrix(principallayer=prin,hamiltonianmatrix=H_lead)
right=RightLeadHamiltonianMatrix(principallayer=prin,hamiltonianmatrix=H_lead)
grid1=Numeric.arange(-40,40,0.025)

nonintgf=GreenFunctions.NonEqNonIntGreenFunction(hmat,left,right,E_Fermi=1.6,uncoupled=1)
nonintgf.SetLeftChemicalPotential(0.0)
nonintgf.SetRightChemicalPotential(-0.0)
nonintgf.SetEnergyList(grid1)
nonintgf.SetInfinitesimal(0.09622)
nonintgf.SetTemperature(0.0)

ep=Numeric.arange(-8.,-2.,0.2)
ep=[-3.]
Ilist=[]
en=[]
occ_lst=[]

se_GW=IntSelfEnergy.NonEqGWSelfEnergy(nonintgf,grid1,V[:4,:4],V[:4,:4],whichHF=2,oversample=10)
#se_hartree=IntSelfEnergy.NonEqHartreeSelfEnergy(nonintgf,[V])
#se_hartree.SetInitialHartree(0.0*se_hartree.GetFullHartree())
#se_fock=IntSelfEnergy.NonEqFockSelfEnergy(nonintgf,[V])
gf=GreenFunctions.NonEqIntGreenFunction([se_GW])
se_GW.NonSelfInteractionFree()
#current=CurrentCalculator.CurrentCalculator(gf)


from ASE.Transport.Tools import WriteToNetCDFFile
gw=se_GW.GetRetardedGW()
#chi=se_GW.GetRetardedChi()
WriteToNetCDFFile('gw2.nc',grid1,gw[:,0,0].imag)
#WriteToNetCDFFile('chi.nc',grid1,chi[:,0,0].imag)
#nonintgf.SetInfinitesimal(0.1)
gf.WriteSpectralInfoToNetCDFFile('spec_no_eta2.nc')
#se_GW.SetActive(0)
#gf.SelfConsistent(mix=0.4,tol=0.002,history=4)
#se_GW.SetActive(1)
#se_fock.SetActive(0)
#gf.WriteSpectralInfoToNetCDFFile('test.nc')
#gf.SelfConsistent(mix=0.4,tol=0.002,history=4)
#gf.WriteSpectralInfoToNetCDFFile('sc.nc',diagonalize=True)



