#!/usr/bin/env python
#PBS -l nodes=1,walltime=01:20:00 -o out.txt -e err.txt

import numpy as np

def semicond_wire(e0, tvc, t, homo, lumo, th):
    """Creates Lead and scattering Hamiltonian for a semiconducting wire.

    gap indicates the desired bandgap of the semiconducting wire::
       
             -t     -t     -th ___ lumo
       e0 ___   ___   ___ /  /
           |              \ / th
           |tvc            X    
      -e0 _|_   ___   ___ / \-th
              t     t     \  \ ___ homo
                           th
    """
    Hl = np.array([[-e0, tvc], # Lead onsite
                   [tvc,  e0]])
    Vl = np.array([[ t, .0], # Lead hopping
                   [.0, -t]])
    Hs = np.array([[homo,  0.0], # scattering onsite
                   [ 0.0, lumo]])
    Vs = np.array([[ th,  th], # Scattering hopping. Format:   VH VL
                   [-th, -th]])# Conduction Valence Homo Lumo  CH CL
    Zs = np.zeros((2, 2)) # zeros

    
    Hlead = np.vstack((np.hstack((Hl, Vl)),
                       np.hstack((Vl, Hl))))

    Hscat = np.vstack((np.hstack((Hl,   Vl, Zs, Zs)),
                       np.hstack((Vl,   Hl, Vs, Zs)),
                       np.hstack((Zs, Vs.T, Hs, Zs)),
                       np.hstack((Zs,   Zs, Zs, Hl))))
    
    return Hlead, Hscat

def interaction(ext, U0=.0, Uhl=.0):
    """Create the ijij interaction matrix, and shift for shown system.

    ::
      
                ext  U0
         -t     / \ ___ lumo
      ___   ___/  /
               \ /   |
                X   Uhl
      ___   ___/ \   |
          t    \  \ ___ homo
                \_/  
                ext  U0

    ijkl = iint dr dr' / |r-r'| i*(r) j*(r') k(r) l(r')

    ijij = iint dr dr' / |r-r'| |i(r)|^2 |j(r')|^2
    """
    
    ijij = np.array([[0.0, 0.0, ext, ext],
                     [0.0, 0.0, ext, ext],
                     [ext, ext,  U0, Uhl],
                     [ext, ext, Uhl,  U0]])
    shift = np.diag([-2 * ext, -2 * ext,
                     -2 * ext - U0 / 2 - Uhl, -2 * ext - U0 / 2 - Uhl])
    return ijij, shift


from kgf.GreenFunctions import NonEqNonIntGreenFunction, NonEqIntGreenFunction
from kgf.ScatteringHamiltonianMatrix import ScatteringHamiltonianMatrix
from kgf.LeadHamiltonianMatrix import LeadHamiltonianMatrix
from kgf.selfenergies import NonEqConstantSelfEnergy
from kgf.selfenergies.GW2index import Hartree2index, Fock2index, GW2index

def trans(e0, tvc, t, homo, lumo, th, ext, U0, Uhl, name):
    Hlead, Hscat = semicond_wire(e0, tvc, t, homo, lumo, th)
    ijij, shift = interaction(ext, U0, Uhl)

    Hlead = np.asarray(Hlead, complex)
    Hscat = np.asarray(Hscat, complex)
    ijij  = np.asarray(ijij,  complex)
    shift = np.asarray(shift, complex)

    # Setup scattering- and lead Hamiltonians
    hmat = ScatteringHamiltonianMatrix(leftprincipallayer=2,
                                       rightprincipallayer=2,
                                       hamiltonianmatrix=Hscat)
    left = right = LeadHamiltonianMatrix(principallayer=2,
                                         hamiltonianmatrix=Hlead)

    # Non-interacting Green function
    energies = np.arange(-80, 80, .01)
    g0 = NonEqNonIntGreenFunction(hmat, left, right, E_Fermi=0.)
    g0.SetLeftChemicalPotential(0.0)
    g0.SetRightChemicalPotential(-0.0)
    g0.SetEnergyList(energies)
    g0.SetInfinitesimal(.02)
    g0.SetTemperature(0.0)
    g0.SetNUGridData(2, -30, 30)

    # Define self-energies
    se_shift = NonEqConstantSelfEnergy(g0, shift, 'shift')
    se_hartree = Hartree2index(g0, ijij, initialhartree=np.zeros_like(ijij))
    se_fock = Fock2index(g0, ijij)
    se_gw = GW2index(g0, energies, ijij, oversample=10)

    # Interacting Green function
    gf = NonEqIntGreenFunction([se_shift, se_hartree, se_fock, se_gw])

    se_gw.SetActive(False)
    gf.WriteSpectralInfoToNetCDFFile(name + '_nsc_HF.nc')
    gf.SelfConsistent()
    gf.WriteSpectralInfoToNetCDFFile(name + '_HF.nc')

    se_gw.SetActive(True)
    gf.SelfConsistent()
    gf.WriteSpectralInfoToNetCDFFile(name + '_GW.nc')

if __name__ == '__main__':
    trans(e0   =  2.0,
          tvc  =  0.3,
          t    =  1.0,
          homo = -2.0,
          lumo =  2.0,
          th   =  0.0,
          ext  =  5.0,
          U0   =  0.0,
          Uhl  =  0.0,
          name='semicond')
