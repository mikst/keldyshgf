#!/usr/bin/env python
#PBS -N IV_anderson
#PBS -l nodes=1:ppn=4
#PBS -l walltime=01:35:00
#PBS -m abe

from ase.parallel import paropen
from gpaw.mpi import world
import numpy as np
from kgf.GreenFunctions import NonEqNonIntGreenFunction, NonEqIntGreenFunction
from kgf.selfenergies.GW2index import Hartree2index, Fock2index, GW2index
from kgf.selfenergies import NonEqConstantSelfEnergy
from kgf.ScatteringHamiltonianMatrix import ScatteringHamiltonianMatrix
from kgf.LeadHamiltonianMatrix import LeadHamiltonianMatrix
from kgf.CurrentCalculator import CurrentCalculator

from tb_models import anderson, anderson_interaction
H_lead, H_scat = anderson(e0=0.0, t=-10, homo=-1.0, lumo=1.0, th=0.8,
                          right_lead=True)
shift, ijij = anderson_interaction(U0=4.0, Uhl=3.0, Uext=2.8,
                                   right_lead=False)

# Setup the non interacting Hamiltonian for the 3 regions
prin = 1
hmat = ScatteringHamiltonianMatrix(leftprincipallayer=prin,
                                   rightprincipallayer=prin,
                                   hamiltonianmatrix=H_scat)
left = right = LeadHamiltonianMatrix(principallayer=prin,
                                     hamiltonianmatrix=H_lead)
# Choice energy grid fine enough to sample the sharpest features
eta = 0.1
de = 0.5 * eta
energies = np.arange(-100, 100, de)
assert abs(energies).min() < 1e-5

# Bias steps must be integers number of de, such that chemical potentials are
# between energy grid points
bias = 2 * de * np.arange(5)

# Bias loop
IV_HF_file = paropen('IV_HF.dat', 'w')
IV_GW_file = paropen('IV_GW.dat', 'w')

for Vbias in bias:
    # Setup the noninteracting Green function
    g0 = NonEqNonIntGreenFunction(hmat, left, right, E_Fermi=0.,
                                  energies=energies)
    g0.SetLeftChemicalPotential(0.5 * Vbias)
    g0.SetRightChemicalPotential(-0.5 * Vbias)
    g0.SetInfinitesimal(1.0e-4)

    # Various selfenergies
    se_shift = NonEqConstantSelfEnergy(g0, shift, 'shift')
    se_hartree = Hartree2index(g0, ijij, initialhartree=np.zeros_like(ijij))
    se_fock = Fock2index(g0, ijij)
    se_gw = GW2index(g0, ijij, oversample=10)

    # Setup the interacting Green function
    gf = NonEqIntGreenFunction([se_shift, se_hartree, se_fock, se_gw])

    # Hartree Fock calculation
    se_gw.SetActive(False) # turn off the GW selfenergy
    gf.SelfConsistent(pulay=(0.02, 0.5, 3))
    gf.WriteSpectralInfoToNetCDFFile('HF_bias%.2f.nc' % Vbias)

    #Calculate the current
    calc = CurrentCalculator(gf)
    I = calc.GetCurrent()
    if world.rank == 0:
        print >> IV_HF_file, Vbias, I
        IV_HF_file.flush()

    # GW calculation
    se_gw.SetActive(True) # turn on the GW selfenergy
    gf.SelfConsistent(pulay=(0.02, 0.5, 3))
    gf.WriteSpectralInfoToNetCDFFile('GW_bias%.2f.nc' % Vbias)
    calc = CurrentCalculator(gf)
    I = calc.GetCurrent()
    if world.rank == 0:
        print >> IV_GW_file, Vbias, I
        IV_GW_file.flush()
