import numpy as np


def metal_wire_(Nsites, onsite, hopping, dtype=complex):
    """Return Hamiltonian for a nearest-neighbor TB chain."""
    H = np.zeros([Nsites, Nsites], dtype)
    H.flat[::Nsites + 1] = onsite
    H.flat[1::Nsites + 1] = H.flat[Nsites::Nsites + 1] = hopping
    return H


def anderson(e0=0.0, t=-10, homo=-2.0, lumo=2.0, th=0.8,
             right_lead=True, dtype=complex):
    """Lead and scattering Hamiltonian for a metallic wire with an impurity.

    Parameters are indicated in the model below, right_lead indicates if
    the impurity is attached to a right lead or not::

                        lumo 
                     th ---- th
        __ t __ t __  /      \  __ t __ t __
        e0   e0   e0  \      /  e0   e0   e0
                     th ---- th
                        homo

    """
    # (double) lead matrix
    Hlead = np.array([[ e0,  t],
                      [  t, e0]], dtype)

    # Scattering region including one principal layer on each side
    Hscat = np.array([[e0,  t,    0,   0,   0,  0],
                      [ t, e0,   th,   th,  0,  0],
                      [ 0, th, homo,    0, th,  0],
                      [ 0, th,    0, lumo,-th,  0],
                      [ 0,  0,   th,  -th, e0,  t],
                      [ 0,  0,    0,    0,  t, e0]], dtype)

    # decouple if no right lead
    if not right_lead:
        Hscat[4, :] = H[:, 4] = 0.0
    
    return Hlead, Hscat


def semicond(e0=2.0, t=2.0, homo=-2.0, lumo=2.0, th=0.0, dtype=complex):
    """Lead and scattering Hamiltonian for a semiconducting wire
    with an impurity.

    ::

                             lumo
                          /  ----
                       -th  /    
       e0 ___-t___-t___ /  th    
                        \ /      
                         x       
      -e0 ___  ___  ___ / \      
              t    t    \  -th   
                        th  \    
                          \  ----
                             homo
    """
    assert right_lead == False
    # Lead onsite
    Hl = np.array([[-e0,  0],
                   [  0, e0]], dtype)

    # Lead hopping
    Vl = np.array([[ t,  0],
                   [ 0, -t]], dtype)

     # scattering onsite
    Hs = np.array([[homo,  0.0],
                   [ 0.0, lumo]], dtype)

    # Scattering hopping Format: Conduction Valence Homo Lumo
    Vs = np.array([[ th,  th],        # VH VL
                   [-th, -th]], dtype)# CH CL

    # zeros
    Zs = np.zeros((2, 2))

    
    Hlead = np.vstack((np.hstack((Hl, Vl)),
                       np.hstack((Vl, Hl))))

    Hscat = np.vstack((np.hstack((Hl,   Vl, Zs, Zs)),
                       np.hstack((Vl,   Hl, Vs, Zs)),
                       np.hstack((Zs, Vs.T, Hs, Zs)),
                       np.hstack((Zs,   Zs, Zs, Hl))))
    
    return Hlead, Hscat


def anderson_interaction(U0=4.0, Uhl=3.0, Uext=2.8,
                         right_lead=False, dtype=complex):
    """Create the interaction matrix, and shift for the metallic wire.

    For a description of the parameters, see PRL 102, 046802 (2009).

    U0 is a Hubbard U for double occupation of HOMO or LUMO.
    Uhl is a cross-correlation between occupation of HOMO and LUMO.   
    Uext is the strength of a nonlocal Coulomb operator giving the
    image charge interaction.
    """
    R = right_lead
    
    # 2 index Interaction matrix V_iijj
    # V_iijj = iint dr dr' / |r-r'| |i(r)|^2 |j(r')|^2
    iijj = np.zeros([4, 4], dtype)
    iijj = np.array([[   0,   Uext,   Uext,      0],
                     [Uext,     U0,    Uhl, Uext*R],
                     [Uext,    Uhl,     U0, Uext*R],
                     [   0, Uext*R, Uext*R,      0]], dtype)
    
    # Shift noninteracting Hamiltonian due to interactions,
    # to keep absolute HOMO and LUMO positions fixed
    # This is just the negative of the HF self-energy of the initial density
    # distribution D = diag(.5, 1, 0, .5)
    if right_lead:
        diag = [2 * Uext, 2 * Uext + U0, 2 * Uext + 2 * Uhl, 2 * Uext]
    else:
        diag = [2 * Uext, Uext + U0, Uext + 2 * Uhl, 0]
    shift = -np.diag(diag)
    
    return shift, iijj


def semicond_interaction(Uext, reduce=False, dtype=complex):
    """Create the interaction matrix, and shift for the semiconductor.

    If reduce is True, the interaction is in an optimized representation
    supplemented by an unpack matrix U_pq, which is otherwise None.

    Uext is the strength of a nonlocal Coulomb operator giving the
    image charge interaction.
    """
    V_qq = np.array([[-Uext, 0], [0, Uext]], dtype)
    U_pq = np.array(
        [[ 0, -1,  0,  0, -1,  0,  0,  0,  0,  0,  1,  0,  0,  0,  0,  1],
         [ 0, -1,  0,  0, -1,  0,  0,  0,  0,  0, -1,  0,  0,  0,  0, -1]],
        dtype).T / np.sqrt(2)
    if not reduce:
        V_qq = np.dot(U_pq, np.dot(V_qq, U_pq.T))
        U_pq = None
        # equivalent to
        # 0 1 2 3
        # V C H L
        # ijkl[0,1,2,2] = ijkl[1,0,2,2] = ijkl[0,1,3,3] = ijkl[1,0,3,4] = Uext
        # ijkl[2,2,0,1] = ijkl[2,2,1,0] = ijkl[3,3,0,1] = ijkl[3,3,1,0] = Uext

    shift = -2 * Uext * np.array([[0, 1, 0, 0],
                                  [1, 0, 0, 0],
                                  [0, 0, 0, 0],
                                  [0, 0, 0, 0]], dtype)
    return shift, V_qq, U_pq


def index2toindex4(iijj):
    """Convert interaction matrix from 2 to 4 index form.

    Convert 2-index interaction of the form::

      V_iijj = iint dr dr' / |r-r'| |i(r)|^2 |j(r')|^2    

    To a 4-index version of the form::
    
      V_ijkl = iint dr dr' / |r-r'| i(r) j*(r) k*(r') l(r')
    """
    Ni = len(iijj)
    ijkl = np.zeros([Ni**2, Ni**2], iijj.dtype)
    ijkl[::Ni+1, ::Ni+1] = iijj
    return ijkl
