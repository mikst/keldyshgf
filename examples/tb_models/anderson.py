#PBS -l nodes=1:ppn=8,walltime=00:25:00 -m ae

import numpy as np
from gpaw.mpi import world

# KGF stuff
from kgf.GreenFunctions import NonEqNonIntGreenFunction, NonEqIntGreenFunction
from kgf.ScatteringHamiltonianMatrix import ScatteringHamiltonianMatrix
from kgf.LeadHamiltonianMatrix import LeadHamiltonianMatrix
from kgf.CurrentCalculator import CurrentCalculator

# The self-energies
from kgf.selfenergies import NonEqConstantSelfEnergy
from kgf.selfenergies.GW2index import Hartree2index, Fock2index, GW2index
from kgf.selfenergies.HFSelfEnergy import Hartree, Fock
from kgf.selfenergies.GWSelfEnergy import NonEqGWSelfEnergy
from kgf.selfenergies.Born2SelfEnergy import Born2SelfEnergy

# The Anderson model matrices
from tb_models import anderson, anderson_interaction, index2toindex4

# Should the site couple to a Right lead?
right_lead = True

# Set parameters. See docstrings of tb_models module of details
Hlead, Hscat = anderson(
    e0  =  0.0, # Onsite energy on leads
    t   =-10.0, # Hopping on leads
    homo= -0.5, # Non-interacting HOMO
    lumo=  1.5, # Non-interacting LUMO
    th  =  0.4, # Coupling to electrodes
    right_lead=right_lead)

shift, iijj = anderson_interaction(
    U0  = 4.0, # Hubbard U on HOMO and LUMO
    Uhl = 3.0, # HOMO-LUMO interaction
    Uext= 2.8, # Cost of forming image charges
    right_lead=right_lead)

ijkl = index2toindex4(iijj)

# Choose energy grid fine enough to sample the sharpest features
de = 0.01
eta = 0.03 # must be >= 2 * de
energies = np.arange(-100, 100, de)
V = 10 * de # Bias. Must be integer times de

# Setup non-int Green function, g0
prin = 1
hmat = ScatteringHamiltonianMatrix(leftprincipallayer=prin,
                                   rightprincipallayer=prin,
                                   hamiltonianmatrix=Hscat)
left = right = LeadHamiltonianMatrix(principallayer=prin,
                                     hamiltonianmatrix=Hlead)
g0 = NonEqNonIntGreenFunction(hmat, left, right, E_Fermi=0., energies=energies)
g0.SetLeftChemicalPotential(V / 2.)
g0.SetRightChemicalPotential(-V / 2.)
g0.SetInfinitesimal(eta)

# Make self-energies
null = np.zeros((4, 4), dtype=complex)
se_null = NonEqConstantSelfEnergy(g0, null, 'null')
se_xc = NonEqConstantSelfEnergy(g0, shift, 'shift')

# two index version
se_H2 = Hartree2index(g0, iijj, initialhartree=null)
se_F2 = Fock2index(g0, iijj)
se_GW2 = GW2index(g0, iijj)

# four index version
se_H4 = Hartree(g0, ijkl, initialhartree=null)
se_F4 = Fock(g0, ijkl)
se_GW4 = NonEqGWSelfEnergy(g0, ijkl)
se_B4 = Born2SelfEnergy(g0, ijkl, polarization=True,  cross=True)
se_P4 = Born2SelfEnergy(g0, ijkl, polarization=True,  cross=False)
se_X4 = Born2SelfEnergy(g0, ijkl, polarization=False, cross=True)

# pulay parameters: tolerance, linear mixing, history
pulay = (0.05, 0.4, 1)

# Do the calculations

# DFT
gf = NonEqIntGreenFunction([se_null])
gf.WriteSpectralInfoToNetCDFFile('DFT.nc')
I = CurrentCalculator(gf).GetCurrent()
if world.rank == 0:
    print >> open('DFT_cond.txt', 'w'), I / V

# HF 2-index
gf = NonEqIntGreenFunction([se_xc, se_H2, se_F2])
gf.SelfConsistent(log='HF2.log', pulay=pulay)
gf.WriteSpectralInfoToNetCDFFile('HF2.nc')
I = CurrentCalculator(gf).GetCurrent()
if world.rank == 0:
    print >> open('HF2_cond.txt', 'w'), I / V

# HF 4-index
gf = NonEqIntGreenFunction([se_xc, se_H4, se_F4])
gf.SelfConsistent(log='HF4.log', pulay=pulay)
gf.WriteSpectralInfoToNetCDFFile('HF4.nc')
I = CurrentCalculator(gf).GetCurrent()
if world.rank == 0:
    print >> open('HF4_cond.txt', 'w'), I / V

# GW 2-index
gf = NonEqIntGreenFunction([se_xc, se_H2, se_F2, se_GW2])
gf.SelfConsistent(log='GW2.log', pulay=pulay)
gf.WriteSpectralInfoToNetCDFFile('GW2.nc')
I = CurrentCalculator(gf).GetCurrent()
if world.rank == 0:
    print >> open('GW2_cond.txt', 'w'), I / V

# GW 4-index
gf = NonEqIntGreenFunction([se_xc, se_H4, se_F4, se_GW4])
gf.SelfConsistent(log='GW4.log', pulay=pulay)
gf.WriteSpectralInfoToNetCDFFile('GW4.nc')
I = CurrentCalculator(gf).GetCurrent()
if world.rank == 0:
    print >> open('GW4_cond.txt', 'w'), I / V
