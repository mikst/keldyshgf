%\documentclass[preprint,prl,showpacs]{revtex4}
\documentclass[twocolumn,prl,showpacs]{revtex4}


\usepackage{psfrag}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}

\begin{document}
\title{Electronic correlations determine the conductance of molecular junctions}
\author{M. Strange}
\affiliation{Nanoscience center, Department of
Chemistry \\ University of Jyvaskyla, Jyvaskyla, Finland}
\author{C. Rostgaard}
\affiliation{Center for Atomic-scale Materials Design, Department of
Physics \\ Technical University of Denmark, DK - 2800 Kgs. Lyngby, Denmark}
\author{H. Hakkinen}
\affiliation{Department of
Physics and Chemistry \\ University of Jyvaskyla, Jyvaskyla, Finland}
\date{\today}
\author{K. S. Thygesen}
\affiliation{Center for Atomic-scale Materials Design, Department of
Physics \\ Technical University of Denmark, DK - 2800 Kgs. Lyngby, Denmark}


\begin{abstract}
The electronic conductance of a molecule connected to source and drain electrodes depends critically on the energetic position of the molecular frontier orbitals relative to the electrode Fermi level[1-3]. Density functional theory (DFT) is widely used to model charge transport at the molecular scale despite of its poor agreement with measured conductance data[4-10] which has long been hampering the interplay between theory and experiments in the field of molecular electronics. Here we show that first-principles calculations including electronic correlations explicitly via many-body perturbation theory reduce the conductance of the two prototype molecular junctions gold/1,4-benzenedithiol and gold/1,4-benzenediamine by a factor of five relative to the DFT values and significantly improves the agreement with experiments. The electronic correlations are found to play an important role for both the energy level alignment at the metal-molecule interface and the shape of the transmission resonances. Our calculations provide new insight into the atomic- and electronic structure of the molecular junctions.
\end{abstract}
\pacs{...}
\maketitle

\begin{table}
\begin{tabular} {l c c c c c}
\hline\hline
      & DFT-PBE & HF       & GW    & G$_0$W$_0$(PBE) & G$_0$W$_0$(HF)\\

BDA   & 0.021           & 0.0004          & 0.004           & 0.008           & 0.0025 \\
BDT+H & 0.054           & 0.0027          & 0.010           & 0.016           & 0.0085 \\
BDT   & 0.28\phantom{0} & 0.57\phantom{0} & 0.83\phantom{0} & 0.75\phantom{0} & 0.87\phantom{00} \\
\hline\hline
\end{tabular}
\caption{\label{table.conductance}Calculated conductance $(G_0)$}
\end{table}

\begin{table}
\begin{tabular} {l l c c c c}
\hline\hline
Molecule & Orbital & DFT-PBE & HF      & GW    & $\Delta E_{\text{tot}}$\\
\hline
BDA      & HOMO    &  -3.9   &-7.2    & -6.2  & -6.8 \\
         & LUMO    &  -1.0   &~4.0    & ~2.9  & ~2.3 \\
         & HL Gap  &  ~2.9   &11.2    & ~9.1  & ~9.1 \\
BDT+H    & LUMO    &  -5.0   &-8.0    & -6.9  & -7.5 \\
         & HOMO    &  -1.2   &~3.4    & ~2.2  & ~1.3 \\
         & HL Gap  &  ~3.8   &11.4    & ~9.1  & ~8.8 \\
 \hline\hline
\end{tabular}
\caption{\label{table.gas_levels} Calculated energy levels (eV)}
\end{table}


Molecular electronics holds the promise of continuing the miniaturization of electronic devices all the way down to the single molecule scale[7]. Over the last decade, advances in experimental techniques such as mechanically controlled break junctions and scanning probe microscopy, have allowed for fundamental studies of electron transport through individually contacted molecules across a wide range of transport regimes ranging from ballistic[11] over strong correlation Kondo[12] to vibration-assisted hopping[13]. In most cases, however, the transport mechanism is off-resonant tunneling through the energy gap between the highest occupied (HOMO) and lowest unoccupied (LUMO) molecular orbitals[1,14-16].  Despite years of intense research a large gap continues to exist between experiments and state of the art calculations based on DFT for molecular conduction in the off-resonance transport regime. The precise origin of this disagreement has been difficult to identify due to variations in the experimental data obtained by different groups, the lack of precise knowledge of the preferred atomic structures of the metal-molecule-metal contacts and the lack of high level theoretical methods which can assess the quality of the DFT approach.

Here we combine nonequilibrium Green function methods for electron transport with the self-consistent GW approximation for exchange and correlation, to establish a theoretical benchmark for the electronic structure and conductance of the gold/1,4-benzenedithiol (BDT)  and gold/1,4-benzenediamine (BDA) molecular junctions. For both systems, the calculated conductance is found to be in excellent agreement with available experimental data, although for BDT this can only be achieved by assuming that the sulfur linkers are chemically passivated. By analyzing the change in the frontier orbital energies as the molecule is brought from the gas-phase into the junction, we isolate the role of electronic correlations for the level alignment and identify the physics underlying the failure of the DFT approach.
 
In a first step we have performed DFT total energy calculations to obtain the most stable geometries for the BDT and BDA molecular junctions. The molecules are placed between two pyramidal shaped tips attached to Au(111) surfaces, and the atomic coordinates of the molecule and tips, and the distance between the Au surfaces are relaxed to the minimum energy configuration, see Figure 1. For BDT we find, in line with another recent DFT study[17], that it is energetically favorable for the hydrogen atoms to remain bonded to the sulfur as compared to binding to the gold tip or forming molecular hydrogen in the gas phase. However, as the energy differences are only 0.1-0.2 eV (see Supplementary Information) we have also considered BDT without hydrogen on sulfur (see Figure 2); this is the ���classical��� configuration which has been extensively studied in the past [8-10]. We note that very recently, new experimental evidence for the chemical structure of the gold-thiolate interface at the Au(111) surface [18] or at Au nanocluster surfaces [19,20] has emerged, pointing to the existence of polymeric -RS-Au(I)-SR-Au(I)-SR units where the formally oxidised Au(I) ���adatoms��� are chemically bound to thiolates and form a part of a more complicated molecular wire (see Ref. 21 and references therein). These extended wires are currently too challenging to treat at the self-consistent GW level. However, we argue here (see Supplementary Information) that the configuration shown in Figure 1 (left) serves as a satisfactory model of the electronic structure of these extended wires.
%Methods
Density functional theory calculations. Structural optimizations were carried out using the real space projector augmented wave method GPAW[29] with a grid spacing of 0.18 �� and the PBE xc-functional[25]. The unit cells contained the molecule between two pyramidal shaped tips (for structure in Figure 1) attached to seven layers of 4x4 Au atoms in the (111) direction. The Brillouin zone was sampled on a 1x4x4 k-point mesh and relaxation of the molecules and Au tips was continued until the residual force was below 0.02 eV/��.

Transport calculations. All transport calculations employ a basis set of numerical atomic orbitals corresponding to double-zeta plus polarization for the Au atoms and double-zeta for the molecules. The transmission function is obtained from the Meir-Wingreen formula[30]
\begin{equation}\label{eq.eq1}
T(\varepsilon) = \text{Tr}[G^r(\varepsilon)\Gamma_L(\varepsilon)G^a(\varepsilon)\Gamma_R(\varepsilon)]
\end{equation}
This formula was originally derived for non-interacting electrons, but we have checked that within our numerical accuracy the conductance obtained from this formula using the equilibrium GW Green function agrees with that obtained from the interacting current formula[30] using the nonequilibrium GW Green function in the low-bias limit. The retarded Green function of the extended molecule (consisting of the molecule plus part of the electrodes) is calculated from
\begin{equation}
G^r(\varepsilon)=[\varepsilon S - h_0 + v_{xc} - \Delta v_{H}-\Sigma_{LR}(\varepsilon)-\Sigma_{xc}(\varepsilon)]^{-1}
\end{equation}

Here S, H0, and Vxc are the overlap matrix, Kohn-Sham Hamiltonian and the local xc-potential in the atomic orbital basis, respectively. The lead self-energy, ���lead = ���L + ���R , incorporates the coupling to the left and right electrodes and is obtained using standard techniques [10]. The fourth term gives the change in the Hartree potential relative to the DFT Hartree potential already contained in H0. Finally, the last term is the many-body xc self-energy which in this work is either the bare exchange potential or the GW self-energy. As indicated both the Hartree potential and the xc self-energy depend on the Green function. The latter is evaluated fully self-consistently using a simple linear mixing of the Green functions. The GW self-energy is evaluated for the extended molecule, however, only the part corresponding to the molecule is used while the remaining part is replaced by the DFT xc-potential. This is done to include non-local image charge effects from the electrodes in the GW self-energy of the molecule while preserving a consistent description of all metal atoms at the DFT level. For the size of the extended molecule we have found it sufficient to include the two gold atoms contacting the molecules in the case of the structures in Figure 1, and the three Au atoms closest to the S atoms in the case of the structure in Figure 2. We note that this could be different for weakly coupled molecules where a larger area of the Au surface atoms are expected to be involved in the screening response.
Hartree-Fock and GW self-energy. The GW self-energy is given by
\begin{equation}
\Sigma_{GW, ij}(\varepsilon) = i\sum_{kl} \int \mathrm{d\varepsilon'}G_{kl}(\varepsilon-\varepsilon')W_{ki,lj}(\varepsilon'),
\end{equation}
where the indices refer to the atomic orbital basis. Note that this expression is written for quantities defined on the Keldysh time contour which is necessary out of equilibrium; the corresponding expressions for the real time components can be found in Ref. [27]. The sum over indices kl run over the extended molecule region to include non-local screening from the metal. The screened interaction is given by the matrix product $W(\varepsilon)=v/(1-vP)$, where the bare Coulomb interaction matrix in the atomic basis is
\begin{equation}
v_{ij, kl} = \int \int \mathrm{dr dr'}\phi_i^*(r) \phi_j(r) \phi_k(r') \phi_l(r')^* / |r-r'|
\end{equation}

%methods end



While DFT is a rigorous theory for the groundstate total energy, the underlying single-particle eigenvalues lack physical significance, and standard semilocal exchange-correlation (xc-)potentials often provide a rather poor description of the of the quasiparticle energies obtained from photo-emission experiments. In contrast, the many-body GW method has been very successfully applied to calculate quasiparticle excitation spectra of both solids and molecules[22,23], however, its application to quantum transport has so far been limited due the challenges related to the open boundary- and nonequilibrium aspects of the transport problem. In our GW-transport method the electronic structure of the metallic electrodes is described by the DFT effective potential while the electron-electron interactions within the molecule and across the metal-molecule interface are described at the GW level. Our GW calculations are performed fully self-consistently in a basis of optimized atomic orbitals, and the energy dependence of the GW self-energy is represented on a uniform grid along the real energy axis as explained in the Methods section and in Refs. [23,24]. See Supplementary Information for a discussion of the role of self-consistency.

The electron transmission probability of the relaxed junction geometries was calculated using three different methods: DFT with the PBE generalized gradient approximation [25], Hartree-Fock, and GW. The results are shown in the lower panels of Figures 1 and 2, and the corresponding conductances are summarized in Table 1. The conductance of BDA and BDT+H calculated with GW are in good agreement with the experimental values of 0.0064G0 reported for BDA in Ref. [14] and 0.011G0 reported for BDT  in Refs. [1,15,16]. In contrast, DFT and Hartree-Fock respectively overestimate and underestimate the experimental conductance. It is striking that the conductance of the ���classical��� BDT configuration (Figure 2) is predicted by all three methods to be significantly higher than the experimental value. These results strongly suggest that the structures probed in experiments on gold/BDT junctions involve a chemically passivated form of the thiolate linking group. It should be kept in mind that the experiments are performed in solution and at room temperature and thus the measured conductance values should not be considered as highly accurate references for theoretical calculations on idealized junctions. However, we do not expect this to have any influence on the conclusions drawn above.  

In Fig. 3 we show the calculated HOMO and LUMO energy levels of benzene-diamine and benzene-dithiol both for the gas-phase and in the junction. We focus first on the gas-phase results which are listed in Table 2. Due to lack of accurate experimental data we have performed DFT total energy calculations for the neutral, cation, and anion species which is known to produce good estimates of the experimental ionization and affinity levels of small molecules[23]. Relative to this reference, the DFT eigenvalues underestimate the HOMO-LUMO gap by 5-6 eV,  Hartree-Fock overestimates it by 2-3 eV,  while the gap obtained with GW lies within 0.5 eV. These trends are consistent with a recent study of ionization potentials of a large number of molecules[23]. We furthermore note that DFT predicts the LUMO to be a bound state thus incorrectly predicting the anions to be stable.


The orbital energies obtained from a GW calculation include the dynamical response of the electron system to the added electron/hole via the correlation part of the self-energy. In general this will introduce a shift of the occupied (unoccupied) states up (down) in energy relative to the bare Hartree-Fock energies, i.e. the energy gap will be reduced. When a molecule is brought from the gas-phase into a junction the electronic character of its environment changes from insulating to metallic. According to the above, and neglecting shifts due to hybridization, this should cause the gap to shrink. However, it has recently been shown for molecules weakly bonded to a metal surface, that this effect is completely missing in effective single-particle theories based on a (semi)local description of correlations [26-28]. As a result, in our DFT and Hartree-Fock calculations, the change in the frontier orbital energies induced by the coupling to gold is completely governed by the effect of hybridization which tends to open the gap by 0.7 eV for both molecules, see right panel of Figure 3. In contrast, the GW gap is reduced by 1 eV due to the enhanced screening in the contact. The net effect of the correlation in the GW self-energy is thus to reduce the HOMO-LUMO gap by 1.7 eV.  

From Figure 3 it follows that the HOMO level obtained from DFT lies approximately 0.7 eV above the GW value. This is in good agreement with a recent study of benzenediamine derivatives on gold(111) which found that DFT places the HOMO level around 1 eV too high as compared to UPS measurements[3]. The fact that DFT provides a better description of the energy levels of adsorbed rather than isolated molecules can be seen as a result of the metallic screening build into the local DFT xc-functional via its origin in the homogeneous electron gas. It should also be noted that the error (compared to GW) of the DFT eigenvalues are significantly larger for the LUMO than the HOMO in agreement with previous plane wave calculations for benzene/metal interfaces[19]. Finally, we note that while the level positions account for the main differences between the DFT and GW calculations, the shape of the transmission function also plays a role. In fact the energy dependence of the GW self-energy leads to a faster decay of the transmission function inside the HOMO-LUMO gap. As a consequence a DFT calculation with the HOMO and LUMO energies shifted to match those of the GW calculation, yields a conductance almost twice as high as the GW conductance (see Supplementary Information).  

In summary, our first-principles many-body calculations show that electronic correlations play an important role for the energy level alignment in molecular transport junctions. The fact that standard density functional theory overestimates molecular conductance was found to result from an incorrect description of the frontier orbital energies of the molecule and, to a lesser extent, an overestimation of the transmission resonance widths. Our results indicate that the sulfur atoms in the gold/1,4-benzenedithiol junction are chemically passivated which is consistent with recent results on the structure of the gold-thiolate interface.

\begin{section}{Acknowledgments}
We would like to thank Karsten Jacobsen and Angel Rubio for inspiring discussions. The authors acknowledge support from the Lundbeck Foundation���s Center for Atomic-scale Materials Design (CAMD), the Danish Center for Scientific Computing and the Academy of Finland. Part of the computations were done at the CSC - the Finnish IT Center for Science in Espoo.
\end{section}
\newpage

%%%%%%% References

%\bibliographystyle{apsrev}
\bibliographystyle{unsrt}
\begin{thebibliography}{23}
\expandafter\ifx\csname natexlab\endcsname\relax\def\natexlab#1{#1}\fi
\expandafter\ifx\csname bibnamefont\endcsname\relax
  \def\bibnamefont#1{#1}\fi
\expandafter\ifx\csname bibfnamefont\endcsname\relax
  \def\bibfnamefont#1{#1}\fi
\expandafter\ifx\csname citenamefont\endcsname\relax
  \def\citenamefont#1{#1}\fi
\expandafter\ifx\csname url\endcsname\relax
  \def\url#1{\texttt{#1}}\fi
\expandafter\ifx\csname urlprefix\endcsname\relax\def\urlprefix{URL }\fi
\providecommand{\bibinfo}[2]{#2}
\providecommand{\eprint}[2][]{\url{#2}}

\bibitem{cms_stokbro_2003}
  K.~Stokbro, J.~Taylor, M.~Brandbyge, J.-L.~Mozos, and P.~Ordej{\'o}n,
  Comp. Matt. Science {\bf 27}, 151 (2003).
\end{thebibliography}
\end{document}
