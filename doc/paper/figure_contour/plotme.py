import numpy as np
import pylab as plt

x = np.arange(-3, 1.0, 0.1)-0.05
y = np.arange(0, 4.0, 0.1)
data = np.loadtxt('T_HL.dat')
z = data[:,2].reshape((40, 40))
#pl.imshow(z, extent=(-3, 1, -1, 3))

CS = plt.contour(x, y, z, [0.004,  0.0064, 0.02],
                 linewidths=2, colors='black')
plt.plot([-0.6], [3.8], 'o', markersize=16, markerfacecolor='black',
         markeredgewidth=2)
plt.text(-0.43, 3.6, '7.2e-03', fontsize=16, color='black')
#plt.gca().get_frame().set_linewidth(4)
plt.clabel(CS, inline=1, fontsize=16, fmt='%0.1e')                
plt.xlabel('Shift of HOMO (eV)', fontsize=16)
plt.ylabel('Shift of LUMO (eV)', fontsize=16)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
#plt.title('Conductance for Au-BDA', fontsize=16)
plt.show()

