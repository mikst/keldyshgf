#!/usr/bin/env python
#PBS -l nodes=1:ppn=4
#PBS -l walltime=00:40:00
#PBS -m abe

from ase.parallel import paropen
from gpaw.mpi import world
import numpy as np
from kgf.GreenFunctions import NonEqNonIntGreenFunction, NonEqIntGreenFunction
from kgf.selfenergies.GW2index import Hartree2index, Fock2index, GW2index
from kgf.selfenergies.HFSelfEnergy import Hartree, Fock, hartree, fock
from kgf.selfenergies.GWSelfEnergy import NonEqGWSelfEnergy
from kgf.selfenergies import NonEqConstantSelfEnergy
from kgf.ScatteringHamiltonianMatrix import ScatteringHamiltonianMatrix
from kgf.LeadHamiltonianMatrix import LeadHamiltonianMatrix
from kgf.CurrentCalculator import CurrentCalculator
from kgf.Tools import dots

# Parameters
e0 = 0.0
t = -10.0
th = 0.8
homo = -1.0
lumo = 1.0
Uext = 2.8

# Mean-field Hamiltonian and overlap
Hlead = np.array([[ e0,  t],
                  [  t, e0]], complex)
Hscat = np.array([[e0,  t,    0,   0,   0,  0],
                  [ t, e0,   th,   th,  0,  0],
                  [ 0, th, homo,    0, th,  0],
                  [ 0, th,    0, lumo, th,  0],
                  [ 0,  0,   th,   th, e0,  t],
                  [ 0,  0,    0,    0,  t, e0]], complex)

# Test three equivalent versions:
# 1) two index,
# 2) 4 index packed, and
# 3) 4-index non-packed and rotated to non-ortho basis
test_systems = {1: 1,
                2: 1,
                3: 1,
                }

# Version 1: 2-index interaction
ijij = np.array([[   0, Uext, Uext,    0],
                 [Uext,    0,    0, Uext],
                 [Uext,    0,    0, Uext],
                 [   0, Uext, Uext,    0]], complex)

# Version 2: 4-index interaction packed
V_qq = np.array([[-Uext, 0], [0, Uext]], dtype=complex)
U_pq = np.array(
    [[-1,  0,  0,  0,  0,  1,  0,  0,  0,  0,  1,  0,  0,  0,  0, -1],
     [-1,  0,  0,  0,  0, -1,  0,  0,  0,  0, -1,  0,  0,  0,  0, -1]],
    dtype=complex).T / np.sqrt(2)

# Version 3: 4-index non pack and non-ortho
rot_in = np.array([[1.0, 0.0, 0.0, 0],
                   [0.0, 0.0, -2., 0],
                   [0.0, 2.0, 1.j, 0],
                   [0.0, 0.0, 0.0, 1]], complex)
Hscat3 = Hscat.copy()
Sscat3 = np.identity(6, complex)
Hscat3[1:-1, 1:-1] = dots(rot_in.T.conj(), Hscat3[1:-1, 1:-1], rot_in)
Sscat3[1:-1, 1:-1] = dots(rot_in.T.conj(), Sscat3[1:-1, 1:-1], rot_in)
ijkl = dots(U_pq, V_qq, U_pq.T).reshape(4, 4, 4, 4)
V3_pp = np.zeros_like(ijkl)
for (i, j, k, l), V in np.ndenumerate(ijkl):
    for p, q, r, s in np.ndindex(4, 4, 4, 4):
        V3_pp[p, q, r, s] += V * (rot_in[i, p] * rot_in[j, q].conj() *
                                  rot_in[k, r].conj() * rot_in[l, s])
V3_pp = V3_pp.reshape(16, 16)

# Choose energy grid fine enough to sample the sharpest features
eta = 0.04
de = 0.5 * eta
energies = np.arange(-100, 100, de)
pulay = (0.04, 0.5, 1)
left = right = LeadHamiltonianMatrix(principallayer=1,
                                     hamiltonianmatrix=Hlead)

# The results all methods should give
N = 4.00 # Number of electrons
E0_MF = [-1.89, 0.00, 0.00, 1.89] # Mean-field zero bias
E0_HF = [-2.21, 0.00, 0.00, 2.21] # Non-selfcon. Hartree-Fock
E0_GW = [-2.4, 0.00, 0.00, 2.4] # HF part of GW at selcon.
Niter = 7 # Number of iterations

if test_systems[1]:
    hmat = ScatteringHamiltonianMatrix(leftprincipallayer=1,
                                       rightprincipallayer=1,
                                       hamiltonianmatrix=Hscat)
    g0 = NonEqNonIntGreenFunction(hmat, left, right, E_Fermi=0.,
                                  energies=energies)
    g0.SetInfinitesimal(eta)

    # Various selfenergies
    se_hartree = Hartree2index(g0, ijij)
    se_fock = Fock2index(g0, ijij)
    se_gw = GW2index(g0, ijij, oversample=10)

    # Setup the interacting Green function
    gf = NonEqIntGreenFunction([se_hartree, se_fock, se_gw])
    H1 = gf.GetInteractingHamiltonian()
    if world.rank == 0:
        print np.sort(np.linalg.eigvals(Hscat[1:-1,1:-1]).real)
        print np.sort(np.linalg.eigvals(H1).real)
    
    # Converge Dyson equation
    gf.SelfConsistent(pulay=pulay)
    H1 = gf.GetInteractingHamiltonian()
    if world.rank == 0:
        print np.sort(np.linalg.eigvals(H1).real)
    gf.WriteSpectralInfoToNetCDFFile('GW1.nc')

if test_systems[2]:
    hmat = ScatteringHamiltonianMatrix(leftprincipallayer=1,
                                       rightprincipallayer=1,
                                       hamiltonianmatrix=Hscat)
    g0 = NonEqNonIntGreenFunction(hmat, left, right, E_Fermi=0.,
                                  energies=energies)
    g0.SetInfinitesimal(eta)

    # Various selfenergies
    se_hartree = Hartree(g0, V_qq, U_pq)
    se_fock = Fock(g0, V_qq, U_pq)
    se_gw = NonEqGWSelfEnergy(g0, V_qq, U_pq, oversample=10)

    # Setup the interacting Green function
    gf = NonEqIntGreenFunction([se_hartree, se_fock, se_gw])
    H2 = gf.GetInteractingHamiltonian()
    if world.rank == 0:
        print np.sort(np.linalg.eigvals(H2).real)

    # Converge Dyson equation
    gf.SelfConsistent(pulay=pulay)
    gf.WriteSpectralInfoToNetCDFFile('GW2.nc')
    H2 = gf.GetInteractingHamiltonian()
    if world.rank == 0:
        print np.sort(np.linalg.eigvals(H2).real)

if test_systems[3]:
    hmat = ScatteringHamiltonianMatrix(leftprincipallayer=1,
                                       rightprincipallayer=1,
                                       hamiltonianmatrix=Hscat3,
                                       overlapmatrix=Sscat3)
    g0 = NonEqNonIntGreenFunction(hmat, left, right, E_Fermi=0.,
                                  energies=energies)
    g0.SetInfinitesimal(eta)

    # Various selfenergies
    se_hartree = Hartree(g0, V3_pp)
    se_fock = Fock(g0, V3_pp)
    se_gw = NonEqGWSelfEnergy(g0, V3_pp, oversample=10)

    # Setup the interacting Green function
    gf = NonEqIntGreenFunction([se_hartree, se_fock, se_gw])
    H3 = gf.GetInteractingHamiltonian()
    S3 = gf.GetIdentityRepresentation()
    if world.rank == 0:
        print np.sort(np.linalg.eigvals(np.linalg.solve(S3, H3)).real)

    # Converge Dyson equation
    gf.SelfConsistent(pulay=pulay)
    gf.WriteSpectralInfoToNetCDFFile('GW3.nc')
    H3 = gf.GetInteractingHamiltonian()
    if world.rank == 0:
        print np.sort(np.linalg.eigvals(np.linalg.solve(S3, H3)).real)
