import numpy as np
from kgf.Tools import FermiDistribution, LambdaFromSelfEnergy, Dagger
from gpaw.mpi import world
from kgf.Parallel import get_collected_energies2
#from ase.io.pupynere import NetCDFFile
from scipy.io.netcdf import netcdf_file as NetCDFFile


class CurrentCalculator:
    def __init__(self, negreenfunction):
        self.SetNonEqGreenFunction(negreenfunction)

    def SetBiasArray(self,bias):
        self.bias=bias

    def GetBiasArray(self):
        return self.bias

    def SetNonEqGreenFunction(self,negf):
        self.negf=negf

    def GetNonEqGreenFunction(self):
        return self.negf

    def GetNonIntGreenFunction(self):
        if hasattr(self.GetNonEqGreenFunction(),'listofintselfenergies'):
            if self.GetNonEqGreenFunction().SpinPolarized():
                return self.GetNonEqGreenFunction().GetNonIntGreenFunction()[0]
            else:
                return self.GetNonEqGreenFunction().GetNonIntGreenFunction()
        else:
            return self.GetNonEqGreenFunction()

    def GetIntegrationGrid(self):
        # This is a subset of the proc's NU energy grid
        if not hasattr(self,'intgrid'):
            return self.GetNonEqGreenFunction().GetCurrentIntegrationGrid()
        else:
            return self.intgrid

    def FixRightPotential(self):
        self.fixright=1

    def GetLocalNUSubset(self,grid):
        energy=self.GetNonEqGreenFunction().GetNUEnergyList()
        locgrid=[]
        for en in grid:
            for en2 in energy:
                if abs(en-en2)<10e-7:
                    locgrid.append(en2)
        return locgrid

    def GetTransmissionFunction(self,grid):
        # grid must be a subset of the local nu grid
        gf=self.GetNonEqGreenFunction()
        nuenergies=gf.GetNUEnergyList()
        translst=np.zeros([len(nuenergies)],float)
        # The transmission function is only well defined for non-interacting electrons
        if hasattr(gf,'listofintselfenergies'):
            if gf.Correlated() is True:
                print "***************************************************************************"
                print "* ERROR: Transmission function only defined for non-interacting electrons *"
                print "***************************************************************************"
                return
        for en in grid:
            index=gf.GetIndex(nuenergies,en)
            lambda_l=LambdaFromSelfEnergy(gf.GetLeftLeadRetardedSelfEnergy(index))
            lambda_r=LambdaFromSelfEnergy(gf.GetRightLeadRetardedSelfEnergy(index))
            Gr,Gl=gf.GetRetardedAndLesser(index)            
            X=np.dot(Gr,lambda_l)
            Y=np.dot(np.conjugate(np.transpose(Gr)),lambda_r)               
            translst[index]=np.trace(np.dot(X,Y)).real
        return translst

    def WriteTransmissionFunctionToNetCDFFile(self,filename,grid):
        # Test if grid is a subset of the full uniform energy grid
        #for en in grid:
            #if not en in self.GetNonEqGreenFunction().GetFullEnergyList():
                #print "ERROR: Transmission function must be calculated on a subset of the energy grid"

        energies=self.GetNonEqGreenFunction().GetFullNUEnergyList()
        if world.rank == 0:
            file=NetCDFFile(filename,'w')
            file.createDimension("NEnergies",len(energies))
            energies_file=file.createVariable("Energies",'d',("NEnergies",))
            energies_file[:]=energies
        t=self.GetTransmissionFunction(self.GetLocalNUSubset(grid))
        if world.size>0:
            t_full=self.GetNonEqGreenFunction().GatherAndMerge(t)
        else:
            t_full=t
        if world.rank==0:
            t_file=file.createVariable("Transmission",'d',("NEnergies",))
            t_file[:]=t_full
            file.sync()
            file.close()

    def GetT(self):
        gf = self.GetNonEqGreenFunction()
        I = 0.0
        mu_l = gf.GetLeftChemicalPotential()
        mu_r = gf.GetRightChemicalPotential()
        mu_c = 0.0
        abs_mu_l = mu_l + gf.GetFermiLevel()
        abs_mu_r = mu_r + gf.GetFermiLevel()
        abs_mu_c = mu_c + gf.GetFermiLevel()
        tem = self.GetNonEqGreenFunction().GetTemperature()
        t = []
        energies = gf.GetNUEnergyList()
        for en in self.GetIntegrationGrid():
            index = gf.GetIndex(energies,en)
            weight = gf.GetNUWeights()[index]
            # NOTE: here one could add the infinitesimal to lambda. 
            # The infinitesimal can be seen
            # as an additional (wideband) coupling. If the third coupling is 
            # seen as a passive contact  it does not enter directly into the 
            # current. If it is seen as a background on the source-drain 
            # contacts, then eta should be added to both left and 
            # right contacts (even division).
            # NOTE: When the trace formula for non-interacting electrons 
            # is derived from the general current formula, there will be an 
            # additional term coming from the third electrode.
            lambda_l = LambdaFromSelfEnergy(
                        gf.GetLeftLeadRetardedSelfEnergy(index))
            lambda_r = LambdaFromSelfEnergy(
                        gf.GetRightLeadRetardedSelfEnergy(index))
            Gr, Gl = gf.GetRetardedAndLesser(index)
            Tmatrix = np.dot((lambda_l - lambda_r), Gl)
            temp = FermiDistribution(en - abs_mu_l, tem) * lambda_l - \
                   FermiDistribution(en - abs_mu_r, tem) * lambda_r
            Tmatrix = Tmatrix + np.dot(temp, (Gr - Dagger(Gr)))
            t.append((1.0j / 2.) * weight * np.trace(Tmatrix))
            # Below: calculation of the trace formula (incl. extra term from "third" electrode)
            #index=self.GetNonIntGreenFunction().GetIndex(energies,en)
            #eta=self.GetNonIntGreenFunction().GetIdentityRepresentation()*self.GetNonIntGreenFunction().GetInfinitesimal(index)
            #lambda_c=2*eta
            #extra=Numeric.matrixmultiply(Gr,lambda_l)
            #extra=Numeric.matrixmultiply(extra,Numeric.transpose(Numeric.conjugate(Gr)))
            #t2=Numeric.trace(Numeric.matrixmultiply(extra,lambda_r))
            #extra=Numeric.matrixmultiply(Gr,lambda_c)
            #extra=Numeric.matrixmultiply(extra,Numeric.transpose(Numeric.conjugate(Gr)))
            #temp2=FermiDistribution(en-abs_mu_c,0.5)*lambda_l-FermiDistribution(en-abs_mu_l,0.0)*lambda_l
            #temp3=FermiDistribution(en-abs_mu_r,0.0)*lambda_r-FermiDistribution(en-abs_mu_c,0.5)*lambda_r
            #extra=(1.0/2)*Numeric.trace(Numeric.matrixmultiply((temp2+temp3),extra))
            #print "Transmission (incl. extra term)",(t2+extra).real
            #print en,t[-1]
            if gf.SpinPolarized():
                Gr,Gl=gf.GetRetardedAndLesser(index,spin=1)
                Tmatrix=np.dot((lambda_l-lambda_r),Gl)
                Tmatrix=Tmatrix+np.dot(temp,(Gr-Dagger(Gr)))
                t[-1]=t[-1]*0.5+0.5*(1.0j/2)*weight*np.trace(Tmatrix)
        
        t = np.array(t)
        t = np.ascontiguousarray(t.real)
        return t

    def GetIntegrationEnergiesAndT(self):
        weight = self.GetNonEqGreenFunction().GetNUWeights()[0] #assume uniform
        t_e = self.GetT() / weight
        t_E = get_collected_energies2(t_e)
        energies_e = self.GetIntegrationGrid()
        energies_E = get_collected_energies2(energies_e)
        return energies_E, t_E

    def WriteIntegrationEnergiesAndT(self,fname):
        energies, t_e = self.GetIntegrationEnergiesAndT()
        if world.rank==0:
            fd = open(fname,'w')
            for e, t in zip(energies,t_e):
                print >>fd, e, t
            fd.close()

    def GetCurrent(self):
        t = self.GetT()
        I = np.sum(t)
        I = world.sum(I)
        return I

    def GetLeftCurrent(self):
        gf=self.GetNonEqGreenFunction()
        I=0.0
        mu_l=gf.GetLeftChemicalPotential()
        abs_mu_l=mu_l+gf.GetFermiLevel()
        tem=self.GetNonEqGreenFunction().GetTemperature()
        #I1=0.0
        t=[]
        energies=gf.GetNUEnergyList()
        for en in self.GetIntegrationGrid():
            index=gf.GetIndex(energies,en)
            weight=gf.GetNUWeights()[index]
            lambda_l=LambdaFromSelfEnergy(gf.GetLeftLeadRetardedSelfEnergy(index))
            Gr,Gl=gf.GetRetardedAndLesser(index)
            Tmatrix=np.dot(lambda_l,Gl)
            #I1+=np.trace(Tmatrix)
            temp=FermiDistribution(en-abs_mu_l,tem)*lambda_l
            Tmatrix=Tmatrix+np.dot(temp,(Gr-Dagger(Gr)))    
            t.append((1.0j)*weight*np.trace(Tmatrix))
            if gf.SpinPolarized():
                Gr,Gl=gf.GetRetardedAndLesser(index,spin=1)
                Tmatrix=np.dot(lambda_l,Gl)
                Tmatrix=Tmatrix+np.dot(temp,(Gr-Dagger(Gr)))        
                t[-1]=t[-1]*0.5+0.5*(1.0j)*weight*np.trace(Tmatrix)
        t = np.array(t).real
        I = np.sum(t)
        I = world.sum(I)
        return I

    def GetRightCurrent(self):
        gf=self.GetNonEqGreenFunction()
        I=0.0
        mu_r=gf.GetRightChemicalPotential()
        abs_mu_r=mu_r+gf.GetFermiLevel()
        tem=self.GetNonEqGreenFunction().GetTemperature()
        t=[]
        #I1=0.0
        energies=gf.GetNUEnergyList()
        for en in self.GetIntegrationGrid():
            index=gf.GetIndex(energies,en)
            weight=gf.GetNUWeights()[index]
            lambda_r=LambdaFromSelfEnergy(gf.GetRightLeadRetardedSelfEnergy(index))
            Gr,Gl=gf.GetRetardedAndLesser(index)
            Tmatrix=np.dot(lambda_r,Gl)
            #I1+=np.trace(Tmatrix)
            temp=FermiDistribution(en-abs_mu_r,tem)*lambda_r
            Tmatrix=Tmatrix+np.dot(temp,(Gr-Dagger(Gr)))    
            t.append((1.0j)*weight*np.trace(Tmatrix))
            if gf.SpinPolarized():
                Gr,Gl=gf.GetRetardedAndLesser(index,spin=1)
                Tmatrix=np.dot(lambda_r,Gl)
                Tmatrix=Tmatrix+np.dot(temp,(Gr-Dagger(Gr)))        
                t[-1]=t[-1]*0.5+0.5*(1.0j)*weight*np.trace(Tmatrix)
        t = np.array(t).real
        I = np.sum(t)
        I = world.sum(I)
        return I

    def UpdateToNetCDFFile(self,filename,sc=True,pulay=[[0.05,0.6,3]],sc_type=1,updateGW=True):
        if world.rank==0:
            # Initialize file
            file=NetCDFFile(filename,'w')
            file.createDimension("Nbias",len(self.GetBiasArray()))
            bias_file = file.createVariable("Bias",'d', ("Nbias",))
            bias_file[:] = self.GetBiasArray()
            sym_file=file.createVariable("Current",'d', ("Nbias",))
            left_file=file.createVariable("LeftCurrent",'d', ("Nbias",))
            right_file=file.createVariable("RightCurrent",'d', ("Nbias",))
            if not self.GetNonEqGreenFunction().SpinPolarized():
                occ_file=file.createVariable("Occupation",'d', ("Nbias",))
            else:
                occ_file_up=file.createVariable("Occupation_up",'d',("Nbias",))
                occ_file_down=file.createVariable("Occupation_down",'d',("Nbias",))
            for i in range(len(self.GetBiasArray())):
                sym_file[i]=0.0
                left_file[i]=0.0
                right_file[i]=0.0
                if self.GetNonEqGreenFunction().SpinPolarized():
                    occ_file_up[i]=0.0
                    occ_file_down[i]=0.0
                else:
                    occ_file[i]=0.0
            file.sync()
        gf=self.GetNonEqGreenFunction()
        # In self-consistent calculations we do not want to erase the nonintgf
        # for every new bias, since it probably defines a good starting point 
        # for the next iteration.
        # For non self-consistent calculations we need to erase it, because the
        # nonintgf should be recalculated ONCE from scratch at the new bias.
        if sc:
            unreg=False
        else:
            unreg=True
        #unreg=True
        count=0
        for bias in self.GetBiasArray():
            print "STARTING,proc",world.rank
            if hasattr(self,'fixright'):
                gf.SetLeftChemicalPotential(bias,unregister=unreg)
                gf.SetRightChemicalPotential(gf.GetRightChemicalPotential(),unregister=unreg)
            else:
                gf.SetLeftChemicalPotential(bias/2,unregister=unreg)
                gf.SetRightChemicalPotential(-bias/2,unregister=unreg)
            if sc:
                if sc_type==0:
                    gf.GetSelfEnergy('GW').SetActive(0)
                    gf.GetSelfEnergy('fock').SetActive(1)
                if sc_type==-1:
                    gf.GetSelfEnergy('GW').SetActive(0)
                    if 'xc' in gf.GetNamesOfInteractingSelfEnergies():
                        gf.GetSelfEnergy('xc').SetActive(0)
                print "Chemical potentials",gf.GetLeftChemicalPotential(),gf.GetRightChemicalPotential()
                gf.SelfConsistent(pulay)
                if sc_type==0:
                    gf.GetSelfEnergy('fock').SetActive(0)
                    gf.GetSelfEnergy('GW').SetActive(1)
                    if updateGW:
                        gf.GetSelfEnergy('GW').Update()
                    gf.UnregisterRetardedLesserLists()
                if sc_type==-1:
                    if 'xc' in gf.GetNamesOfInteractingSelfEnergies():
                        gf.GetSelfEnergy('xc').SetActive(1)
                    gf.GetSelfEnergy('GW').SetActive(1)
                    if updateGW:
                        gf.GetSelfEnergy('GW').Update()
                    gf.UnregisterRetardedLesserLists()
            current = self.GetCurrent()
            print "CURRENT", current
            leftcur = self.GetLeftCurrent()
            rightcur = self.GetRightCurrent()
            if gf.SpinPolarized():
                occ_up=np.sum(gf.GetOccupations()[0].real)
                occ_down=np.sum(gf.GetOccupations()[1].real)
            else:
                occ=np.sum(gf.GetOccupations().real)
            if world.rank==0:
                print "Symmetrized current", current
                sym_file[count]=current
                left_file[count]=leftcur
                right_file[count]=rightcur
                if gf.SpinPolarized():
                    occ_file_up[count]=occ_up
                    occ_file_down[count]=occ_down
                else:
                    occ_file[count]=occ
                file.sync()
            count += 1
        if world.rank==0:
            file.close()
