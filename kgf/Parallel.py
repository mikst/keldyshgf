from __future__ import print_function

import numpy as np
from gpaw.mpi import world


def AssignOrbitals2Procs(Norb):
    Ncpu = world.size
    N, R = divmod(Norb**2, Ncpu)
    per_cpu = N * np.ones(Ncpu, int)
    if R > 0:
        per_cpu[-R:] += 1
    if world.rank == 0 and world.size > 1:
        if R == 0:
            print ('%i**2 orbs with %i on each of the %i CPUs' % (Norb, N,
                                                                  Ncpu))
        else:
            print (('%i**2 orbs with %i on the first %i CPUs ' + 
                   'and one extra on the %i last') % (Norb, N, Ncpu - R, R))
    return per_cpu


def collect_orbitals(a_xo, coords, root=0):
    """Collect array distributed over orbitals to root-CPU.

    Input matrix has last axis distributed amongst CPUs,
    return is None on slaves, and the collected array on root.

    The distribution can be uneven amongst CPUs. The list coords gives the
    number of values for each CPU.
    """
    a_xo = np.ascontiguousarray(a_xo)
    if world.size == 1:
        return a_xo

    # All slaves send their piece to ``root``:
    # There can be several sends before the corresponding receives
    # are posted, so use syncronous send here
    if world.rank != root:
        world.ssend(a_xo, root, 112)
        return None

    # On root, put the subdomains from the slaves into the big array
    # for the whole domain on root:
    xshape = a_xo.shape[:-1]
    Norb2 = sum(coords) # total number of orbital indices
    a_xO = np.empty(xshape + (Norb2,), a_xo.dtype)
    o = 0
    for rank, norb in enumerate(coords):
        if rank != root:
            tmp_xo = np.empty(xshape + (norb,), a_xo.dtype)
            world.receive(tmp_xo, rank, 112)
            a_xO[..., o:o + norb] = tmp_xo
        else:
            a_xO[..., o:o + norb] = a_xo
        o += norb
    return a_xO


def collect_energies(a_ex, root=0):
    """Collect array distributed over energies to root-CPU.

    Input matrix has first axis distributed evenly amongst CPUs,
    return is None on slaves, and the collected array on root.

    As the distribution is even amongst CPUs, gather can be used here.
    """
    a_ex = np.ascontiguousarray(a_ex)
    if world.size == 1:
        return a_ex

    nenergies, xshape = a_ex.shape[0], a_ex.shape[1:]
    if world.rank == root:
        a_Ex = np.empty((world.size, nenergies) + xshape, a_ex.dtype)
        world.gather(a_ex, root, a_Ex)
        return a_Ex.reshape((world.size * nenergies,) + xshape)
    else:
        world.gather(a_ex, root)
        return None


def SliceAlongFrequency(a_eO, coords):
    """Slice along frequency axis.

    Input array has subset of energies, but full orbital matrix.
    Output has full energy, but subset of flattened orbital matrix.

    coords is a list of the number of orbital indices per cpu.
    """
    # flatten orbital axis
    a_eO = np.ascontiguousarray(a_eO).reshape(len(a_eO), -1)
    if world.size == 1:
        return a_eO

    o = 0
    for rank, norb in enumerate(coords):
        a_eo = a_eO[:, o:o + norb].copy()
        tmp = collect_energies(a_eo, root=rank)
        if rank == world.rank:
            a_Eo = tmp
        o += norb
    return a_Eo


def SliceAlongOrbitals(a_Eo, coords):
    """Slice along orbital axis.

    Input has full energy, but subset of flattened orbital matrix.
    Output array has subset of energies, but full orbital matrix.

    coords is a list of the number of orbital indices per cpu.
    """
    Norb = np.sqrt(sum(coords)).astype(int)
    nenergies = len(a_Eo) // world.size # energy points per processor.
    a_Eo = np.ascontiguousarray(a_Eo)
    if world.size == 1:
        return a_Eo.reshape(nenergies, Norb, Norb)

    for rank in range(world.size):
        a_eo = a_Eo[rank * nenergies:(rank + 1) * nenergies, :].copy()
        tmp = collect_orbitals(a_eo, coords, root=rank)
        if rank == world.rank:
            a_eO = tmp.reshape(nenergies, Norb, Norb)
    return a_eO


def get_points_per_core(a_ex):
    """
        The number of energy points per processor is
        returned as a list to rank=0. 
    """
    if world.rank!=0:
        n = np.array((len(a_ex),), int)
        world.ssend(n, 0, 112)

    if world.rank==0:
        ns = [len(a_ex),]
        for rank in range(1, world.size):
            n = np.zeros(1, int)
            world.receive(n, rank, 112)
            ns.append(n.copy())
        ns = np.asarray(ns)
        return ns
    else:
        return None


def get_collected_energies2(a_ex):
    """
        Collect an array which is parallelized over energy points.
        Example. rank=0: a_ex = (0,1,2) 
                 rank=1: a_ex = (5,8)
                  
                 returns (0,1,2,5,8) on rank=0
                 and None on rank!=0
    """
    ns = get_points_per_core(a_ex)
    if world.rank!=0:
        world.ssend(a_ex, 0, 112) 

    if world.rank==0:
        N = np.sum(ns)
        a_Ex = np.empty((N,) + a_ex.shape[1:], a_ex.dtype)
        a_Ex[:ns[0]] = a_ex
        for rank in range(1, world.size):
            e1 = sum(ns[:rank])
            e2 = e1 + ns[rank]
            world.receive(a_Ex[e1:e2], rank, 112)
        return a_Ex
    else:
        return None

