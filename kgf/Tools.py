import numpy as np
from kgf.Hilbert import hilbert
from gpaw.utilities.blas import gemm, gemv, rotate
from scipy.io.netcdf import netcdf_file as NetCDFFile

#from ase.io.pupynere import NetCDFFile


def hartree_product(G, V,
                    pre=1.0, out=None, rot=None,
                    work1=None, work2=None):
    """Perform matrix product sum_kl G_kl V_ij,kl
    
    If rot is specified, it is used to rotate V, such that
    rot . V . rot^dag is used instead of V.

    Arguments:

    G: ndarray
       input matrix of shape Ni x Ni
    V: ndarray
       input matrix of shape Ni**2 x Ni**2

    Optional parameters:
    
    pre: float or complex (default: 1.0)
         the output is multiplied by this prefactor
    rot: ndarray (default: None)
         if specified, V is replaced by rot . V . rot^dag
         rot array should have shape  len(G)**2 x len(V)
    out: ndarray (default None)
         if specified, this is used for the output array, otherwise it is
         created. A reference to out is always returned
    work: ndarray (default None)
          if rot is specified, it can be advantageous to specify the
          required work arrays of arbitrary shape and *total* length
          len(G)**2 * len(V)

    If rot is None, the out array generally has the elements::

      out_ij = pre * sum_kl G_kl V_ij,kl

    Else::
    
      out_ij = pre * sum_klqq' G_kl rot_ij,q V_qq' (rot^dag)_q',kl
    """
    assert G.dtype == V.dtype
    if out is None:
        out = np.zeros_like(G)

    Ni = len(G)
    if rot is None:
        gemv(pre, V, G.reshape(-1), 0.0, out.reshape(-1), 't')
    else:
        Nq = len(V)
        if work1 is None:
            work1 = np.zeros(Nq, dtype=G.dtype)
        if work2 is None:
            work2 = np.zeros(Nq, dtype=G.dtype)

        # work1_q = sum_kl rot^conj_kl,q G_kl
        gemv(1.0, rot.conj(), G.reshape(-1), 0.0, work1, 'n')

        # work2_q = sum_q' V_qq' work1_q'
        gemv(1.0, V, work1, 0.0, work2, 't')
        
        # out_ij = sum_q rot_ij,q work2_q
        gemv(pre, rot, work2, 0.0, out.reshape(-1), 't')
    return out


def exchange_product(G, V,
                     pre=1.0, out=None, rot=None,
                     work1=None, work2=None):
    """Perform matrix product sum_kl G_kl V_ik,jl

    If rot is specified, it is used to rotate V, such that
    rot . V . rot^dag is used instead of V.

    Arguments:

    G: ndarray
       input matrix of shape Ni x Ni
    V: ndarray
       input matrix of shape Ni**2 x Ni**2

    Optional parameters:
    
    pre: float or complex (default: 1.0)
         the output is multiplied by this prefactor
    rot: ndarray, len(G)**2 x len(V) (default: None)
         if specified, V is replaced by rot . V . rot^dag
    out: ndarray (default None)
         if specified, this is used for the output array, otherwise it is
         created. A reference to out is always returned
    work: ndarray (default None)
          if rot is specified, it can be advantageous to specify the
          required work arrays of arbitrary shape and *total* length
          len(G)**2 * len(V)

    If rot is None, the out array generally has the elements::

      out_ij = pre * sum_kl G_kl V_ik,jl

    Else::
    
      out_ij = pre * sum_klqq' G_kl rot_ik,q V_qq' (rot^dag)_q',jl
    """
    assert G.dtype == V.dtype
    if out is None:
        out = np.zeros_like(G)

    Ni = len(G)
    if rot is None:
        V_ikjl = V.reshape(Ni, Ni, Ni, Ni)
        for i, k in np.ndindex(Ni, Ni):
            gemv(pre, V_ikjl[i, k], G[k], 1.0, out[i])
    else:
        Nq = len(V)
        if work1 is None:
            work1 = np.zeros(Ni**2 * Nq, dtype=G.dtype)
        if work2 is None:
            work2 = np.zeros(Ni**2 * Nq, dtype=G.dtype)

        # work2_q,jl = sum_q' V_qq' rot^dag_q',jl
        gemm(1.0, rot, V, 0.0, work2.reshape(Nq, -1), 'c')

        # work1_k,qj = sum_l A_kl work2_qj,l
        gemm(1.0, work2.reshape(-1, Ni),
             G, 0.0, work1.reshape(Ni, -1), 't')
        # out_ij = sum_kq rot_i,kq work1_kq,j
        gemm(pre, work1.reshape(-1, Ni),
             rot.reshape(Ni, -1), 0.0, out)
    return out


def polarization_product(G, Gd,
                         pre=1.0, out=None, rot=None, rotd=None,
                         work1=None, work2=None):
    """Perform kronecker matrix product (G otimes Gd)

    If rot is specified, it is used to rotate the output, such that
    rot^dag . out . rot is returned instead of out

    Arguments:

    G: ndarray
       input matrix of shape Ni x Ni
    Gd: ndarray
        input matrix of shape Ni x Ni

    Optional parameters:
    
    pre: float or complex (default: 1.0)
         the output is multiplied by this prefactor
    rot: ndarray (default: None)
         if specified, this is used to rotate output.
         array should have shape len(G)**2 x len(V)
    rotd: ndarray (default: None)
          if specified, this is used to rotate output.
          array should be the Hermitian conjugate of rot
    out: ndarray (default None)
         if specified, this is used for the output array, otherwise it is
         created. A reference to out is always returned
    work: ndarray (default None)
          if rot is specified, it can be advantageous to specify the
          required work arrays of arbitrary shape and *total* length
          len(G)**2 * len(V)

    If rot is None, the out array generally has the elements::

      out_ij,kl = pre * G_ik Gd_jl

    Else::
    
      out_qq' = pre * sum_ijkl Ud_q,ij G_ik Gd_jl U_kl,q'
    """
    Ni = len(G)
    if rot is None:
        if out is None:
            out = np.zeros((Ni**2, Ni**2), dtype=G.dtype)
        #out[:] = pre * np.kron(G, Gd)
        out_ijkl = out.reshape(Ni, Ni, Ni, Ni)
        for i, k in np.ndindex(Ni, Ni):
            out_ijkl[i, :, k, :] = pre * G[i, k] * Gd
    else:
        Nq = rot.shape[1]
        if rotd is None: # XXX Grr, it should be possible to avoid rotd!
            rotd = np.np.ascontiguousarray(np.conj(rot.T))
        else:
            assert rot.shape == rotd.shape[::-1]
        
        if out is None:
            out = np.zeros((Nq, Nq), dtype=G.dtype)

        # Temporary work arrays
        if work1 is None:
            work1 = np.zeros_like(rot)
        if work2 is None:
            work2 = work1.copy()

        # Contiguous views with packed format unfolded, p -> ii
        work1 = work1.reshape(Ni, Ni, Nq)
        work2 = work2.reshape(Ni, Ni, Nq)
        rot = rot.reshape(Ni, Ni, Nq)

        # work1_kj,q' = sum_l B_jl U_kl,q'
        for work1_iq, U_iq in zip(work1, rot):
            gemm(1.0, U_iq, Gd, 0.0, work1_iq)

        # work2_ij,q' = sum_k A_ik work1_kj,q'
        gemm(1.0, work1, G, 0.0, work2)

        # out_qq' = sum_ij rot^dag_q,ij work2_ij,q'
        gemm(pre, work2.reshape(-1, Nq), rotd, 0.0, out)

    return out



def exchange_product_old(out_ii, pre, A_ii, U_pq, B_qq, Ud_qp,
                      work1_pq=None, work2_qp=None):
    """Perform matrix product out = pre * sum_kl A_kl [U B Ud]_ik,jl.

    The out array has elements::

      out_ij = pre * sum_klqq' A_kl U_ik,q B_qq' Ud_q',jl
    """
    Nq = len(B_qq)
    Ni = len(A_ii)
    if work1_pq is None:
        work1_pq = np.zeros_like(U_pq)
    if work2_qp is None:
        work2_qp = np.zeros((Nq, Ni**2), A_ii.dtype)

    # Different views into the work arrays
    work1_iqi = work1_pq.reshape(Ni, Nq, Ni)
    work2_qii = work2_qp.reshape(Nq * Ni, Ni)
    U_iiq = U_pq.reshape(Ni, Ni * Nq)
    
    gemm(1.0, Ud_qp, B_qq, 0.0, work2_qp) # work2_q,jl = sum_q' B_qq' Ud_q',jl
    gemm(1.0, work2_qii, A_ii, # work1_kqj = sum_l A_kl work2_q,jl
         0.0, work1_iqi.reshape(Ni, Nq * Ni), 't')
    gemm(pre, work1_iqi.reshape(Ni * Nq, Ni),
         U_iiq, 0.0, out_ii) # out_ij = sum_kq U_ik,q work1_kqj


#def apply_kron_simple(out_qq, pre, Ud_qp, A_ii, B_ii, U_pq,
#                work_pq=None):
#    """Perform matrix product out = pre * Ud (A otimes B) U.
#
#    The out array has elements::
#
#      out_qq' = pre * sum_ijkl Ud_q,ij A_ik B_jl U_kl,q'
#    """
#    if work_pq is None:
#        work_pq = np.zeros_like(U_pq)
#    AB_pp = np.ascontiguousarray(np.kron(A_ii, B_ii))
#    gemm(1.0, U_pq, AB_pp, 0.0, work_pq)
#    gemm(pre, work_pq, Ud_qp, 0.0, out_qq)
#
#
#def apply_kron_old(out_qq, pre, Ud_qp, A_ii, B_ii, U_pq,
#                work1_pq=None, work2_pq=None):
#    """Perform matrix product out = pre * Ud (A otimes B) U.
#
#    The out array has elements::
#
#      out_qq' = pre * sum_ijkl Ud_q,ij A_ik B_jl U_kl,q'
#    """
#    Nq = len(out_qq)
#    Ni = len(A_ii)
#
#    # Temporary work arrays
#    if work1_pq is None:
#        work1_pq = np.zeros_like(U_pq)
#    if work2_pq is None:
#        work2_pq = np.zeros_like(U_pq)
#
#    # Contiguous views with packed format unfolded, p -> ii
#    work1_iiq = work1_pq.reshape(Ni, Ni, Nq)
#    work2_iiq = work2_pq.reshape(Ni, Ni, Nq)
#    U_iiq = U_pq.reshape(Ni, Ni, Nq)
#
#    # work1_kj,q' = sum_l B_jl U_kl,q'
#    for work1_iq, U_iq in zip(work1_iiq, U_iiq):
#        gemm(1.0, U_iq, B_ii, 0.0, work1_iq)
#
#    # work2_ij,q' = sum_k A_ik work1_kj,q'
#    gemm(1.0, work1_iiq, A_ii, 0.0, work2_iiq)
#
#    # out_qq' = sum_ij Ud_q,ij work1_ij,q'
#    gemm(pre, work2_pq, Ud_qp, 0.0, out_qq)
#
#
#def apply_kron(A, B, U, out, work=None):
#    """Matrix product of (A otimes B) U
#       Parameters:
#       A: (N, N) ndarray
#       B: (N, N) ndarray
#       U: (N**2, M) ndarray
#       out: (N**2, M), ndarray, intent out
#            The result is written to out
#       work: (N**2, M), ndarray
#            Working array
#
#       The out array has elements:
#       out_ij,q = sum_kl A_ik B_jl U_kl,q
#       
#       """
#    if work is None:
#        work = np.zeros_like(U)
#    n = A.shape[0]
#    n2 = U.shape[0] # n2 = n**2
#    # work <- (I kron B) U 
#    for i in range(n):
#        U_block = U[i*n: (i+1)*n]
#        work_block = work[i*n: (i+1)*n]
#        gemm(1.0, U_block, B, 0.0, work_block)
#    # out <- (A kron I) work 
#    work = work.reshape(n, -1)
#    out = out.reshape(n, -1)
#    gemm(1.0, work, A, 0.0, out)
#    out = out.reshape(n2, -1)


def dots(*args):
    x = args[0]
    for M in args[1:]:
        x = np.dot(x, M)
    return x        


def PermuteMatrix(M, axes):
    """ Permutes the basis vectors among themselves to produce
    a new matrix. 'permutation' is a list of indices"""
    assert M.ndim == 2 and M.shape[0] == M.shape[1]
    return M.take(axes, 0).take(axes, 1)


def RotateMatrix(h, U):
    """ U contains the new basis as its columns"""
    return np.dot(U.T.conj(), np.dot(h, U))


#def FFTShift(array):
#    return np.fft.ifftshift(array, axes=[0])


def TranslateAlongAxis0(array, translation):
    """Optimized method for translating along axis 0"""
    newarray = array.copy()
    if translation == 0:
        return newarray
    newarray[:translation] = array[-translation:]
    newarray[translation:] = array[:-translation]
    return newarray


def TranslateAlongAxis0NonPeriodic(array, translation):
    """Optimized method for translating along axis 0"""
    newarray = np.zeros_like(array)
    if translation == 0:
        return newarray
    if translation > 0:
        newarray[translation:] = array[:-translation]
    else:
        newarray[:translation] = array[-translation:]
    return newarray


def Dagger(matrix):
    return np.conj(np.swapaxes(matrix, 0, 1))


def LambdaFromSelfEnergy(selfenergy):
    return 1.j * (selfenergy - Dagger(selfenergy))


def FermiDistribution(energy, kBT=0.):
    if kBT == 0.:
        return 0.5 * (1. - np.sign(energy))
    return 1. / (np.exp(np.clip(energy / kBT, -100, 100)) + 1)


#def WriteToNetCDFFile(filename, x, y, dtype=float):
#    file = NetCDFFile(filename,'w')
#    file.createDimension('Npoints', len(x))
#    myvara = file.createVariable('xvalues', dtype, ('Npoints',))
#    myvara[:] = x
#    myvarb = file.createVariable('yvalues', dtype, ('Npoints',))
#    myvarb[:]=y
#    file.flush()
#    file.close()


#def ReadFromNetCDFFile(filename):
#    file = NetCDFFile(filename, 'r')
#    x = file.variables['xvalues'][:]
#    y = file.variables['yvalues'][:]
#    return x, y


#def WriteMatrixToNetCDFFile(filename, m):
#    file = NetCDFFile(filename, 'w')
#    file.createDimension('Complex', 2)
#    file.createDimension('NBasisFunctions',len(m))
#    matrix = file.createVariable('matrix', 'd', (
#        'NBasisFunctions', 'NBasisFunctions', 'Complex'))
#    matrix[..., 0] = m.real
#    matrix[..., 1] = m.imag
#    file.flush()
#    file.close()


#def ReadMatrixFromNetCDFFile(filename):
#    file = NetCDFFile(filename, 'r')
#    matrix = file.variables['matrix'][:]
#    m = np.empty(matrix.shape[:2], complex)
#    m.real = matrix[..., 0]
#    m.imag = matrix[..., 1]
#    return m


def Convolution(f,g,zero_index,axis=0):
    # Convention: Let G(w) be a function in frequency space, e.g. a GF.
    # Then the function in time-space is G(t)=de/(2*pi)*FFT.fft(G(w),axis=0)
    # where 'de' is the frequency (energy) spacing.
    # Conversely, if G(t) is given, then
    #         G(w)=((2*pi)/de)*FFT.inverse_fft(G(t),axis=0)
    # Calculates the convolution (f * g)(x) = (1/(2*pi))\int dy f(y)g(x-y).
    # This is the Fourier transform of f(t)g(t).
    # If f and g are matrices the integral is performed along the specified
    # axis.
    # 'zero_index' is the index number of the zero point.
    # 'time_translate' translates the two functions relative to each other by 
    # the specified number og grid points.
    f_fft=np.fft.fft(f,axis=axis)
    g_fft=np.fft.fft(g,axis=axis)
    product_fft=f_fft*g_fft
    pre=1.0/(2*np.pi)
    return pre*TranslateAlongAxis0(np.fft.ifft(product_fft,axis=0),-zero_index)


#def DirectConvolution(f,g,grid,de):
#    # Calculates (1/pi)\int dy f(y)g(x-y) directly (no FFT).
#    # 'grid' is in integer points. 'de' is energy spacing. 
#    s=[len(grid)]+list(f.shape[1:])
#    h=np.zeros(s,complex)
#    for i in range(len(grid)):
#        f_translate=TranslateAlongAxis0(f,-grid[i])
#        h[i]=np.sum(f_translate*g)*de 
#    return h/np.pi


def AntiConvolution(f,g,zero_index,axis=0):
    # Calculates the anti-convolution (f ** g)(x)=(1/(2*pi))\int dy f(x+y)g(y).
    # This is the Fourier transform of f(t)g(-t).
    # If f and g are matrices the integral is performed along the specified
    # axis.
    # 'zero_index' is the index of the zero point.
    f_fft=np.fft.fft(f,axis=axis)
    g_fft=np.conjugate(np.fft.fft(np.conjugate(g),axis=axis))
    product_fft=f_fft*g_fft
    pre=1.0/(2*np.pi)
    return pre*TranslateAlongAxis0(np.fft.ifft(product_fft,axis=0),zero_index)


def HilbertTransform(f, energy):
    #return hilbert(f, nfft=len(energy), kerneltype='simple')
    return hilbert(f, nfft=len(energy), kerneltype='interpolate')


def LinearInterpolation(a1, indices, size):
    # Interpolate from array a1 to array a2. a1 is sampled on indices
    # Treat first index as special case

    # Don't do anything if a1 and a2 are identical
    if len(a1) == size:
        return a1
    assert 0 
    s = (size,) + a1.shape[1:]
    a2 = np.zeros(s, a1.dtype)
    slope0 = (a1[1] - a1[0]) / (1.0 * indices[1] - indices[0])    
    for i in range(indices[0]):
        a2[i] = a1[0] + slope0 * (1.0*i-indices[0])
    for i in range(1, len(indices)):
        for j in range(indices[i-1], indices[i]):
            a2[j] = a1[i-1]+(a1[i]-a1[i-1])*(1.0*j-indices[i-1])/(
                indices[i]-indices[i-1])
    slope1 = (a1[-1]-a1[-2])/(1.0*indices[-1]-indices[-2])
    for i in range(indices[-1],a2.shape[0]):
        a2[i] = a1[-1] + slope1 * (1. * i - indices[-1])
    return a2


def transmission(Gr, sigmar_l, sigmar_r):
    """Calculate transmission
    T = Tr[G^r Lambda^L G^a Lambda^R]
    Gr: retarded Green function of scattering region
    sigmar_l: Left lead retarded selfenergy
    sigmar_r: Right lead retarded selfenergy
    """
    lambda_l = 1.j * (sigmar_l - sigmar_l.T.conj())
    lambda_r = 1.j * (sigmar_r - sigmar_r.T.conj())
    return dots(Gr, lambda_l, Gr.T.conj(), lambda_r).trace().real


#def ExtendGridN2(grid, m):
#    N = m * len(grid)
#    # Make sure that grid lenght is a power of 2
#    count = 0
#    while N != 1:
#        if N%2==1:
#            N+=1
#        N/=2
#        count+=1
#        print count
#    N = 2**(count) 
#    start=grid[0]
#    grid2=np.arange(0,N)*(grid[1]-grid[0])+start
#    for i in range(len(grid2)):
#        grid2[i]=round(grid2[i],8)
#    return grid2


def ExtendArray(f, grid1, grid2, diff, a=0.0):
    # Extends f (on grid1) by a (on grid2) along axis 0
    dim = f.shape
    s = [len(grid2)] + list(f.shape[1:])
    h = a * np.ones(s, complex)
    h[diff:diff + len(grid1)] = f
    return h


def GetRetardedFunctionFromAInTime(A):
    # Calculates the retarded function, F^r, in time domain from 
    # the spectral function: A = i(F^r-F^a)=i(F^>-F^<) given in ENERGY
    # G^r(t)=theta(-t)[G^>(t)-G^<(t)].
    # Since the function (A) is so localized in frequency space there is no
    # effect of the end point corrections here.     
    A_time=np.fft.fft(A, axis=0)
    A_time[len(A_time) / 2:] = 0 #* A_time[len(A_time)/2:]
    return -1.j * A_time


def GetRetardedFunctionFromA(A, grid, fourierdata=None):
    from kgf.FourierTransform import fourierintegral
    A_time = GetRetardedFunctionFromAInTime(A)      
    A_time2 = (1. / (grid[1]  -grid[0])) * fourierintegral(
        A_time, grid[1] - grid[0], sign=1, data=fourierdata) / len(grid)
    return A_time2
