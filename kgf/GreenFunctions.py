from __future__ import print_function
import numpy as np

from gpaw.mpi import world
from scipy.io.netcdf import netcdf_file as NetCDFFile
#from ase.io.pupynere import NetCDFFile

from kgf.Tools import \
     FermiDistribution, LambdaFromSelfEnergy, LinearInterpolation, \
     PermuteMatrix, RotateMatrix, transmission, gemm, dots
from kgf.memory import maxrss
from kgf.Parallel import collect_energies, SliceAlongFrequency, \
     SliceAlongOrbitals, AssignOrbitals2Procs
from kgf.SelfConsistency import SelfConsistent
        

class GreenFunction:
    """Possible class attributes:

    self:
    ----
    shape
    hamiltonian
    identity
    inf
    energy
    greenfunctionflag
    representation
    """
    def __init__(self, hamiltonian=None, identity=None,
                 energy=None, infinitesimal=1.e-4):
        if hamiltonian is not None:
            # To avoid that SetHamiltonianRepresentation
            # of call ScatteringGreenFUnction is used    
            GreenFunction.SetHamiltonianRepresentation(self, hamiltonian)
        if identity is not None:
            self.SetIdentityRepresentation(identity)
        if energy is not None:
            self.SetEnergy(energy)
        self.SetInfinitesimal(infinitesimal)

    def SetHamiltonianRepresentation(self, hamiltonian):
        self.hamiltonian = hamiltonian
        # This will force an update
        self.UnregisterRepresentation()

    def GetHamiltonianRepresentation(self):
        return self.hamiltonian

    def GetMatrixDimension(self):
        if not hasattr(self, 'shape'):
            self.shape = self.GetHamiltonianRepresentation().shape
        return self.shape

    def SetIdentityRepresentation(self, identity):
        self.identity = identity
        # This will force an update
        self.UnregisterRepresentation()

    def IsOrthonormal(self):
        s = self.GetIdentityRepresentation()
        m = abs(s - np.identity(len(s))).max()
        return m < 0.0001

    def GetIdentityRepresentation(self):
        return self.identity

    def SetInfinitesimal(self, infinitesimal):
        self.inf = infinitesimal
        # This will force an update
        self.UnregisterRepresentation()

    def GetInfinitesimal(self):
        if not hasattr(self, 'inf'):
            self.inf = 1.0e-4
        return self.inf

    def SetEnergy(self, energy):
        self.energy = energy
        # This will force an update
        self.UnregisterRepresentation()

    def GetEnergy(self):
        return self.energy

    def GetComplexEnergy(self):
        return self.GetEnergy() + 1.j * self.GetInfinitesimal()

    def ReferenceGreenFunction(self):
        # Default is *not* to reference Green function
        if not hasattr(self, "greenfunctionflag"):
            self.ReferenceGreenFunctionOff()
        return self.greenfunctionflag

    def ReferenceGreenFunctionOn(self):
        self.greenfunctionflag = 1

    def ReferenceGreenFunctionOff(self):
        self.greenfunctionflag = 0

    def SetRepresentation(self, representation):
        self.representation = representation

    def GetRepresentation(self):
        # If the representation is not set: update
        if not hasattr(self, "representation"):
            self.UpdateRepresentation()
        representation = self.representation
        
        # If not ReferenceGreenFunction: Delete the representation
        if not self.ReferenceGreenFunction():
            self.UnregisterRepresentation()
        return representation

    def UpdateRepresentation(self):
        hamiltonian = self.GetHamiltonianRepresentation()
        identity = self.GetIdentityRepresentation()
        # No copy of the matrix is performed: input -> output
        #greenfunction = ArrayTools.Inverse(
        #    self.GetComplexEnergy() * identity - hamiltonian, copymatrix=0)
        greenfunction = np.linalg.inv(
                self.GetComplexEnergy() * identity - hamiltonian)
        self.SetRepresentation(greenfunction)

    def UnregisterRepresentation(self):
        # Try to unregister representation
        if hasattr(self, 'representation'):
            delattr(self, 'representation')

    def Set(self, **kwargs):
        for name, value in kwargs.items():
            method_name = 'Set' + name
            name = name.lower()
            if value == None:
                self.Unregister(name)
            elif hasattr(self, method_name):
                getattr(self, method_name)(value)
            else:
                setattr(self, name, value)
    
    def Unregister(self, *args):
        for name in args:
            method_name = 'Unregister' + name
            name = name.lower()
            if hasattr(self, method_name):
                getattr(self, method_name)()
            elif hasattr(self, name):
                delattr(self, name)
            else:
                print (name + ' already unregistered or invalid name')


class LeadGreenFunction(GreenFunction):
    """Possible class attributes:

    Inherited from GreenFunction:
    ----------------------------
    shape
    hamiltonian
    identity
    inf
    energy
    greenfunctionflag
    representation

    self:
    ----
    interaction
    identity_interaction
    niterations
    convergence
    """
    def __init__(self, hamiltonian=None, interaction=None, identity=None,
                 identityinteraction=None, energy=None):
        GreenFunction.__init__(self, hamiltonian=hamiltonian,
                               identity=identity, energy=energy)
        if interaction is not None:
            self.SetInteractionRepresentation(interaction)
        if identityinteraction is not None:
            self.SetInteractionIdentityRepresentation(identityinteraction)

    def SetInteractionRepresentation(self, interaction):
        self.interaction=interaction
        # This will force an update
        self.UnregisterRepresentation()

    def GetInteractionRepresentation(self):
        return self.interaction

    def SetInteractionIdentityRepresentation(self, identityinteraction):
        self.identity_interaction = identityinteraction
        # This will force an update
        self.UnregisterRepresentation()

    def GetInteractionIdentityRepresentation(self):
        return self.identity_interaction

    def SetMaxNumberOfIterations(self, niterations):
        self.niterations = niterations

    def GetMaxNumberOfIterations(self):
        if not hasattr(self, 'niterations'):
            self.niterations = 100000
        return self.niterations
    
    def SetConvergenceFactor(self, convergence):
        self.convergence = convergence

    def GetConvergenceFactor(self):
        if not hasattr(self, 'convergence'):
            self.convergence = 1.0e-8
        return self.convergence

    def GetRetardedEquation(self):
        # Calculating (EO-H) by iteration
        energy = self.GetComplexEnergy()
        
        # h00=energy*I11-H11:
        h00 = (energy * self.GetIdentityRepresentation() -
               self.GetHamiltonianRepresentation())

        # h11=energy*I11-H11=h00:
        h11 = h00.copy()

        # h10=energy*Ii^dagger-Hi^dagger:
        h10 = (energy * self.GetInteractionIdentityRepresentation().T.conj() -
               self.GetInteractionRepresentation().T.conj()).copy()
        
        # h01=energy*Ii-Hi:
        h01 = (energy * self.GetInteractionIdentityRepresentation() -
               self.GetInteractionRepresentation())

        h01B = np.zeros_like(h01)
        A = np.zeros_like(h01)
        B = np.zeros_like(h01)
        delta = 10000
        n = 0
        while (delta > self.GetConvergenceFactor()) and \
                  (n < self.GetMaxNumberOfIterations()):
            # Calculating:
            # A = h_{11}^{-1}xh_{01}    B = h_{11}^{-1}xh_{10}
            # h_{11}xA = h_{01}         h_{11}xB=h_{10}

            A[:] = np.linalg.solve(h11, h01)
            B[:] = np.linalg.solve(h11, h10)

            # Then: h_{00} -> h_{00}-h_{01}xB 
            #       h_{11} -> h_{11}-h_{10}xA-h_{01}xB
            #       h_{01} -> -h_{01}xA
            #       h_{10} -> -h_{10}xB
            # Calculate the "screened" interactions

            #h01B = np.dot(h01, B)
            #h00 -= h01B
            #h11 -= np.dot(h10, A) + h01B
            #h01 = -np.dot(h01, A)
            #h10 = -np.dot(h10, B)
            gemm(1.0, B, h01, 0.0, h01B)
            h00 -= h01B
            gemm(-1.0, A, h10, 1.0, h11)
            h11 -= h01B
            gemm(-1.0, A, h01.copy(), 0.0, h01)
            gemm(-1.0, B, h10.copy(), 0.0, h10)

            # If element h01 is negligible: stop the iteration
            delta = abs(h01).max()
            n += 1
        return h00  

    def ReadRetardedEquationFromNetCDFFile(self, filename, lead):
        # lead can be 'left' or 'right' depending on which lead should be read
        file = NetCDFFile(filename)
        dim = int(file.variables["Dimension"].getValue())
        reteq = np.zeros([dim, dim], complex)
        reteq.real=file.variables[lead][:, :, 0]
        reteq.imag=file.variables[lead][:, :, 1]
        file.close()
        return reteq

    def UpdateRepresentation(self):
        leadgreenfunction = np.linalg.inv(self.GetRetardedEquation())
        self.SetRepresentation(leadgreenfunction)


class ScatteringGreenFunction(GreenFunction):
    """Possible class attributes:

    Inherited from GreenFunction:
    ----------------------------
    shape
    hamiltonian
    identity
    inf
    energy
    greenfunctionflag
    representation

    self:
    ----
    hsmatrix
    internalscatter
    inthsmatrix
    hlmatrix
    hrmatrix
    widebandlimit
    leftleadgf
    rightleadgf
    sigmatag
    sigmaright
    sigmaleft
    """
    def __init__(self, scatteringhamiltonian=None, leftleadhamiltonian=None,
                 rightleadhamiltonian=None,
                 internalscatteringhamiltonian=None, energy=None):
        if scatteringhamiltonian is not None:
            self.SetScatteringHamiltonian(scatteringhamiltonian)
        if internalscatteringhamiltonian is not None:
            self.SetInternalScatteringHamiltonian(
                internalscatteringhamiltonian)
        if leftleadhamiltonian is not None:
            self.SetLeftLeadHamiltonian(leftleadhamiltonian)
        if rightleadhamiltonian is not None:
            self.SetRightLeadHamiltonian(rightleadhamiltonian)
        self.InitializeLeadGreenFunctions()
    
    def SetScatteringHamiltonian(self,hs):
        self.hsmatrix=hs

    def GetScatteringHamiltonian(self):
        return self.hsmatrix

    def SetInternalScatteringHamiltonian(self,hs):
        self.internalscatter=1
        self.inthsmatrix=hs

    def GetInternalScatteringHamiltonian(self):
        return self.inthsmatrix

    def SetLeftLeadHamiltonian(self,hl):
        self.hlmatrix=hl

    def GetLeftLeadHamiltonian(self):
        if not hasattr(self,'hlmatrix'):
            print ("Please specify the leftleadhamiltonian.")
        else:
            return self.hlmatrix
    
    def SetRightLeadHamiltonian(self,hr):
        self.hrmatrix=hr

    def GetRightLeadHamiltonian(self):
        if not hasattr(self,'hrmatrix'):
            print ("Please specify the rightleadhamiltonian.")
        else:
            return self.hrmatrix

    def AlignFermiLevels(self,h00_l=None):
        """Aligns the fermi level of the scattering region
           to the fermi level of the lead, or to the argument e0.
           
           The aligning is done by matching a matrix element
           <w|H|w> (the first) in the principal layer
           in the scattering region to the corresponding matrix element
           in the lead"""
        
        #instances
        Hs = self.GetScatteringHamiltonian()
        Hl = self.GetLeftLeadHamiltonian() # assuming the leads are identical
        
        #matrices
        h00_s = Hs.GetH_l()[0, 0] 
        s00_s = Hs.GetS_l()[0, 0]
        if h00_l == None:
            h00_l = Hl.GetH_l()[0, 0]
        S = Hs.GetOverlapMatrix()
        H = Hs.GetHamiltonianMatrix()
        print ("diff:", h00_s - h00_l)
        #do the alignment
        Hs_aligned = H - ((h00_s - h00_l) / s00_s) * S
        
        #update the matrix of the Hs instance
        Hs.SetHamiltonianMatrix(Hs_aligned)
        
        # Do the same for Hartree and xc if they exist
        if hasattr(Hs,'hartree'):
            Hs.SetHartreeMatrix(Hs.GetHartreeMatrix() -
                                ((h00_s - h00_l) / s00_s) * S)
        if hasattr(Hs, 'xc'):
            Hs.SetXCMatrix(Hs.GetXCMatrix() - ((h00_s - h00_l) / s00_s) * S)
        if hasattr(self, 'inthsmatrix'):
            Hs_int = self.GetInternalScatteringHamiltonian()
            Hs_int.SetHamiltonianMatrix(Hs_aligned.copy())
            if hasattr(Hs_int,'hartree'):
                Hs_int.SetHartreeMatrix(Hs_int.GetHartreeMatrix() -
                                        ((h00_s - h00_l) / s00_s) * S)
            if hasattr(Hs_int,'xc'):
                Hs_int.SetXCMatrix(Hs_int.GetXCMatrix() -
                                   ((h00_s - h00_l) / s00_s) * S)
        print ("Principal layer convergence: " + str(
            self.GetPrincipalLayerConvergence()) + " eV\n")

    def GetPrincipalLayerConvergence(self,pl1=None,pl2=None):
        """The function returns a real number, which is the
           absolute value of the largest difference of the
           matrix elements of the principal layer in the lead
           and in the scattering region.
           These should be as small as possible
           (say below at least 0.2 eV)."""
        
        #instances
        Hs = self.GetScatteringHamiltonian()
        Hl = self.GetLeftLeadHamiltonian()#assuming the leads are identical

        #matrices
        if pl1 == None or pl2 == None:
            # no principal layers was specified ->
            # use the lead and S region p.l.
            pl1 = Hs.GetH_l() # leftmost principallayer in the S region
            pl2 = Hl.GetH_l() # principallayer in the lead
        
        err = max(np.absolute((pl2 - pl1).flat))
        return err

    def InitializeLeadGreenFunctions(self):
        # Initialize left lead GF
        H_ll = self.GetLeftLeadHamiltonian().GetH_l()
        O_ll = self.GetLeftLeadHamiltonian().GetS_l()
        H_intsc = self.GetLeftLeadHamiltonian().GetT_l()
        O_intsc = self.GetLeftLeadHamiltonian().GetInteractionS_l()
        self.SetLeftLeadGreenFunction(LeadGreenFunction(
            hamiltonian=H_ll, interaction=H_intsc, identity=O_ll,
            identityinteraction=O_intsc))
        
        # Initialize right lead GF
        H_rr = self.GetRightLeadHamiltonian().GetH_r()
        O_rr = self.GetRightLeadHamiltonian().GetS_r()
        H_intsc = self.GetRightLeadHamiltonian().GetT_r()
        O_intsc = self.GetRightLeadHamiltonian().GetInteractionS_r()
        self.SetRightLeadGreenFunction(LeadGreenFunction(
            hamiltonian=H_rr, interaction=H_intsc, identity=O_rr,
            identityinteraction=O_intsc))
        
        # Initialize parent Green function
        H_sc = self.GetScatteringHamiltonian().GetH_s()
        O_sc = self.GetScatteringHamiltonian().GetS_s()
        GreenFunction.__init__(self, hamiltonian=H_sc, identity=O_sc)
                
    def GetHamiltonianRepresentation(self):
        if hasattr(self, 'internalscatter'):
            return self.GetInternal_H_sc()
        else:
            return GreenFunction.GetHamiltonianRepresentation(self)

    def SetHamiltonianRepresentation(self, h0):
        if hasattr(self, 'internalscatter'):
            int_h = self.GetInternalScatteringHamiltonian()
            h = self.GetScatteringHamiltonian()

            # Setting internal Hamiltonian
            int_h.SetH_s(h0)

            # Setting internal part of large scattering Hamiltonian
            H_mat = h.GetHamiltonianMatrix()
            leftprinlength = int_h.GetLeftPrincipalLayerLength()
            rightprinlength = int_h.GetRightPrincipalLayerLength()
            length = H_mat.shape[0]
            H_mat[leftprinlength:length - rightprinlength,
                  leftprinlength:length - rightprinlength] = h0
            h.SetHamiltonianMatrix(H_mat)
            GreenFunction.SetHamiltonianRepresentation(self, h.GetH_s())
        else:
            GreenFunction.SetHamiltonianRepresentation(self, h0)

    def GetIdentityRepresentation(self):
        if hasattr(self, 'internalscatter'):
            return self.GetInternal_S_sc()
        else:
            return GreenFunction.GetIdentityRepresentation(self)
        
    def GetInternal_H_sc(self):
        return self.GetInternalScatteringHamiltonian().GetH_s()

    def GetInternal_S_sc(self):
        return self.GetInternalScatteringHamiltonian().GetS_s()

    def GetInternal_H_intl(self):
        return self.GetInternalScatteringHamiltonian().GetT_l()

    def GetInternal_S_intl(self):
        return self.GetInternalScatteringHamiltonian().GetInteractionS_l()

    def GetInternal_H_intr(self):
        return self.GetInternalScatteringHamiltonian().GetT_r()

    def GetInternal_S_intr(self):
        return self.GetInternalScatteringHamiltonian().GetInteractionS_r()

    def GetInternal_H_l(self):
        return self.GetInternalScatteringHamiltonian().GetH_l()

    def GetInternal_S_l(self):  
        return self.GetInternalScatteringHamiltonian().GetS_l()

    def GetInternal_H_r(self):
        return self.GetInternalScatteringHamiltonian().GetH_r()

    def GetInternal_S_r(self):
        return self.GetInternalScatteringHamiltonian().GetS_r()

    def WideBandLimitOn(self):
        self.widebandlimit=1

    def SetEnergy(self,energy):
        GreenFunction.SetEnergy(self,energy)
        # Propagating changes to lead green functions
        try:
            self.GetLeftLeadGreenFunction().SetEnergy(energy)
        except AttributeError:
            pass
        try:
            self.GetRightLeadGreenFunction().SetEnergy(energy)
        except AttributeError:
            pass
        # Force an update of self energies:
        self.UnregisterRightLeadSelfEnergy()
        self.UnregisterLeftLeadSelfEnergy()
        
    def SetLeftLeadGreenFunction(self,leftleadgreenfunction):
        # The lead green functions should not be referenced
        leftleadgreenfunction.ReferenceGreenFunctionOff()
        self.leftleadgf=leftleadgreenfunction

    def GetLeftLeadGreenFunction(self):
        return self.leftleadgf

    def SetRightLeadGreenFunction(self,rightleadgreenfunction):
        # The lead green functions should not be referenced
        rightleadgreenfunction.ReferenceGreenFunctionOff()
        self.rightleadgf = rightleadgreenfunction

    def GetRightLeadGreenFunction(self):
        return self.rightleadgf

    def GetLeftLeadInteractionRepresentation(self):
        return self.GetScatteringHamiltonian().GetT_l()

    def GetRightLeadInteractionRepresentation(self):
        return self.GetScatteringHamiltonian().GetT_r()

    def GetLeftLeadIdentityRepresentation(self):
        return self.GetScatteringHamiltonian().GetInteractionS_l()

    def GetRightLeadIdentityRepresentation(self):
        return self.GetScatteringHamiltonian().GetInteractionS_r()

    def ReferenceSelfEnergies(self):
        # Default: Self energies are not referenced:
        if not hasattr(self, 'sigmatag'):
            self.ReferenceSelfEnergiesOff()
        return self.sigmatag

    def ReferenceSelfEnergiesOn(self):
        self.sigmatag = 1

    def ReferenceSelfEnergiesOff(self):
        self.sigmatag = 0

    def UpdateRightLeadSelfEnergy(self):
        """Calculating self energy due to right lead."""
        def SE(E, S, H, Ginv):
            # Calculating left lead self-energy = tau . G . tau'
            # where tau' = E * I^dag - H^dag and tau = E * I - H
            return np.dot(E * S - H,
                          np.linalg.solve(Ginv, E * S.T.conj() - H.T.conj()))

        #E = self.GetComplexEnergy()
        Glead = self.GetRightLeadGreenFunction()
        E = Glead.GetComplexEnergy()
        Ginv = Glead.GetRetardedEquation()
        if not hasattr(self, 'internalscatter'):
            sigma = SE(E=E,
                       S=self.GetRightLeadIdentityRepresentation(),
                       H=self.GetRightLeadInteractionRepresentation(),
                       Ginv=Ginv)
        else:
            sigma = SE(E=E,
                       S=Glead.GetInteractionIdentityRepresentation(),
                       H=Glead.GetInteractionRepresentation(),
                       Ginv=Ginv)
            # The following retarded equation uses sigma as self-energy to
            # the lower right block of scattering Hamiltonian.
            # NOTE: Even if the internal region is the full scattering region,
            # there is still a difference coming from the fact that the lower
            # right block of scattering Hamiltonian in that case might be
            # different from the principal layer (the difference
            # will be larger the larger 'principal-layer convergence') 
            Ginv_int = E * self.GetInternal_S_r() - self.GetInternal_H_r()
            l = sigma.shape[0]
            Ginv_int[-l:, -l:] -= sigma
            sigma = SE(E=E,
                       S=self.GetInternal_S_intr(),
                       H=self.GetInternal_H_intr(),
                       Ginv=Ginv_int)
        self.SetRightLeadSelfEnergy(sigma)

    def GetRightLeadSelfEnergy(self):
        # If the presentation is not set: update
        if not hasattr(self, 'sigmaright'):
            self.UpdateRightLeadSelfEnergy()
        sigmaright = self.sigmaright
        # If not ReferenceSelfEnergies: Delete the self energy
        if not self.ReferenceSelfEnergies():
            self.UnregisterRightLeadSelfEnergy()
        return sigmaright

    def SetRightLeadSelfEnergy(self, selfenergy):
        self.sigmaright = selfenergy

    def UnregisterRightLeadSelfEnergy(self):
        # First unregister right lead self energy:
        if hasattr(self, 'sigmaright') and not hasattr(self, 'widebandlimit'):
            delattr(self, 'sigmaright')
    
    def UpdateLeftLeadSelfEnergy(self):
        """Calculating self energy due to left lead."""
        def SE(E, S, H, Ginv):
            # Calculating left lead self-energy = tau . G . tau'
            # where tau' = E * I^dag - H^dag and tau = E * I - H
            return np.dot(E * S - H,
                          np.linalg.solve(Ginv, E * S.T.conj() - H.T.conj()))

        #E = self.GetComplexEnergy()
        Glead = self.GetLeftLeadGreenFunction()
        E = Glead.GetComplexEnergy()
        Ginv = Glead.GetRetardedEquation()
        if not hasattr(self, 'internalscatter'):
            sigma = SE(E=E,
                       S=self.GetLeftLeadIdentityRepresentation(),
                       H=self.GetLeftLeadInteractionRepresentation(),
                       Ginv=Ginv)
        else:
            sigma = SE(E=E,
                       S=Glead.GetInteractionIdentityRepresentation(),
                       H=Glead.GetInteractionRepresentation(),
                       Ginv=Ginv)
            Ginv_int = E * self.GetInternal_S_l() - self.GetInternal_H_l()
            l = sigma.shape[0]
            Ginv_int[:l, :l] -= sigma
            sigma = SE(E=E,
                       S=self.GetInternal_S_intl(),
                       H=self.GetInternal_H_intl(),
                       Ginv=Ginv_int)
        self.SetLeftLeadSelfEnergy(sigma)

    def GetLeftLeadSelfEnergy(self):
        # If the presentation is not set: update
        if not hasattr(self, 'sigmaleft'):
            self.UpdateLeftLeadSelfEnergy()
        sigmaleft = self.sigmaleft
        # If not ReferenceSelfEnergies: Delete the self energy
        if not self.ReferenceSelfEnergies():
            self.UnregisterLeftLeadSelfEnergy()
        return sigmaleft

    def SetLeftLeadSelfEnergy(self, selfenergy):
        self.sigmaleft = selfenergy
        
    def UnregisterLeftLeadSelfEnergy(self):
        # Try to unregister left lead self energy:
        if hasattr(self, 'sigmaleft') and not hasattr(self, 'widebandlimit'):
            delattr(self, 'sigmaleft')

    def GetRetardedEquation(self):
        # Calculating the Scroedinger eq: E*S - H - sigmaL - sigmaR:
        return (self.GetComplexEnergy() * self.GetIdentityRepresentation()
                - self.GetHamiltonianRepresentation()
                - self.GetLeftLeadSelfEnergy()
                - self.GetRightLeadSelfEnergy())

    def GetTransmission(self):
        Gr = self.GetRepresentation()
        Sl = self.GetLeftLeadSelfEnergy()
        Sr = self.GetRightLeadSelfEnergy()
        return transmission(Gr, Sl, Sr)
        
    def UpdateRepresentation(self):
        # G(E) = inv(E*S - H - sigmaL - sigmaR)
        self.SetRepresentation(np.linalg.inv(self.GetRetardedEquation()))


class NonEqNonIntGreenFunction(ScatteringGreenFunction):
    """Possible class attributes:

    Inherited from GreenFunction:
    ----------------------------
    shape
    hamiltonian
    identity
    inf
    energy
    greenfunctionflag
    representation

    Inherited from ScatteringGreenFunction:
    --------------------------------------
    hsmatrix
    internalscatter
    inthsmatrix
    hlmatrix
    hrmatrix
    widebandlimit
    leftleadgf
    rightleadgf
    sigmatag
    sigmaright
    sigmaleft

    self:
    ----
    mem
    mul
    mur
    intgf
    temp
    uncoupled
    leftwide
    rightwide
    efermi
    lesserlist
    retardedlist
    selistright
    selistleft
    nua
    nub
    nuc
    energylist
    nuindices
    """
    def __init__(self, scatteringhamiltonian, leftleadhamiltonian,
                 rightleadhamiltonian, internalscatteringhamiltonian=None,
                 E_Fermi=0.0, mul=0.0, mur=0.0, uncoupled=0, energies=None,
                 temp=0.0, boxdata=None):
        
        ScatteringGreenFunction.__init__(self, scatteringhamiltonian,
                                         leftleadhamiltonian,
                                         rightleadhamiltonian,
                                         internalscatteringhamiltonian)     
        self.mem = maxrss()
        self.SetFermiLevel(E_Fermi)
        self.mul = mul
        self.mur = mur
        self.temp = temp
        # 'uncoupled' is a flag that can be used to perform calculations for 
        # an isolated central region, i.e. without coupling to leads. In this
        # case the Fermi level is the chemical potential of the central region.
        self.SetCouplingStatus(uncoupled)
        if energies is not None:
            self.SetEnergyList(energies)
        self.boxdata = boxdata
        self.p2b = AssignOrbitals2Procs(self.GetMatrixDimension()[0])
        
    def GetLeadInfinitesimal(self):
        return self.GetLeftLeadGreenFunction().GetInfinitesimal()

    def SetLeadInfinitesimal(self, eta):
        self.GetLeftLeadGreenFunction().SetInfinitesimal(eta)
        self.GetRightLeadGreenFunction().SetInfinitesimal(eta)

    def SetInteractingGreenFunctionRef(self, intgf):
        self.intgf = intgf

    def SpinPolarized(self):
        # The noninteracting GF is assumed to be non-spin polarized
        return False

    def SetTemperature(self, temp):
        self.temp = temp
        self.Unregister()
        if hasattr(self, 'intgf'):
            for name in ['GW', 'Bubble2', 'Exchange2']:
                if name in self.intgf.GetNamesOfInteractingSelfEnergies():
                    self.intgf.GetSelfEnergy(name).EvaluateCurrentBounds()
            self.intgf.UnregisterRetardedLesserLists()
            self.intgf.UnregisterInteractingSelfEnergies()
        
    def GetIndex(self, grid, val):
        for i, g in enumerate(grid):
            if abs(g - val) < 1e-5:
                return i

    def GetTempFactor(self):
        # This factor determines the size of the thermo-window
        return 5

    def GetTemperature(self):
        return self.temp

    def SetCouplingStatus(self, uncoupled):
        self.uncoupled = uncoupled

    def Uncoupled(self):
        return self.uncoupled

    def Equilibrium(self):
        return abs(self.GetLeftChemicalPotential() -
                   self.GetRightChemicalPotential()) < 1.0e-6

    def WideBand(self):
        if hasattr(self,'leftwide'):
            return True
        else:
            return False

    def IdenticalCoupling(self):
        if self.WideBand():
            if abs(self.GetLeftWideBand() -
                   self.GetRightWideBand()).max() < 1.0e-6:
                return True
        return False

    def SetLeftWideBand(self,leftwide):
        # Retarded component of the band in the left lead.
        # Should be diagonal and NEGATIVE imaginary
        if min((leftwide.imag).flat) >= 0.0:
            print ("ERROR: the wideband retarded self-energy must be "
                   "negative and imaginary")
            stop
        self.leftwide = leftwide

    def GetLeftWideBand(self):
        return self.leftwide
        
    def SetRightWideBand(self,rightwide):
        if min((rightwide.imag).flat)>=0.0:
            print ("ERROR: the wideband retarded self-energy must be "
                   "negative and imaginary")
            stop
        self.rightwide=rightwide

    def GetRightWideBand(self):
        return self.rightwide

    def SetFermiLevel(self, efermi, recursive=True):
        if hasattr(self, 'intgf') and recursive:
            self.intgf.SetFermiLevel(efermi)
        else:
            self.Unregister(all=False)
            self.efermi = efermi

    def GetFermiLevel(self):
        return self.efermi

    def GetLeftEnergy(self, energy):
        return energy - self.GetLeftChemicalPotential()

    def GetRightEnergy(self, energy):
        return energy - self.GetRightChemicalPotential()  

    def SetHamiltonianRepresentation(self, h0, nonrecursive=False):
        # If nonrecursive is True then interating GF is not called.
        if nonrecursive==False and hasattr(self,'intgf'):
            self.intgf.SetHamiltonianRepresentation(h0)
        else:
            ScatteringGreenFunction.SetHamiltonianRepresentation(self, h0)
            self.Unregister(all=False)
            
    def GetCurrentIntegrationGrid(self):
        mu_l = self.GetLeftChemicalPotential()
        mu_r = self.GetRightChemicalPotential()
        T=self.GetTemperature()
        efermi = self.GetFermiLevel()
        upper_bound = max(mu_l + efermi, mu_r + efermi)+T*self.GetTempFactor()
        lower_bound = min(mu_l + efermi, mu_r + efermi)-T*self.GetTempFactor()
        energies = self.GetNUEnergyList()
        start = np.searchsorted(energies, lower_bound)
        end = np.searchsorted(energies, upper_bound)
        return energies[start:end]
    
    def Unregister(self,all=True):
        if world.rank==0:
            print ("************************************************")
            print ("Deleting non-interacting lesser and retarded GFs")
            print ("************************************************")    
        if hasattr(self,'lesserlist'):
            del self.lesserlist
        if hasattr(self,'retardedlist'):
            del self.retardedlist
        if hasattr(self,'selistright') and all:
            del self.selistright
        if hasattr(self,'selistleft') and all:
            del self.selistleft

    def GetDensityOfStates(self):
        """Total density of states -1/pi Im(Tr(GS))"""
        G = self.GetRetardedList()
        S = self.GetIdentityRepresentation()
        return -np.dot(G, S).imag.trace(axis1=1, axis2=2) / np.pi

    def GetSpectralMatrix(self):
        """A = i(G^r-G^a) ; dimensions: (energy,orbital,orbital)"""
        Gr = self.GetRetardedList()
        Ga = self.GetAdvancedList()
        return 1.0j * (Gr - Ga)

    def GetSpectralFunctions(self):
        """Projected density of states -1/pi Im(SGS/S)

        Returns a 2D array, where first dimension is energy, second is orbital.
        """
        G = self.GetRetardedList()
        S = self.GetIdentityRepresentation()
        return -np.imag(np.dot(S, np.dot(G, S)).diagonal(axis1=0, axis2=2) / \
                        S.diagonal()) / np.pi
        
    def GetDensityMatrix(self):
        """Determine dual density matrix.

        Return the matrix::

          D = -i G<(t=0) = -i/2 pi * int dw G<(w)

        Which is not the physical, but the dual density matrix.
        """
        den_en = -1.j * self.GetLesserList()
        den = np.tensordot(den_en, self.GetNUWeights(), [0, 0]) / (2 * np.pi)
        world.sum(den)
        return den

    def GetOccupations(self):
        D = self.GetDensityMatrix()
        S = self.GetIdentityRepresentation()
        return np.dot(np.dot(S, D), S).real.diagonal() / S.real.diagonal()

    def GetIntegratedDOS(self):
        den_en = 1.j * (self.GetRetardedList() - self.GetAdvancedList())
        den = np.tensordot(den_en, self.GetNUWeights(), [0, 0]) / (2 * np.pi)
        world.sum(den)
        S = self.GetIdentityRepresentation()
        return np.dot(np.dot(S, den), S).real.diagonal() / S.real.diagonal()

    def GetIntegratedDOS2(self):
        I = np.dot(self.GetNUWeights(), self.GetSpectralFunctions())
        world.sum(I)
        return I

    def GetTotalParticleNumber(self):
        """Returns REAL number of particles, i.e. overlap taken into account"""
        D = self.GetDensityMatrix()
        S = self.GetIdentityRepresentation()
        return 2 * np.dot(D, S).real.trace() # multiply by 2 due to spin

    def GetBoxRetarded(self, index):
        if self.boxdata is None:
            return 0.0
        if not hasattr(self, 'boxretarded_e'):
            from kgf.Hilbert import hilbert
            eta, a, b, T = self.boxdata
            energies = self.GetFullEnergyList()
            mynuindices = self.GetMyNUGridIndices()
            se = np.empty(len(energies), complex)
            se.imag = .5 * (np.tanh(.5 * (energies - a) / T) -
                            np.tanh(.5 * (energies - b) / T))
            se.real = hilbert(se.imag)#  * 0.0 XXX
            se.imag -= 1
            se *= eta
            if False and world.rank == 0:
                import pylab as pl
                pl.plot(energies, se.real, label='real')
                pl.plot(energies, se.imag, label='imag')
                pl.legend()
                pl.savefig('box.png')
            self.boxretarded_e = se.take(mynuindices)
            assert len(self.boxretarded_e) == len(self.GetNUEnergyList())
            assert (energies.take(mynuindices) == self.GetNUEnergyList()).all()
            print (self.boxdata , '!!!!!!!!!!')
        return self.boxretarded_e[index] * self.GetIdentityRepresentation()

    def UpdateRetardedAndLesserLists(self):
        #mult = np.dot
        ef = self.GetFermiLevel()
        mul = self.GetLeftChemicalPotential()
        mur = self.GetRightChemicalPotential()
        T = self.GetTemperature()
        dim = self.GetHamiltonianRepresentation().shape
        enlist = self.GetNUEnergyList()
        self.lesserlist = np.zeros([len(enlist), dim[0], dim[0]], complex)
        self.retardedlist = np.zeros([len(enlist), dim[0], dim[0]], complex)
        if world.rank == 0:
            print ("Updating non-interacting retarded/lesser Green's function")
        for i, en in enumerate(enlist):
            sigma_left = self.GetLeftLeadRetardedSelfEnergy(i)
            sigma_right = self.GetRightLeadRetardedSelfEnergy(i)
            box = self.GetBoxRetarded(i)
            self.SetEnergy(en)
        
            # Calculate retarded GF
            Gr = np.linalg.inv(
                self.GetComplexEnergy() * self.GetIdentityRepresentation()
                - self.GetHamiltonianRepresentation()
                - sigma_left - sigma_right - box)
                
            # For T=0: 
            # -G^<(e) = G^r(e) - G^a(e) for e < min{mu_L,mu_R}
            # -G^<(e) = G^r(e)[ sigma_L^r - sigma_L^a ] G^a(e) for mu_R < e < mu_L
            # -G^<(e) = G^r(e)[ sigma_R^r - sigma_R^a ] G^a(e) for mu_L < e < mu_R
            # -G^<(e) = 0 for e > max{mu_L,mu_R}            
            # In the finite T case the Keldysh equation must be used: 
            #   G^<(e) = G^r(e)[ - f_L(e){sigma_L^r - sigma_L^a}
            #                    - f_R(e){sigma_R^r - sigma_R^a}] G^a(e)
            
            if self.Uncoupled() or self.Equilibrium():
                Gl = -FermiDistribution(en - ef, kBT=T) * (Gr - Gr.T.conj())
            else:
                Tf = self.GetTempFactor()
                # For energies outside the thermo-bias interval:
                if (en < min(mul + ef-Tf*T, mur + ef-Tf*T)) or (en > max(mul + ef+Tf*T, mur + ef+Tf*T)):
                    if en < min(mul + ef-Tf*T, mur + ef-Tf*T):
                        Gl = -(Gr - Gr.T.conj())
                    else:
                        Gl = np.zeros_like(Gr)
                else:
                    Gl = np.dot(np.dot(
                        Gr, self.GetLeftLeadLesserSelfEnergy(i) +
                        self.GetRightLeadLesserSelfEnergy(i)), Gr.T.conj())
                    # Calculating the "extra" term in the Keldysh equation.
                    # This is important in particular when bound states are
                    # formed outside the band of the leads.
                    # We use that
                    #       (1+G^rSigma^R)G_0^l(1+Sigma^aG^a)=2i\eta G^r G^a
                    # The 'extra' term can introduce artificial kinks in G^l
                    # at E_F due to the Fermi function.
                    # This kink will vanish when eta -> 0.
                    # Alternatively the 'extra' term can be seen as a third
                    # contact which provides the necessary broadening of the
                    # levels. In any case we use a finite temperature to smear
                    # out the kink.
                    extra = 2.j * self.GetInfinitesimal() * \
                            FermiDistribution(en - ef, 0.5) * \
                            np.dot(Gr, Gr.T.conj())
                    Gl += extra
            self.retardedlist[i] = Gr
            self.lesserlist[i] = Gl
 
    def GetGreaterList(self):
        # Use: G^r - G^a = G^> - G^<
        gr = self.GetRetardedList()
        gl = self.GetLesserList()
        return gl + gr - np.swapaxes(gr, 1, 2).conj()
        
    def SetQuickLeads(self):
        # NOT implemented.
        # Idea is to use the center of mass of the lead spectral function and
        # avoid the integration in the Kramer-Kronig relation.
        self.quick = 1

    def QuickLeads(self):
        if not hasattr(self,'quick'):
            return False
        else:
            return True

    def UpdateLeftLeadRetardedSelfEnergyList(self):
        if self.WideBand():
            return
        enlist = self.GetNUEnergyList()
        if self.Uncoupled():
            self.selistleft = np.zeros([len(enlist)])
            return
        dim = self.GetHamiltonianRepresentation().shape
        self.selistleft = np.zeros([len(enlist),dim[0],dim[0]], complex)
        if world.rank == 0:
            print ("Updating left lead self-energy")
        for i in range(len(enlist)):
            en = enlist[i] 
            self.SetEnergy(self.GetLeftEnergy(en))
            self.selistleft[i] = self.GetLeftLeadSelfEnergy() 
        
    def UpdateRightLeadRetardedSelfEnergyList(self):
        if self.WideBand():
            return
        enlist=self.GetNUEnergyList()
        if self.Uncoupled():
            self.selistright=np.zeros([len(enlist)])
            return
        dim=self.GetHamiltonianRepresentation().shape
        self.selistright=np.zeros([len(enlist),dim[0],dim[0]],complex)
        if world.rank==0:
            print ("Updating right lead self-energy")
        for i in range(len(enlist)):
            en=enlist[i] 
            self.SetEnergy(self.GetRightEnergy(en))
            self.selistright[i]= self.GetRightLeadSelfEnergy() 

    def GetLeftLeadLesserSelfEnergy(self,index):
        # Calculates sigma^<(e) = -f(e)[sigma^r - sigma^a]
        energy=self.GetNUEnergyList()[index]
        abs_leftmu=self.GetFermiLevel()+self.GetLeftChemicalPotential()
        f=FermiDistribution(energy=energy-abs_leftmu,kBT=self.GetTemperature())
        sigma_left=self.GetLeftLeadRetardedSelfEnergy(index)
        return 1.0j*f*LambdaFromSelfEnergy(sigma_left)
   
    def GetRightLeadLesserSelfEnergy(self,index):
        # Calculates sigma^<(e) = -f(e)[sigma^r - sigma^a]
        energy=self.GetNUEnergyList()[index]
        abs_rightmu=self.GetFermiLevel()+self.GetRightChemicalPotential()
        f = FermiDistribution(energy=energy - abs_rightmu,
                              kBT=self.GetTemperature())
        sigma_right = self.GetRightLeadRetardedSelfEnergy(index)
        return 1.j * f * LambdaFromSelfEnergy(sigma_right)

    def GetLeftLeadGreaterSelfEnergy(self, index):
        ret = self.GetLeftLeadRetardedSelfEnergy(index)
        less = self.GetLeftLeadLesserSelfEnergy(index)
        return ret - ret.T.conj() + less
   
    def GetRightLeadGreaterSelfEnergy(self, index):
        ret = self.GetRightLeadRetardedSelfEnergy(index)
        less = self.GetRightLeadLesserSelfEnergy(index)
        return ret - ret.T.conj() + less

    def SetLeftChemicalPotential(self, mul, unregister=True, recursive=True):
        # Chemical potential is wrt. Fermi level
        # unregister=True means retarded and lesser lists are unregistered.
        # Set unregister=False in self-consistent calcs to start new iterations
        # with the old result as initial guess.

        if hasattr(self, 'intgf') and recursive:
            self.intgf.SetLeftChemicalPotential(mul, unregister)
            return
        if hasattr(self, 'energylist'):
            de = self.GetFullEnergyList()[1] - self.GetFullEnergyList()[0]
        if hasattr(self, 'selistleft'):
            # Potential changed by integer number of energy grid spacings
            # If wideband limit is 'on' there is no 'selistleft'.
            # This is no problem as we do not need to shift it in this case.
            if world.rank == 0:
                print ('NOT re-calculating left self-energies. Shifting old one')
            
            n = int(round((mul - self.mul) / de))
            #print mul, self.mul, de, n
            #print len(self.GetFullEnergyList()), len(self.GetNUGridIndices()), len(self.selistleft)
           
            # Change to parallization over frequency
            assert self.selistleft.shape[-1] == self.GetMatrixDimension()[-1]
            self.selistleft = SliceAlongFrequency(self.selistleft, self.p2b)
            # Interpolate to uniform grid
            self.selistleft = LinearInterpolation(
                self.selistleft, self.GetNUGridIndices(),
                len(self.GetFullEnergyList()))
            if n > 0:
                self.selistleft[n:] = self.selistleft[:-n]
                self.selistleft[:n] = self.selistleft[n]
            if n < 0:
                self.selistleft[:n] = self.selistleft[-n:]
                self.selistleft[n:] = self.selistleft[n]
            self.selistleft = np.take(self.selistleft,
                                      self.GetNUGridIndices(), axis=0).copy()
            # Change to parallization over orbitals
            self.selistleft = SliceAlongOrbitals(self.selistleft, self.p2b)                                      
        if unregister:
            self.Unregister(all=False)
        self.mul = mul
        
    def GetLeftChemicalPotential(self):
        if not hasattr(self, 'mul'):
            return 0.0
        return self.mul

    def SetRightChemicalPotential(self, mur, unregister=True,recursive=True):
        # Chemical potential is wrt. Fermi level
        # unregister=True means retarded and lesser lists are unregistered.
        # Set unregister=False in self-consistent calc. to start new iterations
        # with the old result as initial guess.
        if hasattr(self, 'intgf') and recursive:
            self.intgf.SetRightChemicalPotential(mur, unregister)
            return
        if hasattr(self, 'energylist'):
            de=self.GetFullEnergyList()[1] - self.GetFullEnergyList()[0]
        if hasattr(self, 'selistright'):
            # Potential changed by integer number of energy grid spacings
            # If wideband limit is 'on' there is no 'selistleft'.
            # This is no problem as we do not need to shift it in this case.
            if world.rank == 0:
                print ("NOT re-calculating right self-energies. Shifting old one.")
            n = int(round((mur - self.mur) / de))
            # Change to parallization over frequencies
            assert self.selistright.shape[-1] == self.GetMatrixDimension()[-1]
            self.selistright = SliceAlongFrequency(self.selistright, self.p2b)
            # Interpolate to uniform grid
            self.selistright = LinearInterpolation(
                self.selistright, self.GetNUGridIndices(),
                len(self.GetFullEnergyList()))
            if n > 0:
                self.selistright[n:]=self.selistright[:-n]
                self.selistright[:n]=self.selistright[n]
            if n < 0:
                self.selistright[:n]=self.selistright[-n:]
                self.selistright[n:]=self.selistright[n]
            self.selistright = np.take(self.selistright,
                                       self.GetNUGridIndices(), axis=0).copy()
            # Change to parallization over orbitals
            self.selistright = SliceAlongOrbitals(self.selistright, self.p2b) 
        if unregister:
            self.Unregister(all=False)
        self.mur = mur
        
    def GetRightChemicalPotential(self):
        if not hasattr(self,'mur'):
            return 0.0
        return self.mur

    def SetNUGridData(self, a, b, c):
        # a controls how fast the grid becomes coarser
        # b and c sets the interval within which the fine grid is used
        self.nua = a
        self.nub = b
        self.nuc = c
        if hasattr(self, 'energylist'):
            self.SetEnergyList(self.energylist)

    def GetNUGridData(self):
        if not hasattr(self, 'nua'):
            self.nua = 2.0
            self.nub = -30000.0
            self.nuc = 30000.0
        return self.nua, self.nub, self.nuc

    def SetEnergyList(self, energylist):
        res1 = len(energylist) % world.size
        nuindices = self.CalculateNUGridIndices(energylist,
                                                self.GetNUGridData())
        res2 = len(nuindices) % world.size
        new_energylist = energylist.copy()
        if res1!=0 or res2!=0:
            # Extending energygrids so all proc's have same number of energy points
            if res2<res1:
                new_length = len(energylist) + (world.size-res1) + world.size
            else:
                new_length = len(energylist) + (world.size-res1)
            de = energylist[1] - energylist[0]
            ext = np.arange(energylist[-1]+de,energylist[-1]+de*(1+new_length-len(energylist)),de)
            new_energylist = np.array(energylist.tolist()+ext.tolist())
            if res2>0:
                nuindices.extend(range(len(energylist),len(energylist)+world.size-res2))
        new_energylist = np.around(new_energylist, 7)
        self.energylist = new_energylist
        self.nuindices = nuindices
        self.Unregister()

    def GetEnergyList(self):
        n = len(self.GetFullEnergyList())
        l = n / world.size
        start = l * world.rank
        end = l * (world.rank + 1)
        return self.GetFullEnergyList()[start:end]
    
    def GetFullEnergyList(self):
        if not hasattr(self, 'energylist'):
            self.energylist = []
        return self.energylist

    def CalculateNUGridIndices(self,grid,data):
        a,b,c = data
        indices=[]
        count=0
        while count<len(grid):
            indices.append(count)
            x = grid[count]
            if x<=0:
                y = int(1+pow(abs(x),a)/pow(abs(b),a))
            else:
                y = int(1+pow(abs(x),a)/pow(abs(c),a))
            count += y
        return indices

    def GetNUGridIndices(self):
        # This is the NU indices of the full grid
        return self.nuindices

    def GetMyNUGridIndices(self):
        n = len(self.nuindices)
        l = n / world.size
        start = l * world.rank
        end = l * (world.rank + 1)
        return self.nuindices[start:end]

    def GetFullNUEnergyList(self):
        return np.take(self.GetFullEnergyList(),self.GetNUGridIndices())
    
    def GetNUEnergyList(self):
        n = len(self.GetFullNUEnergyList())
        l = n // world.size
        start = l * world.rank
        end = l * (world.rank + 1)
        return self.GetFullNUEnergyList()[start:end]
        
    def GetFullNUWeights(self):
        weight = self.GetFullNUEnergyList().tolist()
        weight.append(weight[-1]+weight[-1]-weight[-2])
        weight2 = [weight[0]-weight[1]+weight[0]]+weight
        weight = np.array(weight2)
        return (weight[2:]-weight[:-2])/2

    def GetNUWeights(self):
        n = len(self.GetFullNUWeights())
        l = n // world.size
        start = l * world.rank
        end = l * (world.rank + 1)
        return self.GetFullNUWeights()[start:end]
                
    def SetRetardedList(self, lst):
        self.retardedlist = lst

    def GetRetardedList(self):
        if not hasattr(self,'retardedlist'):
            self.UpdateRetardedAndLesserLists()
        return self.retardedlist

    def GetRetarded(self,index):
        return self.GetRetardedList()[index]

    def GetLesserList(self):
        if not hasattr(self,'lesserlist'):
            self.UpdateRetardedAndLesserLists()
        return self.lesserlist  

    def GetLesser(self,index):
        return self.GetLesserList()[index]
    
    def GetRetardedAndLesser(self, index):
        return self.GetRetarded(index), self.GetLesser(index)

    def SetLesserList(self,lst):
        self.lesserlist = lst

    def GetAdvancedList(self):
        Gr = self.GetRetardedList()
        return np.swapaxes(Gr.conj(), 1, 2)

    def GetLeftLeadRetardedSelfEnergyList(self):
        if not hasattr(self,'selistleft'):
            self.UpdateLeftLeadRetardedSelfEnergyList()
        return self.selistleft

    def GetLeftLeadRetardedSelfEnergy(self,index):
        if self.WideBand():
            return self.GetLeftWideBand()
        else:
            return self.GetLeftLeadRetardedSelfEnergyList()[index]

    def SetLeftLeadRetardedSelfEnergyList(self,lst):
        self.selistleft=lst

    def GetRightLeadRetardedSelfEnergyList(self):
        if not hasattr(self,'selistright'):
            self.UpdateRightLeadRetardedSelfEnergyList()
        return self.selistright

    def GetRightLeadRetardedSelfEnergy(self, index):
        if self.WideBand():
            return self.GetRightWideBand()
        else:
            return self.GetRightLeadRetardedSelfEnergyList()[index]

    def SetRightLeadRetardedSelfEnergyList(self, lst):
        self.selistright = lst

    def GetNearestEnergyMidpoint(self, energy):
        energies = self.GetEnergyList()
        if energies == []: return energy
        if len(energies) == 1: return energies[0]
        i = 0
        while energy > energies[i] and i < len(energies) - 1: i += 1
        return .5 * (energies[i] + energies[i + 1])

    def GetTransmissionFunction(self):
        Gr_e = self.GetRetardedList()
        Ne = len(Gr_e)
        T_e = np.empty(Ne)
        for e in range(Ne):
            Sl = self.GetLeftLeadRetardedSelfEnergy(e)
            Sr = self.GetRightLeadRetardedSelfEnergy(e)
            T_e[e] = transmission(Gr_e[e], Sl, Sr)
        return T_e


class NonEqIntGreenFunction(NonEqNonIntGreenFunction):
    """ This class inherits from NonEqNonIntGreenFunction. However, most
        methods are re-written here so that they refer to the instance of
        NonEqNonIntGreenFunction kept by the interacting self-energy.
        All the list stuff in NonEqNonIntGreenFunction is not implemented
        for this class.

    Possible class attributes:

    Inherited from GreenFunction:
    ----------------------------
    shape
    hamiltonian
    identity
    inf
    energy
    greenfunctionflag
    representation

    Inherited from ScatteringGreenFunction:
    --------------------------------------
    hsmatrix
    internalscatter
    inthsmatrix
    hlmatrix
    hrmatrix
    widebandlimit
    leftleadgf
    rightleadgf
    sigmatag
    sigmaright
    sigmaleft

    Inherited from NonEqNonIntGreenFunction:
    --------------------------------------
    mem
    mul
    mur
    intgf
    temp
    uncoupled
    leftwide
    rightwide
    efermi
    lesserlist
    retardedlist
    selistright
    selistleft
    nua
    nub
    nuc
    energylist
    nuindices

    self:
    ----
    listofintselfenergies
    intretlst
    intlesslst
    """
    
    def __init__(self, listofintselfenergies):
        self.SetListOfInteractingSelfEnergies(listofintselfenergies)
        g0 = self.GetNonIntGreenFunction()
        
        # Propagate a reference to this interacting GF to the noninteracting GF
        g0.SetInteractingGreenFunctionRef(self)

        # Pass trivial methods from g0
        for name in ('GetTemperature',
                     'GetMatrixDimension',
                     'GetHamiltonianRepresentation',
                     'GetIdentityRepresentation',
                     'GetEnergyList',
                     'GetFullEnergyList',
                     'GetNUEnergyList',
                     'GetNUGridIndices',
                     'GetFermiLevel',
                     'GetLeftChemicalPotential',
                     'GetRightChemicalPotential',
                     'GetLeftLeadRetardedSelfEnergy',
                     'GetRightLeadRetardedSelfEnergy',
                     #'GetTransmissionFunction',
                     ):
            setattr(self, name, getattr(g0, name))

    def MemoryEstimate(self):
        N_b = self.GetMatrixDimension()[0]
        N_g = len(self.GetFullEnergyList())
        print ('Memory required to store GFs:', \
              N_g * (N_b**2) * 16 * 6 / (1024**2), 'MB')
        if 'GW' in self.GetNamesOfInteractingSelfEnergies(activeonly=True):
            N_q = self.GetSelfEnergy('GW').GetV().shape[0]
            print ('Memory required for GW self-energy:',\
                  N_g * (N_q**2) * 16 * 4 / (1024**2), 'MB')

    def SpinPolarized(self):
        return False

    def Correlated(self):
        for se in self.GetListOfInteractingSelfEnergies():
            if se.Correlated():
                return True
        return False

    def GetActiveList(self):
        return [se.Active() for se in self.GetListOfInteractingSelfEnergies()]

    def SetActiveList(self, lst):
        for active, selfenergy in zip(
            lst, self.GetListOfInteractingSelfEnergies()):
            selfenergy.SetActive(active)

    def UpdateInteractingSelfEnergies(self):
        for se in self.GetListOfInteractingSelfEnergies(activeonly=True):
            se.Update()

    def UnregisterInteractingSelfEnergies(self):
        for se in self.GetListOfInteractingSelfEnergies():
            se.Unregister()

    def SetTemperature(self,T):
        self.GetNonIntGreenFunction().SetTemperature(T)
        names = self.GetNamesOfInteractingSelfEnergies()
        for name in ['GW', 'B2', 'X2']:
            if name in names:
                index = names.index(name)
                se = self.GetListOfInteractingSelfEnergies()[index]
                se.EvaluateCurrentBounds()
        self.UnregisterRetardedLesserLists()
        self.UnregisterInteractingSelfEnergies()
        
    def GetCurrentIntegrationGrid(self):
        names = self.GetNamesOfInteractingSelfEnergies(activeonly=True)
        selfenergies = self.GetListOfInteractingSelfEnergies(activeonly=True)
        for name in ['GW', 'B2', 'X2', 'phonon_fock']:
            if name in names:
                se = selfenergies[names.index(name)]
                return se.GetCurrentGrid()
        return self.GetNonIntGreenFunction().GetCurrentIntegrationGrid()

    def SetListOfInteractingSelfEnergies(self, listofintselfenergies):
        self.listofintselfenergies = listofintselfenergies

    def GetListOfInteractingSelfEnergies(self, activeonly=False):
        if activeonly:
            return [se for se in self.listofintselfenergies if se.Active()]
        else:
            return self.listofintselfenergies

    def GetTransmissionFunction(self, ferretti=False):
        """Determine the electron transmission function.

        ferretti=True can be used to use the Landauer like transmission
        expression for an interacting systems as described in
        Ferretti et al., PRB 72, 125114 (2005)
        """
        Gr_e = self.GetRetardedList()
        T_e = np.empty(len(Gr_e))
        selist = self.GetListOfInteractingSelfEnergies(activeonly=True)
        if not np.any([se.Correlated() for se in selist]):
            ferretti = False

        for e, Gr in enumerate(Gr_e):
            Sl = self.GetLeftLeadRetardedSelfEnergy(e)
            lambdal = 1.j * (Sl - Sl.T.conj())
            Sr = self.GetRightLeadRetardedSelfEnergy(e)
            lambdar = 1.j * (Sr - Sr.T.conj())
            if not ferretti:
                T_e[e] = dots(Gr, lambdal, Gr.T.conj(), lambdar).trace().real
            else:
                delta = 0.0
                for se in selist:
                    if se.Correlated():
                        ret = se.GetRetarded(e)
                        delta += 1.j * (ret - ret.T.conj())
                delta[:] = np.linalg.solve(lambdal + lambdar +
                                           2 * self.GetInfinitesimal() *
                                           self.GetIdentityRepresentation(),
                                           delta)
                delta.flat[::len(delta) + 1] += 1.0
                T_e[e] = dots(
                    lambdal, Gr, lambdar, delta, Gr.T.conj()).trace().real
                    #Gr, lambdal, delta, Gr.T.conj(), lambdar).trace().real
        return T_e
                
    def AppendSelfEnergy(self,se):
        self.listofintselfenergies.append(se)

    def GetNamesOfInteractingSelfEnergies(self, activeonly=False):
        return [se.GetName()
                for se in self.GetListOfInteractingSelfEnergies(activeonly)]

    def GetSelfEnergy(self, name):
        names = self.GetNamesOfInteractingSelfEnergies()
        ind = names.index(name)
        return self.GetListOfInteractingSelfEnergies()[ind]

    def RemoveSelfEnergy(self,name):
        names=self.GetNamesOfInteractingSelfEnergies()
        ind=names.index(name)
        lst=self.GetListOfInteractingSelfEnergies()
        lst[ind:ind+1]=[]
        self.SetListOfInteractingSelfEnergies(lst)

    def GetRetardedInteractingSelfEnergy(self, index):
        se = np.zeros(self.GetMatrixDimension(), complex)
        for selfenergy in self.GetListOfInteractingSelfEnergies(
            activeonly=True):
#            if index==0 and world.rank==0:
#                print 'GetRetardedInteractingSelfEnergy: ' + selfenergy.name
            se += selfenergy.GetRetarded(index)
        return se

    def GetRetardedSelfEnergy(self, index):
        g0 = self.GetNonIntGreenFunction()
        ret_int = self.GetRetardedInteractingSelfEnergy(index)
        ret_left = g0.GetLeftLeadRetardedSelfEnergy(index)
        ret_right = g0.GetRightLeadRetardedSelfEnergy(index)
        box = g0.GetBoxRetarded(index)
        return ret_int + ret_left + ret_right + box

    def GetLesserInteractingSelfEnergy(self, index):
        se = np.zeros(self.GetMatrixDimension(), complex)
        for selfenergy in self.GetListOfInteractingSelfEnergies(
            activeonly=True):
            se += selfenergy.GetLesser(index)
        return se

    def GetLesserSelfEnergy(self, index):
        GF_0 = self.GetNonIntGreenFunction()
        less_int = self.GetLesserInteractingSelfEnergy(index)
        less_left = GF_0.GetLeftLeadLesserSelfEnergy(index)
        less_right = GF_0.GetRightLeadLesserSelfEnergy(index)
        return less_int + less_left + less_right

    def SetHamiltonianRepresentation(self, h0):
        self.GetNonIntGreenFunction().SetHamiltonianRepresentation(
            h0, nonrecursive=True)
        self.UnregisterRetardedLesserLists()
        self.UnregisterInteractingSelfEnergies()

    def GetNonIntGreenFunction(self):
        return self.GetListOfInteractingSelfEnergies()[0].GetNonIntGreenFunction()

    def SetFermiLevel(self,efermi):
        self.GetNonIntGreenFunction().SetFermiLevel(efermi,recursive=False)
        for name in ['GW','B2','X2']:
            if name in self.GetNamesOfInteractingSelfEnergies():
                index = self.GetNamesOfInteractingSelfEnergies().index(name)
                self.GetListOfInteractingSelfEnergies()[index].EvaluateCurrentBounds()
        self.UnregisterRetardedLesserLists()
        self.UnregisterInteractingSelfEnergies()

    def SetLeftChemicalPotential(self,mul,unregister=True):
    # Chemical potential is wrt. Fermi level
        self.GetNonIntGreenFunction().SetLeftChemicalPotential(
            mul, unregister, recursive=False)
        for name in ['GW','B2','X2']:
            if name in self.GetNamesOfInteractingSelfEnergies():
                index = self.GetNamesOfInteractingSelfEnergies().index(name)
                self.GetListOfInteractingSelfEnergies()[index].EvaluateCurrentBounds()
        self.UnregisterRetardedLesserLists()
        self.UnregisterInteractingSelfEnergies()
        
    def SetRightChemicalPotential(self,mur,unregister=True):
    # Chemical potential is wrt. Fermi level
        self.GetNonIntGreenFunction().SetRightChemicalPotential(mur,unregister,recursive=False)
        for name in ['GW','B2','X2']:
            if name in self.GetNamesOfInteractingSelfEnergies():
                index = self.GetNamesOfInteractingSelfEnergies().index(name)
                self.GetListOfInteractingSelfEnergies()[index].EvaluateCurrentBounds()
        self.UnregisterRetardedLesserLists()
        self.UnregisterInteractingSelfEnergies()
        
    def UpdateRetardedAndLesserLists(self):
        energies = self.GetNUEnergyList()
        if not hasattr(self, 'intretlst'):
            s = (len(energies),) + self.GetMatrixDimension()
            self.intlesslst = np.empty(s, complex)
            self.intretlst = np.empty(s, complex)
        if world.rank == 0:
            print ("Updating interacting Green's functions")
        for i in range(len(energies)):
            self.intretlst[i], self.intlesslst[i] =self.GetRetardedAndLesser(i)
    
    def SetRetardedList(self, lst):
        self.intretlst = lst

    def SetLesserList(self, lst):
        self.intlesslst = lst

    def UnregisterRetardedLesserLists(self):
        if hasattr(self, 'intretlst'):
            del self.intretlst
        if hasattr(self, 'intlesslst'):
            del self.intlesslst

    def GetRetardedList(self):
        if not hasattr(self, 'intretlst'):
            self.UpdateRetardedAndLesserLists()
        return self.intretlst

    def GetLesserList(self):
        if not hasattr(self, 'intlesslst'):
            self.UpdateRetardedAndLesserLists()
        return self.intlesslst

    def GetKeldyshBounds(self):
        # Interval within which the Keldysh equation should be used for G^l.
        # Outside the fluctuation dissipation theorem can be used.
        en_cur = self.GetCurrentIntegrationGrid()
        if len(en_cur) == 0:
            ef = self.GetFermiLevel()
            return [ef, ef]
        return [en_cur[0], en_cur[-1]]

    def GetRetardedAndLesser(self, index):
        # By asking first for the interacting self-energy we make sure that
        # lead self-energies are not calculated twice.
        # If energy is not in the pre-defined energy list of the self-energy,
        # an interpolation is perfomed.
        # First get all the self-energies needed
        energy = self.GetNUEnergyList()[index]
        g0 = self.GetNonIntGreenFunction()
        sigma_ret = self.GetRetardedSelfEnergy(index)
        #########################
        # Calculate retarded GF #
        #########################
        g0.SetEnergy(energy)
        H = (g0.GetComplexEnergy() * g0.GetIdentityRepresentation()
             - g0.GetHamiltonianRepresentation() - sigma_ret)
        Gr = np.linalg.inv(H)
        bounds = self.GetKeldyshBounds()
        if (not (g0.Uncoupled() or g0.Equilibrium())) and \
               (bounds[0] <= energy <= bounds[1]):
        # For higher precision one could use the converged GW^r inside
        # the small Keldysh interval
        #####################################################
        # Use Keldysh equation to obtain lesser/greater GFs #
        #####################################################
            sigma_less = self.GetLesserSelfEnergy(index)
            Gl = np.dot(np.dot(Gr, sigma_less), Gr.T.conj())
            # Calculating the "extra" term in the Keldysh equation if necessary
            # (see explanation under the non-interacting class)
            Gl += 2.j * g0.GetInfinitesimal() * FermiDistribution(
                energy - g0.GetFermiLevel(), kBT=0.5)*np.dot(Gr, Gr.T.conj())
        else:
            # Use dissipation-fluctuation theorem
            Gl = -FermiDistribution(energy - g0.GetFermiLevel(),
                                    kBT=self.GetTemperature()) * (
                Gr - Gr.T.conj())
        return Gr, Gl

    def GetLeadRetardedSelfEnergy(self, index):
        left = self.GetLeftLeadRetardedSelfEnergy(index)
        right = self.GetRightLeadRetardedSelfEnergy(index)
        return left + right

    def SelfConsistent(self, pulay=[[0.05, 0.6, 3]], log=None, doslog=None):
        """Iterate Green function to self-consistency.

        Parameters:

        pulay is a tuple of the form (tolerance, linear mixing, history), it
        can also be a list of such tuples, ie.
        pulay=[(tol1, mix1, his1), (tol2, mix2, his2), ...]
        to first converge to tol1 using mix1 and his1, then converge further
        to tol2 < tol1 using mix2 and his2 etc.
        
        log can be None for stdout, or a str indicating a filename

        doslog can be
          1) None: Nothing is dumped
          2) str:  an nc file is dumped for each iteration. If doslog='file'
                   these wil be named 'fileX.nc', where X is the iteration
                   count
          3) dict: an nc file is dumped for each iteration. doslog must contain
                   a 'filename' key  which is a str with a wildcard,
                   eg. 'file%i.nc'. The remaining keys are passed to
                   WriteSpectralInfoToNetCDFFile.
                   Example: doslog={'filename': 'GW_%i.nc',
                   'diagonalize': True, 'spectral': 'summed}

        """
        if world.rank == 0:
            if log is None:
                from sys import stdout
                log = stdout
            elif isinstance(log, str):
                log = open(log, 'w')
        
        # pulay is a list [[tol1,mix1,history1],[tol2,mix2,history2],...]
        if type(pulay[0]) is not list:
            pulay = [pulay]

        g0 = self.GetNonIntGreenFunction()
        if world.rank==0:
            grid = self.GetFullEnergyList()
            print ('Active selfenergies:', ', '.join(
                self.GetNamesOfInteractingSelfEnergies(activeonly=True)),
                file=log)
            print ('Pulay: tol=%.3f, mix=%.3f, history=%i' % tuple(
                pulay[0]), file=log)
            print ('Bias:', 
                  g0.GetLeftChemicalPotential(),
                   g0.GetRightChemicalPotential(),
                  file=log)
            print ('Fermi level:', g0.GetFermiLevel(), file=log)
            print ('Grid spacing:', round(grid[1] - grid[0], 6), file=log)
            print ('Grid size:', grid[0], grid[-1], file=log)
            print ('Infinitesimal: %s (leads=%s)' % (
                g0.GetInfinitesimal(), g0.GetLeadInfinitesimal()), file=log)
            if g0.boxdata is not None:
                print >>log, 'Box data: eta=%s, start=%s, end=%s, T=%s' %tuple(
                    g0.boxdata)
            print ('Non-uniform grid data:', g0.GetNUGridData(), file=log)
            print ('Number of orbitals:', self.GetMatrixDimension()[0],
                   file=log)
            print ( 'Number of processors:', world.size, file=log)
            print ('Energy points per processors:', len(grid)//world.size,
                   file=log)
            for se in self.GetListOfInteractingSelfEnergies():
                if hasattr(se, 'oversample'):
                    print (se.GetName(), 'oversample', se.oversample, file=log)
                if hasattr(se, 'p2b'):
                    print (se.GetName(), 'orbital sets per processor',
                          se.p2b[0], '/', se.p2b[-1], file=log)
                if hasattr(se, 'p2p'):
                    print (se.GetName(),'pairorbital sets per processor',
                          se.p2p[0], '/', se.p2p[-1], file=log)
            log.flush()
        
        for p in pulay:
            SelfConsistent(self, p[0], p[1], p[2], log=log, doslog=doslog)

    def GetInteractingHamiltonian(self):
        """Return H0 + \sum Sigma, where Sigma is all non-correlated
        (frequency-independent) interaction self energies.
        I.e. Sigma includes xc, hartree, fock, but *not* leads or GW corr.
        """
        H = self.GetHamiltonianRepresentation().copy()
        for se in self.GetListOfInteractingSelfEnergies(activeonly=True):
            if not se.Correlated():
                H += se.GetRetarded()
        return H
    
    def WriteSpectralInfoToNetCDFFile(self, filename, orbitals=None,
                                      diagonalize=False, spectral=None):
        """spectral key can be None, 'summed', or 'individual'"""
        # The non-interacting GF
        g0 = self.GetNonIntGreenFunction()

        # Energy grids
        energies = self.GetFullEnergyList()
        NUenergies = self.GetNUEnergyList()
        NUindices = self.GetNUGridIndices()
        Ne = len(energies)
        NUE = len(NUenergies)

        # Start by finding "zero order" wavefunctions
        H = g0.GetHamiltonianRepresentation()
        S = g0.GetIdentityRepresentation()

        # Cut out subspace if specified
        if orbitals is not None:
            H = PermuteMatrix(H, orbitals)
            S = PermuteMatrix(S, orbitals)
            #print 'hej'
            def Rotate(array):
                array2 = PermuteMatrix(array, orbitals)
                #print 'array2.shape', array2.shape
                return RotateMatrix(array2, U)
                #return RotateMatrix(PermuteMatrix(array, orbitals), U)
        else:
            def Rotate(array):
                return RotateMatrix(array, U)

        Norb = len(H)
        if diagonalize:
            eig, U = np.linalg.eig(np.linalg.solve(S, H))
            indices = eig.real.argsort()
            eig = eig.real[indices]
            U = U[:, indices]
        else:
            # Simply use the diagonal values of WFs
            eig = H.real.diagonal()
            U = np.identity(Norb, complex)

        # Normalize orbitals
        T = RotateMatrix(S, U)
        U /= np.sqrt(T.diagonal())

        # Prepare file
        # ONLY proccess 0 writes the file
        if world.rank == 0:
            file = NetCDFFile(filename, 'w')
            file.createDimension("NEnergies", Ne)
            file.createDimension("NOrbitals", Norb)
            file.createDimension("Single", 1)
            file.createVariable("Energies", 'd', ("NEnergies",))[:] = energies
            file.createVariable("Orbitals", 'd',
                                ("NOrbitals",))[:] = np.arange(Norb)
            file.createVariable("Imaginary", 'd',
                                ("Single",))[:] = g0.GetInfinitesimal()

            # zero-order energies
            file.createVariable("E0", 'd', ("NOrbitals",))[:] = eig

        # Write various self-energies:
        hf_rot = eig.copy()
        active_names = self.GetNamesOfInteractingSelfEnergies(activeonly=True)
        if 'fock' in active_names:
            fock = self.GetSelfEnergy('fock').GetRetarded()
            if 'x_correction' in active_names:
                fock += self.GetSelfEnergy('x_correction').GetRetarded()
            fock_rot = Rotate(fock).real.diagonal()
            hf_rot += fock_rot
            if world.rank == 0:
                file.createVariable("fock_energy", 'd',
                                    ("NOrbitals",))[:] = fock_rot
            del fock, fock_rot
        if 'xc' in active_names:
            xc = self.GetSelfEnergy('xc').GetRetarded()
            xc_rot = Rotate(xc).real.diagonal()
            hf_rot += xc_rot
            if world.rank == 0:
                file.createVariable("xc_energy", 'd',
                                    ("NOrbitals",))[:] = -xc_rot
            del xc, xc_rot
        if 'hartree' in active_names:
            hartree = self.GetSelfEnergy('hartree').GetRetarded()
            hartree_rot = Rotate(hartree).real.diagonal()
            hf_rot += hartree_rot
            if world.rank == 0:
                file.createVariable("hartree_energy", 'd',
                                    ("NOrbitals",))[:] = hartree_rot
            del hartree, hartree_rot
        if 0:#'GW' in active_names and spectral:
        #if 'GW' in active_names and spectral is not None:
            corr_rot = self.GetSelfEnergy('GW').GetRetardedGW().copy()
            #print corr_rot.shape
            #print U.shape
            #print len(orbitals)
            for corr in corr_rot:
                #print corr.shape
                corr[:] = Rotate(corr)
            corr_rot = collect_energies(corr_rot)
            if world.rank == 0:
                corr_rot = LinearInterpolation(corr_rot, NUindices, Ne)
                for i in range(Norb):
                    file.createVariable("corr_GW_re_" + str(i), 'd', (
                        "NEnergies",))[:] = corr_rot[:, i, i].real
                    file.createVariable("corr_GW_im_" + str(i), 'd', (
                        "NEnergies",))[:] = corr_rot[:, i, i].imag
            del corr_rot # Clean up

        # Write HF energies
        if world.rank == 0:
            file.createVariable("HF_energy",'d', ("NOrbitals",))[:] =  hf_rot
            # Write the "HF line"
            for i in range(Norb):
                file.createVariable('E_line_%i' % i, 'd',
                                    ('NEnergies',))[:] = energies - hf_rot[i]
        
        # Write spectral functions
        if spectral is not None:
            dos_int = np.empty([NUE, Norb])
            dos_nonint = np.empty([NUE, Norb])
            for i in range(NUE):
                # First get the "true" GFs from overlap GFs and rotate (SGS).
                Gr  = Rotate(RotateMatrix(self.GetRetardedList()[i],
                                          g0.GetIdentityRepresentation()))
                Gr0 = Rotate(RotateMatrix(g0.GetRetardedList()[i],
                                          g0.GetIdentityRepresentation()))
                dos_int[i] = -Gr.imag.diagonal() / np.pi
                dos_nonint[i] = -Gr0.imag.diagonal() / np.pi
            dos_int = collect_energies(dos_int)
            dos_nonint = collect_energies(dos_nonint)
            if world.rank == 0:
                if spectral == 'summed':
                    dos_int = LinearInterpolation(
                        dos_int.sum(1), NUindices, Ne)
                    dos_nonint = LinearInterpolation(
                        dos_nonint.sum(1), NUindices, Ne)
                    file.createVariable('reduced_dos', 'd',
                                        ('NEnergies',))[:] = dos_int
                    file.createVariable('reduced_dos_nonint', 'd',
                                        ('NEnergies',))[:] = dos_nonint
                else:
                    dos_int = LinearInterpolation(dos_int, NUindices, Ne)
                    dos_nonint = LinearInterpolation(dos_nonint, NUindices, Ne)
                    for i in range(Norb):
                        file.createVariable("spec_" + str(i), 'd',
                                            ("NEnergies",))[:] = dos_int[:, i]
                        file.createVariable("spec0_" + str(i), 'd',
                                            ("NEnergies",))[:]=dos_nonint[:, i]
            del Gr, Gr0, dos_int, dos_nonint # Clean up

        # Write transmission function
        trans = self.GetTransmissionFunction()
        trans = collect_energies(trans)
        if world.rank == 0:
            trans = LinearInterpolation(trans, NUindices, Ne)
            file.createVariable('transmission', 'd', ('NEnergies',))[:] = trans
        del trans

        # Write transmission function using Ferretti correction for int
        trans = self.GetTransmissionFunction(ferretti=True)
        trans = collect_energies(trans)
        if world.rank == 0:
            trans = LinearInterpolation(trans, NUindices, Ne)
            file.createVariable('transmission_ferretti',
                                'd', ('NEnergies',))[:] = trans
        del trans

        # Write Total DOS
        dos = self.GetDensityOfStates()
        dos = collect_energies(dos)
        if world.rank == 0:
            dos = LinearInterpolation(dos, NUindices, Ne)
            file.createVariable('tot_dos', 'd', ('NEnergies',))[:] = dos
        del dos

        # Write Non-interacting Total DOS
        nonintdos = g0.GetDensityOfStates()
        nonintdos = collect_energies(nonintdos)
        if world.rank == 0:
            nonintdos = LinearInterpolation(nonintdos, NUindices, Ne)
            file.createVariable('nonint_tot_dos', 'd',
                                ('NEnergies',))[:] = nonintdos
        del nonintdos

        # Close down nicely
        if world.rank == 0:
            file.sync()    
            file.close()
