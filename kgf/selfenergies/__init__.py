import numpy as np


class NonEqIntSelfEnergy:
    def __init__(self, nonintgreenfunction, name, correlated=False):
        self.nonintgf = nonintgreenfunction
        self.name = name
        self.shape = nonintgreenfunction.GetMatrixDimension()
        self.SetActive(True)
        self.correlated = correlated

    def SetName(self,name):
        self.name=name

    def GetName(self):
        return self.name

    def SetNonIntGreenFunction(self, nonintgf):
        self.nonintgf = nonintgf

    def GetNonIntGreenFunction(self):
        return self.nonintgf

    def Active(self):
        return self.activity

    def SetActive(self, activity):
        self.activity = activity

    def Correlated(self):
        return self.correlated

    def GetRetarded(self, index, conv=False):
        # Always use this method for the retarded self energy (don't ask
        # for the list).
        # This ensures proper updates. 
        # Remember to ask for Update() before returning the self-energy. 
        pass

    def GetLesser(self, index):
        # Always use this method for the retarded self energy (don't ask
        # for the list).
        # This ensures proper updates. 
        # Remember to ask for Update() before returning the self-energy. 
        pass

    def Unregister(self):
        pass

    def Update(self, conv=False):
        pass
        

class NonEqConstantSelfEnergy(NonEqIntSelfEnergy):
    def __init__(self, nonintgreenfunction, sigma=None, name='constant'):
        NonEqIntSelfEnergy.__init__(self, nonintgreenfunction, name,
                                    correlated=False)
        self.SetSigma(sigma)

    def GetSigma(self):
        if self.sigma is None: self.Update()
        return self.sigma

    def SetSigma(self, sigma):
        self.sigma = sigma
    
    def GetLesser(self, index):
        return np.zeros(self.shape, complex)

    def GetGreater(self, index):
        return np.zeros(self.shape, complex)
        
    def GetRetarded(self, index=None, converged=False):
        return self.GetSigma()   

    def Unregister(self, gf=False, leadgf=False):
        if gf:
            self.nonintgf.Unregister(leadgf)


class BoxProbe(NonEqIntSelfEnergy):
    """Box shaped Buttinger probe.
    
    Kramers-kroning: real = H(imag); imag = -H(real)
    """
    def __init__(self, g0, eta, a, b, T=0.3, plot=None):
        raise DeprecationWarning('Use the box probe native to a NonEq GF')
        NonEqIntSelfEnergy.__init__(self, g0, name='boxprobe', correlated=True)

        from kgf.Hilbert import hilbert
        from kgf.Tools import FermiDistribution

        energies = g0.GetFullEnergyList()
        mynuindices = g0.GetMyNUGridIndices()

        # Construct retarded box selfenergy
        se = np.empty(len(energies), complex)
        se.imag = .5 * (np.tanh(.5 * (energies - a) / T) -
                        np.tanh(.5 * (energies - b) / T))
        se.real = hilbert(se.imag)
        se.imag -= 1
        se *= eta
        self.retarded_e = se

        # Use the dissipation-fluctuation theorem
        # S^<(e) = -fermi(e) [S^r - S^a] = -2.j * fermi(e) * Im(S^r)
        # To determine the lesser selfenergy
        fermi_e = FermiDistribution(energies - g0.GetFermiLevel(),
                                    g0.GetTemperature())
        self.lesser_e = -2.j * fermi_e * se.imag

        # Remember to multiply by the overlap matrix, when needed
        self.S = g0.GetIdentityRepresentation()

        if plot is not None:
            import pylab as pl
            pl.plot(energies, self.retarded_e.real, label='S^r (real)')
            pl.plot(energies, self.retarded_e.imag, label='S^r (imag)')
            pl.plot(energies, self.lesser_e.real, label='S^< (real)')
            pl.plot(energies, self.lesser_e.imag, label='S^< (imag)')
            pl.axis('tight')
            pl.axis(xmin=a-10, xmax=b+10)
            pl.legend(loc='best')
            pl.savefig(plot)
            print ('plot complete')

        # Truncate to the NU energies on this cpu
        self.retarded_e = self.retarded_e.take(mynuindices)
        self.lesser_e = self.lesser_e.take(mynuindices)
         # XXX temporary check
        assert len(self.retarded_e) == len(g0.GetNUEnergyList())

    def Update(self, conv=False):
        pass

    def GetRetarded(self, index):
        return self.retarded_e[index] * self.S

    def GetLesser(self, index):
        return self.lesser_e[index] * self.S

    def Unregister(self, gf=False, leadgf=False):
        if gf:
            self.nonintgf.Unregister(leadgf)
