import numpy as np
from kgf.selfenergies import NonEqConstantSelfEnergy, NonEqIntSelfEnergy
from kgf.Tools import TranslateAlongAxis0NonPeriodic as Translate
from kgf.Hilbert import hilbert
import pylab as pl

class PhononHartreeSelfEnergy(NonEqConstantSelfEnergy):
    def __init__(self, nonintgf, M_lii, Omega_l):
        NonEqConstantSelfEnergy.__init__(self, nonintgf, name='phonon_hartree')

        # Set the parameters. This also forces an Unregister
        self.SetMassMatrix(M_lii)
        self.SetFrequencyList(Omega_l)

    def GetSigma(self):
        return self.GetRetarded(0)

    def GetRetarded(self, energy, converged=False):
        if self.update_needed:
            self.Update()
        return self.retarded_ii

    def SetMassMatrix(self, M_lii):
        self.M_lii = M_lii
        self.Unregister()
        
    def SetFrequencyList(self, Omega_l):
        self.Omega_l = Omega_l
        self.Unregister()

    def Update(self, conv=False):
        """
          -- H,r  --     2 i     l  /    --  <     l
          >     = >   --------- M   |dw  >  G (w) M
          --      --  pi Omega   ij /    --  mn  nm
            ij    l           l          mn
        """
        # Allocate space for retarded array
        self.retarded_ii = np.zeros(self.shape, complex)

        # Get lesser Green function
        less_eii = self.nonintgf.GetLesserList()

        # Energy grid-spacing
        de = self.nonintgf.GetEnergyList()[1]-self.nonintgf.GetEnergyList()[0]

        # Loop over eigen-frequencies
        for Omega, M_ii in zip(self.Omega_l, self.M_lii):
            # Integrate over the trace of G_less times M
            int_trace = np.sum(np.trace(np.dot(less_eii, M_ii),
                                        axis1=1, axis2=2)) * de
            
            # Update retarded phonon Hartree self-energy
            self.retarded_ii += M_ii * 2.j * int_trace / (np.pi * Omega)

        self.update_needed = False

    def Unregister(self):
        self.update_needed = True


class PhononFockSelfEnergy(NonEqIntSelfEnergy):
    def __init__(self, nonintgf, M_lii, Omega_l):
        NonEqIntSelfEnergy.__init__(self, nonintgf, 'phonon_fock')

        # Set the parameters. This also forces an Unregister
        self.SetMassMatrix(M_lii)
        self.SetFrequencyList(Omega_l)

    def GetRetarded(self, index, converged=False):
        if self.update_needed:
            self.Update()
        return self.retarded_eii[index]

    def GetGreater(self, index):
        if self.update_needed:
            self.Update()
        return self.greater_eii[index]

    def GetLesser(self, index):
        if self.update_needed:
            self.Update()
        return self.lesser_eii[index]

    def SetMassMatrix(self, M_lii):
        self.M_lii = M_lii
        self.Unregister()
        
    def SetFrequencyList(self, Omega_l):
        self.Omega_l = Omega_l
        self.Unregister()

    def GetTempFactor(self):
        return 10.0

    def GetCurrentBounds(self):
        #if not hasattr(self, 'currentbounds'):
        self.EvaluateCurrentBounds()
        return self.currentbounds

    def GetCurrentGrid(self):
        b0 = self.GetCurrentBounds()[0]
        b1 = self.GetCurrentBounds()[1]
        return self.GetNonIntGreenFunction().GetNUEnergyList()[b0:b1]

    def SetCurrentBounds(self, currentbounds):
        self.currentbounds = currentbounds

    def EvaluateCurrentBounds(self):
        mu_l = self.GetNonIntGreenFunction().GetLeftChemicalPotential()
        mu_r = self.GetNonIntGreenFunction().GetRightChemicalPotential()
        efermi = self.GetNonIntGreenFunction().GetFermiLevel()
        T = self.GetNonIntGreenFunction().GetTemperature()
        upper_bound = max(mu_l + efermi, mu_r + efermi) + (
            abs(mu_l)+ abs(mu_r) + T * self.GetTempFactor())
        lower_bound = min(mu_l + efermi, mu_r + efermi) - (
            abs(mu_l) + abs(mu_r) + T * self.GetTempFactor())
        energies = self.GetNonIntGreenFunction().GetNUEnergyList()
        start = np.searchsorted(energies, lower_bound)
        end = np.searchsorted(energies, upper_bound)
        self.SetCurrentBounds([start, end])

    def Update(self, conv=False):
        """Update the retarded, greater, and lesser self-energies according to
        the rules below, where it has been assumed that the boson temperature
        is zero::
        
          -- f,r      -- --  l  /     l               l    \  l
          >     (w) = >  >  M   | diff (w) - i H[diff] (w) | M
          --          -- --  im \     mn              mn   /  nj
            ij        l  mn       

                    l      1 /  >                <             \
          where diff (w) = - | G (w - Omega ) - G (w + Omega ) |
                    ij     2 \  ij         l     ij         l  /

          and H[diff] is the Hilbert transform of this (with the sign
          convention H[f](y) = 1/pi p.v. int dx f(x) / (x - y) ).

          -- f,<        -- -- l  <              l
          >     (w)  =  >  > M  G (w + Omega ) M
          --            -- -- im mn         l   nj
            ij          l  mn    

          -- f,>        -- -- l  >              l
          >     (w)  =  >  > M  G (w - Omega ) M
          --            -- -- im mn         l   nj
            ij          l  mn    
                        
        """
        # Allocate space for retarded, greater, and lesser arrays
        energies_e = self.nonintgf.GetEnergyList()
        Ne = len(energies_e)
        de = energies_e[1] - energies_e[0]
        dim = (Ne,) + self.shape
        self.retarded_eii = np.zeros(dim, complex)
        self.greater_eii  = np.zeros(dim, complex)
        self.lesser_eii   = np.zeros(dim, complex)

        # Get lesser and greater Green functions
        Ggreat_eii = self.nonintgf.GetGreaterList()
        Gless_eii  = self.nonintgf.GetLesserList()

        # Temporary arrays
        min_eii  = np.zeros(dim, complex)
        plus_eii = np.zeros(dim, complex)

        # Loop over eigen-frequencies
        for Omega, M_ii in zip(self.Omega_l, self.M_lii):
            # Translation steps
            steps = Omega / de

            # Interpolate if steps is non-integer
            dec = steps % 1
            steps = int(steps - dec)
            min_eii.flat[:] = 0.
            plus_eii.flat[:] = 0.
            for weight in (1 - dec, dec):
                if round(weight, 5) != 0:
                    min_eii += weight * Translate(Ggreat_eii, +steps)
                    plus_eii += weight * Translate(Gless_eii, -steps)
                steps += 1

            self.greater_eii += np.dot(M_ii,
                                       np.dot(min_eii, M_ii)
                                       ).swapaxes(0, 1).copy()
            self.lesser_eii += np.dot(M_ii,
                                      np.dot(plus_eii, M_ii)
                                      ).swapaxes(0, 1).copy()
            
            diff = 0.5 * (self.greater_eii - self.lesser_eii)
            self.retarded_eii += diff 
            other_part = 1.0j * hilbert(diff)
            self.retarded_eii -= other_part

        self.update_needed = False


    def Unregister(self):
        self.update_needed = True
