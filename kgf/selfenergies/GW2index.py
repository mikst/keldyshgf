from __future__ import print_function

import pickle
import numpy as np

from gpaw.mpi import world
from kgf.Tools import \
     LinearInterpolation, TranslateAlongAxis0, Convolution, AntiConvolution
from kgf.Parallel import \
     AssignOrbitals2Procs, SliceAlongFrequency, SliceAlongOrbitals
from kgf.selfenergies import NonEqIntSelfEnergy, NonEqConstantSelfEnergy
from kgf.selfenergies.GWSelfEnergy import NonEqGWSelfEnergy as GW4index
from kgf.FourierTransform import fourierintegral, GetFourierData

class Hartree2index(NonEqConstantSelfEnergy):
    def __init__(self, g0, ijij, initialhartree=None):
        NonEqConstantSelfEnergy.__init__(self, g0, name='hartree')
        self.ijij = ijij
        if initialhartree is None:
            initialhartree = self.GetFullHartree()
        self.initialhartree = initialhartree

    def GetFullHartree(self):
        D = self.GetNonIntGreenFunction().GetDensityMatrix()
        return 2 * np.diag(np.dot(D.diagonal(), self.ijij))

    def Update(self, conv=False):
        self.SetSigma(self.GetFullHartree() - self.initialhartree)

    def Unregister(self, gf=False, leadgf=False):
        self.sigma = None
        NonEqConstantSelfEnergy.Unregister(self, gf=False, leadgf=False)


class Fock2index(NonEqConstantSelfEnergy):
    def __init__(self, g0, ijij):
        NonEqConstantSelfEnergy.__init__(self, g0, name='fock')
        self.ijij = ijij
        
    def Update(self, conv=False):
        D = self.GetNonIntGreenFunction().GetDensityMatrix()
        self.SetSigma(-D.T * self.ijij)
    
    def Unregister(self, gf=False, leadgf=False):
        self.sigma = None
        NonEqConstantSelfEnergy.Unregister(self, gf=False, leadgf=False)


class GW2index(GW4index):
    def __init__(self, g0, ijij, oversample=10,
                 polarization_only=False, translate=True):
        # NOTE: The energygrid should contain 0.0 (then translations can be
        # performed on the grid).
        # Alternatively translations must be performed in Fourier space which
        # is more time-consuming.
        NonEqIntSelfEnergy.__init__(self, g0, name='GW', correlated=True)
        self.Vijij = np.asarray(ijij, complex)
        self.p2b = AssignOrbitals2Procs(g0.GetMatrixDimension()[0])
        self.p2p = self.p2b
        energies = self.GetFullEnergyList()
        self.zero_index = abs(energies).argmin() # Do not use np.searchsorted
                                                 # as zero might be a small neg
        if abs(energies[self.zero_index]) > 1e-7:
            raise IndexError('Energygrid must contain 0.0')
        if not translate:
            # set translate flag to False to avoid
            # the translations back and forth
            self.zero_index = 0

        self.oversample = oversample
        self.fourierdata = GetFourierData(len(self.GetExtendedGrid()))
        self.pol_only = polarization_only


    def WriteCOHSEX(self, name='cohsex.pckl', energy=0.0, tol=1.0e-6):
        energies = self.GetEnergyList()
        min_energy = abs(energies).min()
        zero_index = abs(energies).argmin()
        rho = self.GetNonIntGreenFunction().GetDensityMatrix()
        if min_energy < energy + tol:
            V = self.GetV()
            Wr = self.GetRetardedW()[zero_index]
            cohsex = 0.5 * (Wr - V) * np.identity(len(V)) - Wr * rho
            print (np.around(cohsex.real, 1))
            rank = world.rank
            data = {}
            data['V'] = V
            data['Wr'] = Wr
            data['cohsex'] = cohsex
            data['rho'] = rho
            fd = open(name, 'wb')
            pickle.dump(data, fd)
            fd.close()
        world.barrier()            
        

    def GetV(self):
        return self.Vijij


    def UpdateGreaterAndLesserChi(self):
        if world.rank == 0:
            print ("Calculating chi_0^g")
        energies = self.GetFullEnergyList()
        Gl = self.GetNonIntGreenFunction().GetLesserList()
        Gg = self.GetNonIntGreenFunction().GetGreaterList()
        nuindices = self.GetNonIntGreenFunction().GetNUGridIndices()
        # 1. slice along frequency axis 
        Gl = SliceAlongFrequency(Gl,self.p2b)
        Gg = SliceAlongFrequency(Gg,self.p2b)
        # 2. make Fourier transform to get G(t) AND G(-t)
        Gl = np.fft.fft(LinearInterpolation(Gl,nuindices,len(energies)),axis=0)
        Gg = np.fft.fft(LinearInterpolation(Gg,nuindices,len(energies)),axis=0)
        # 3. slice along orbital axes
        Gl = SliceAlongOrbitals(Gl,self.p2b)
        # We use that G<_ij(-t)=-G<_ji(t)^* and same for G>
        Gl_nt = -np.conjugate(np.swapaxes(Gl,1,2))
        Gg = SliceAlongOrbitals(Gg,self.p2b)
        Gg_nt=-np.conjugate(np.swapaxes(Gg,1,2))
        # 4. create chi functions in time domain
        chil = Gl * np.swapaxes(Gg_nt, 1, 2) / (2 * np.pi)
        del Gl, Gg_nt
        chig = Gg * np.swapaxes(Gl_nt, 1, 2) / (2 * np.pi)
        del Gg, Gl_nt
        # 5. slice along time axis
        chil = SliceAlongFrequency(chil, self.p2b)
        chig = SliceAlongFrequency(chig, self.p2b)
        # 6. tranform to frequency domain 
        de = energies[1] - energies[0]
        chil = -1.j * de * TranslateAlongAxis0(np.fft.ifft(chil, axis=0),
                                               self.zero_index)
        chig = -1.j * de * TranslateAlongAxis0(np.fft.ifft(chig, axis=0),
                                               self.zero_index)
        # Factor 2 for spin
        self.SetLesserChi(2 * chil)
        self.SetGreaterChi(2 * chig)

    def UpdateRetardedAndLesserGW(self):
        energies = self.GetFullEnergyList()
        de = energies[1] - energies[0]
        L = len(energies)

        extenergies = self.GetExtendedGrid()
        dex = extenergies[1] - extenergies[0]
        Lx = len(extenergies)
        nui = self.GetNonIntGreenFunction().GetNUGridIndices()

        # Rearrange representations of Gl, Gg, and Wg
        Gg = self.GetNonIntGreenFunction().GetGreaterList()
        Gl = self.GetNonIntGreenFunction().GetLesserList()
        Wg = self.GetGreaterW()
        Gg_ij = SliceAlongFrequency(Gg, self.p2b)
        Gl_ij = SliceAlongFrequency(Gl, self.p2b)
        Wg_ij = SliceAlongFrequency(Wg, self.p2b)
        Wg_ji = SliceAlongFrequency(Wg.swapaxes(1, 2), self.p2b)
        GW_ret = np.zeros_like(Gg_ij)
        GW_less = np.zeros_like(Gg_ij)

        if world.rank == 0:
            print ("Calculating GW^r and GW^l")

        for i in range(Gg_ij.shape[1]):
            ##########################################
            # Obtaining GW_r from A=1.0j*(GW^>-GW^<) #
            ##########################################
            Gg = LinearInterpolation(Gg_ij[:, i], nui, L)
            Gl = LinearInterpolation(Gl_ij[:, i], nui, L)
            Wg = LinearInterpolation(Wg_ij[:, i], nui, L)
            Wgt= LinearInterpolation(Wg_ji[:, i], nui, L)

            # Make GW> from GW^>_ij(t) = ig^>_ij(t)*W^>_ij(t)
            GWg_ij = 1.j * Convolution(Gg, Wg, self.zero_index) * de

            # Make GW< from
            # GW^<_ij(t) = ig^<_ij(t)*W^<_ij(t) = ig^<_ij(t)*W^>_ji(-t)
            GWl_ij = 1.j * AntiConvolution(Gl, Wgt, self.zero_index) *de

            GW_less[:, i] = GWl_ij.take(nui)

            ret = np.fft.fft(GWg_ij - GWl_ij, n=Lx)
            ret[Lx // 2:] = 0.0
            ret = fourierintegral(ret, dex, sign=1,
                                  data=self.fourierdata) / (Lx * dex)
            ret = ret[:L].take(nui)
            
            GW_ret[:, i] = ret

        # Rearrange back to standard representation
        GW_ret_new = SliceAlongOrbitals(GW_ret, self.p2b)
        GW_less_new = SliceAlongOrbitals(GW_less, self.p2b)
        self.SetRetardedGW(GW_ret_new)
        self.SetLesserGW(GW_less_new)
