import numpy as np
from gpaw.mpi import world
from kgf.FourierTransform import fourierintegral, GetFourierData
from kgf.Tools import LinearInterpolation
from kgf.selfenergies import NonEqIntSelfEnergy
from kgf.Parallel import \
     AssignOrbitals2Procs, SliceAlongFrequency, SliceAlongOrbitals


class NonEq2BSelfEnergy(NonEqIntSelfEnergy):
        def __init__(self, g0, energygrid, V, oversample=10):
            
            NonEqIntSelfEnergy.__init__(self,g0,'B2')
            self.SetV(V)
            self.TestEnergyGrid()
            self.SetOversample(oversample)          
            self.InitiateFourier()
            self.p2b=AssignOrbitals2Procs(g0.GetMatrixDimension()[0])

        def GetRetarded2B(self):
            if not hasattr(self,'b2'):
                self.UpdateRetarded2B()
            return self.b2

        def SetRetarded2B(self,b2):
            self.b2=b2

        def UnregisterRetarded2B(self):
            if hasattr(self,'b2'):
                del self.b2

        def GetLesser2B(self):
            return self.b2l

        def SetLesser2B(self,b2l):
            self.b2l=b2l

        def Update(self):
            self.Unregister()
            self.UpdateRetarded2B()         

        def Unregister(self):
            self.UnregisterRetarded2B()

        def Correlated(self):
            return True

        def GetTempFactor(self):
            # This factor determines the size of the thermo-window
            return 10

        def SetOversample(self,oversample):
            self.oversample=oversample

        def GetOversample(self):
            return self.oversample

        def TestEnergyGrid(self):
            # Test if 0.0 is contained in the energygrid
            en=self.GetFullEnergyList()
            test=[]
            for i in range(len(en)):
                test.append(round(1.0e8*en[i]))
            if not (0.0 in test):
                if world.rank==0:
                    print "'energygrid1' must contain 0.0"
                return 0

        def GetIndex(self,energygrid,val):
            for i in range(len(energygrid)):
                if energygrid[i]+1.0e-7>=val:
                    return i

        def SetV(self,V):
            self.V=V

        def GetV(self):
            return self.V

        def GetEnergyList(self):
            return self.GetNonIntGreenFunction().GetEnergyList()

        def GetFullEnergyList(self):
            return self.GetNonIntGreenFunction().GetFullEnergyList()

        def GetExtendedGrid(self):
            if not hasattr(self,'extgrid'):
                grid=self.GetFullEnergyList()
                N=self.GetOversample()*len(grid)
                # Make sure that grid lenght is a power of 2
                count = 0
                while N != 1:
                    if N%2==1:
                        N+=1
                    N/=2
                    count+=1
                N = 2**(count) 
                start=grid[0]
                grid2=np.arange(0,N)*(grid[1]-grid[0])+start
                for i in range(len(grid2)):
                    grid2[i]=round(grid2[i],8)
                self.extgrid=grid2
            return self.extgrid

        def UnregisterExtendedEnergyList(self):
            del self.extgrid

        def GetCurrentBounds(self):
            if not hasattr(self,'currentbounds'):
                self.EvaluateCurrentBounds()
            return self.currentbounds

        def GetCurrentGrid(self):
            b0 = self.GetCurrentBounds()[0]
            b1 = self.GetCurrentBounds()[1]
            return self.GetNonIntGreenFunction().GetNUEnergyList()[b0:b1]

        def SetCurrentBounds(self,currentbounds):
            self.currentbounds=currentbounds

        def EvaluateCurrentBounds(self):
            mu_l=self.GetNonIntGreenFunction().GetLeftChemicalPotential()
            mu_r=self.GetNonIntGreenFunction().GetRightChemicalPotential()
            efermi=self.GetNonIntGreenFunction().GetFermiLevel()
            T=self.GetNonIntGreenFunction().GetTemperature()
            upper_bound=max(mu_l+efermi,mu_r+efermi)+abs(mu_l)+abs(mu_r)+T*self.GetTempFactor()
            lower_bound=min(mu_l+efermi,mu_r+efermi)-abs(mu_l)-abs(mu_r)-T*self.GetTempFactor()
            energies=self.GetNonIntGreenFunction().GetNUEnergyList()
            start=np.searchsorted(energies,lower_bound)
            end=np.searchsorted(energies,upper_bound)
            self.SetCurrentBounds([start,end])
            
        def InitiateFourier(self):
            self.fourierdata=GetFourierData(len(self.GetExtendedGrid()))
            
        def ExtendArray(self,f,grid1,grid2):
            # Extends f (on grid1) by 0 (on grid2) along axis 0
            diff=self.GetIndex(grid2,0.0)-self.GetIndex(grid1,0.0)
            dim=f.shape
            s=[len(grid2)]+list(f.shape[1:])
            h=np.zeros(s,complex)
            h[diff:diff+len(grid1)]=f
            return h

        def GetRetardedFunctionFromAInTime(self,A,grid):
            # Calculates the retarded function, F^r, in time domain from 
            # the spectral function: A = i(F^r-F^a)=i(F^>-F^<) given in ENERGY
            # G^r(t)=theta(-t)[G^>(t)-G^<(t)].
            # Since the function (A) is so localized in frequency space there is no effect of the 
            # end point corrections here.
            #A_time=(2*np.pi/(grid[1]-grid[0]))*fourierintegral(A,grid[0],grid[-1],grid[1]-grid[0],sign=0)[0]       
            A_time=np.fft.fft(A,axis=0)
            A_time[len(A_time)/2:]=0*A_time[len(A_time)/2:]
            return -1.0j*A_time

        def GetRetardedFunctionFromA(self,A,grid):
            delta = grid[1]-grid[0]
            A_time =self.GetRetardedFunctionFromAInTime(A, grid)
            A_time2 = fourierintegral(A_time, delta, sign=1, data=self.fourierdata)/ (len(grid) * delta)
            #A_time2=np.fft.ifft(A_time, axis=0)
            return A_time2

        def GetGreaterAndLesser2B(self):
            if world.rank==0:
                print "Calculating 2B< and 2B>"
            energies=self.GetFullEnergyList()
            Gl=self.GetNonIntGreenFunction().GetLesserList()
            Gg=self.GetNonIntGreenFunction().GetGreaterList()
            zero_index=self.GetIndex(energies,0.0)
            nuindices = self.GetNonIntGreenFunction().GetNUGridIndices()
            # 1. slice along frequency axis 
            Gl = SliceAlongFrequency(Gl,self.p2b)
            Gg = SliceAlongFrequency(Gg,self.p2b)
            # 2. make Fourier transform to get G(t) AND G(-t)
            Gl = np.fft.fft(LinearInterpolation(Gl,nuindices,len(energies)),axis=0)
            Gg = np.fft.fft(LinearInterpolation(Gg,nuindices,len(energies)),axis=0)
            # 3. slice along orbital axes
            Gl = SliceAlongOrbitals(Gl,self.p2b)
            # We use that G<_ij(-t)=-G<_ji(t)^* and same for G>
            Gl_nt = -np.conjugate(np.swapaxes(Gl,1,2))
            Gg = SliceAlongOrbitals(Gg,self.p2b)
            Gg_nt=-np.conjugate(np.swapaxes(Gg,1,2))
            # 4. create self-energies in time domain
            V=self.GetV()
            chil = Gl * np.swapaxes(Gg_nt,1,2) / (2*np.pi)
            Wl = np.empty(chil.shape,complex)
            for t in range(len(chil)):
                Wl[t] = np.dot(np.dot(V,chil[t]),V)
            B2l = Wl * Gl
            del Gl,Gg_nt,chil,Wl
            chig = Gg * np.swapaxes(Gl_nt,1,2) / (2*np.pi)
            Wg = np.empty(chig.shape,complex)
            for t in range(len(chig)):
                Wg[t] = np.dot(np.dot(V,chig[t]),V)
            B2g = Wg * Gg
            del Gg,Gl_nt,chig,Wg
            # 5. slice along time axis
            B2l = SliceAlongFrequency(B2l,self.p2b)
            B2g = SliceAlongFrequency(B2g,self.p2b)
            # 6. tranform to frequency domain 
            de = energies[1] - energies[0]
            B2l =  de**2 * np.fft.ifft(B2l,axis=0) / (2*np.pi)
            B2g =  de**2 * np.fft.ifft(B2g,axis=0) / (2*np.pi)
            # Factor 2 for spin
            return 2*B2g,2*B2l
            

        def UpdateRetarded2B(self):
            energies=self.GetFullEnergyList()
            extenergies=self.GetExtendedGrid()
            nuindices=self.GetNonIntGreenFunction().GetNUGridIndices()
            diff=self.GetIndex(extenergies,0.0)-self.GetIndex(energies,0.0)
            B2g,B2l = self.GetGreaterAndLesser2B()
            s = (len(nuindices),)+B2l.shape[1:]
            B2_ret=np.zeros(s,complex)
            dim=B2l.shape
            if world.rank==0:
                print "Calculating 2B^r"
            for ij in range(dim[1]):
                #for j in range(dim[2]):
                    A_ij=1.0j*(B2g[:,ij]-B2l[:,ij])
                    extA_ij=self.ExtendArray(A_ij,energies,extenergies)
                    B2_ret_full=self.GetRetardedFunctionFromA(extA_ij,extenergies)[diff:diff+len(energies)]
                    B2_ret[:,ij]=np.take(B2_ret_full,nuindices,axis=0).copy()
            del B2g
            B2l = np.take(B2l,nuindices,axis=0).copy()
            B2l = SliceAlongOrbitals(B2l,self.p2b)
            B2_ret = SliceAlongOrbitals(B2_ret,self.p2b)
            self.SetRetarded2B(B2_ret)
            self.SetLesser2B(B2l)

        def GetRetarded(self,index):
            return self.GetRetarded2B()[index]

        def GetLesser(self,index):      
            return self.GetLesser2B()[index]


class NonEq2XSelfEnergy(NonEqIntSelfEnergy):
        def __init__(self,g0,energygrid,V,oversample=10):

            NonEqIntSelfEnergy.__init__(self,g0,'X2')
            self.SetV(V)
            self.TestEnergyGrid()
            self.SetOversample(oversample)
            self.InitiateFourier()
            self.p2b=AssignOrbitals2Procs(g0.GetMatrixDimension()[0])

        def GetRetarded2X(self):
            if not hasattr(self,'x2'):
                self.UpdateRetarded2X()
            return self.x2

        def SetRetarded2X(self,x2):
            self.x2=x2

        def UnregisterRetarded2X(self):
            if hasattr(self,'x2'):
                del self.x2

        def GetLesser2X(self):
            return self.x2l

        def SetLesser2X(self,x2l):
            self.x2l=x2l

        def Update(self):
            self.Unregister()
            self.UpdateRetarded2X()

        def Unregister(self):
            self.UnregisterRetarded2X()

        def Correlated(self):
            return True

        def GetTempFactor(self):
            # This factor determines the size of the thermo-window
            return 10

        def SetOversample(self,oversample):
            self.oversample=oversample

        def GetOversample(self):
            return self.oversample

        def TestEnergyGrid(self):
            # Test if 0.0 is contained in the energygrid
            en=self.GetFullEnergyList()
            test=[]
            for i in range(len(en)):
                test.append(round(1.0e8*en[i]))
            if not (0.0 in test):
                if world.rank==0:
                    print "'energygrid1' must contain 0.0"
                return 0

        def GetIndex(self,energygrid,val):
            for i in range(len(energygrid)):
                if energygrid[i]+1.0e-7>=val:
                    return i

        def SetV(self,V):
            self.V=V

        def GetV(self):
            return self.V

        def GetEnergyList(self):
            return self.GetNonIntGreenFunction().GetEnergyList()

        def GetFullEnergyList(self):
            return self.GetNonIntGreenFunction().GetFullEnergyList()

        def GetExtendedGrid(self):
            if not hasattr(self,'extgrid'):
                grid=self.GetFullEnergyList()
                N=self.GetOversample()*len(grid)
                # Make sure that grid lenght is a power of 2
                count = 0
                while N != 1:
                    if N%2==1:
                        N+=1
                    N/=2
                    count+=1
                N = 2**(count)
                start=grid[0]
                grid2=np.arange(0,N)*(grid[1]-grid[0])+start
                for i in range(len(grid2)):
                    grid2[i]=round(grid2[i],8)
                self.extgrid=grid2
            return self.extgrid

        def UnregisterExtendedEnergyList(self):
            del self.extgrid

        def GetCurrentBounds(self):
            if not hasattr(self,'currentbounds'):
                self.EvaluateCurrentBounds()
            return self.currentbounds

        def GetCurrentGrid(self):
            b0 = self.GetCurrentBounds()[0]
            b1 = self.GetCurrentBounds()[1]
            return self.GetNonIntGreenFunction().GetNUEnergyList()[b0:b1]

        def SetCurrentBounds(self,currentbounds):
            self.currentbounds=currentbounds

        def EvaluateCurrentBounds(self):
            mu_l=self.GetNonIntGreenFunction().GetLeftChemicalPotential()
            mu_r=self.GetNonIntGreenFunction().GetRightChemicalPotential()
            efermi=self.GetNonIntGreenFunction().GetFermiLevel()
            T=self.GetNonIntGreenFunction().GetTemperature()
            upper_bound=max(mu_l+efermi,mu_r+efermi)+abs(mu_l)+abs(mu_r)+T*self.GetTempFactor()
            lower_bound=min(mu_l+efermi,mu_r+efermi)-abs(mu_l)-abs(mu_r)-T*self.GetTempFactor()
            energies=self.GetNonIntGreenFunction().GetNUEnergyList()
            start=np.searchsorted(energies,lower_bound)
            end=np.searchsorted(energies,upper_bound)
            self.SetCurrentBounds([start,end])


        def InitiateFourier(self):
            self.fourierdata=GetFourierData(len(self.GetExtendedGrid()))

        def ExtendArray(self,f,grid1,grid2):
            # Extends f (on grid1) by 0 (on grid2) along axis 0
            diff=self.GetIndex(grid2,0.0)-self.GetIndex(grid1,0.0)
            dim=f.shape
            s=[len(grid2)]+list(f.shape[1:])
            h=np.zeros(s,complex)
            h[diff:diff+len(grid1)]=f
            return h

        def GetRetardedFunctionFromAInTime(self,A,grid):
            # Calculates the retarded function, F^r, in time domain from
            # the spectral function: A = i(F^r-F^a)=i(F^>-F^<) given in ENERGY
            # G^r(t)=theta(-t)[G^>(t)-G^<(t)].
            # Since the function (A) is so localized in frequency space there is no effect of the
            # end point corrections here.
            #A_time=(2*np.pi/(grid[1]-grid[0]))*fourierintegral(A,grid[0],grid[-1],grid[1]-grid[0],sign=0)[0]
            A_time=np.fft.fft(A,axis=0)
            A_time[len(A_time)/2:]=0*A_time[len(A_time)/2:]
            return -1.0j*A_time

        def GetRetardedFunctionFromA(self,A,grid):
            delta = grid[1]-grid[0]
            A_time =self.GetRetardedFunctionFromAInTime(A, grid)
            A_time2 = fourierintegral(A_time, delta, sign=1, data=self.fourierdata)/ (len(grid) * delta)
            #A_time2=np.fft.ifft(A_time, axis=0)
            return A_time2

        def GetGreaterAndLesser2X(self):
            if world.rank==0:
                print "Calculating 2X< and 2X>"
            energies=self.GetFullEnergyList()
            Gl=self.GetNonIntGreenFunction().GetLesserList()
            Gg=self.GetNonIntGreenFunction().GetGreaterList()
            zero_index=self.GetIndex(energies,0.0)
            nuindices = self.GetNonIntGreenFunction().GetNUGridIndices()
            # 1. slice along frequency axis
            Gl = SliceAlongFrequency(Gl,self.p2b)
            Gg = SliceAlongFrequency(Gg,self.p2b)
            # 2. make Fourier transform to get G(t) AND G(-t)
            Gl = np.fft.fft(LinearInterpolation(Gl,nuindices,len(energies)),axis=0)
            Gg = np.fft.fft(LinearInterpolation(Gg,nuindices,len(energies)),axis=0)
            # 3. slice along orbital axes
            Gl = SliceAlongOrbitals(Gl,self.p2b)
            # We use that G<_ij(-t)=-G<_ji(t)^* and same for G>
            Gl_nt = -np.conjugate(np.swapaxes(Gl,1,2))
            Gg = SliceAlongOrbitals(Gg,self.p2b)
            Gg_nt=-np.conjugate(np.swapaxes(Gg,1,2))
            # 4. create self-energies in time domain
            V=self.GetV()
            X2l = np.empty(Gl.shape,complex)
            for j in range(Gl.shape[2]):
                for t in range(len(Gl)):
                    # In mult og matrix by vector we have (M*v)_ij = M_ij * v_j
                    Vj = V[:,j]
                    temp = np.dot((Gl[t] * Vj),Gg_nt[t]) * V
                    X2l[t,:,j] = np.sum(temp * Gl[t,:,j],axis = 1)
            X2g = np.empty(Gg.shape,complex)
            for j in range(Gg.shape[2]):
                for t in range(len(Gg)):
                    Vj = V[:,j]
                    temp = np.dot((Gg[t] * Vj),Gl_nt[t]) * V
                    X2g[t,:,j] = np.sum(temp * Gg[t,:,j],axis = 1)
            # 5. slice along time axis
            X2l = SliceAlongFrequency(X2l,self.p2b)
            X2g = SliceAlongFrequency(X2g,self.p2b)
            # 6. tranform to frequency domain
            de = energies[1] - energies[0]
            X2l =  (de/(2*np.pi))**2 * np.fft.ifft(X2l,axis=0) 
            X2g =  (de/(2*np.pi))**2 * np.fft.ifft(X2g,axis=0)
            # No spin factor for exchange diagram
            return X2g,X2l

        def UpdateRetarded2X(self):
            energies=self.GetFullEnergyList()
            extenergies=self.GetExtendedGrid()
            nuindices=self.GetNonIntGreenFunction().GetNUGridIndices()
            diff=self.GetIndex(extenergies,0.0)-self.GetIndex(energies,0.0)
            X2g,X2l = self.GetGreaterAndLesser2X()
            s = (len(nuindices),)+X2l.shape[1:]
            X2_ret=np.zeros(s,complex)
            dim=X2l.shape
            if world.rank==0:
                print "Calculating 2X^r"
            for ij in range(dim[1]):
                #for j in range(dim[2]):
                    A_ij=1.0j*(X2g[:,ij]-X2l[:,ij])
                    extA_ij=self.ExtendArray(A_ij,energies,extenergies)
                    X2_ret_full=self.GetRetardedFunctionFromA(extA_ij,extenergies)[diff:diff+len(energies)]
                    X2_ret[:,ij]=np.take(X2_ret_full,nuindices,axis=0).copy()
            del X2g
            X2l = np.take(X2l,nuindices,axis=0).copy()
            X2l = SliceAlongOrbitals(X2l,self.p2b)
            X2_ret = SliceAlongOrbitals(X2_ret,self.p2b)
            self.SetRetarded2X(X2_ret)
            self.SetLesser2X(X2l)

        def GetRetarded(self,index):
            return self.GetRetarded2X()[index]

        def GetLesser(self,index):
            return self.GetLesser2X()[index]
