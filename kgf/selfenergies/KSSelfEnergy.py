import numpy as np
from kgf.selfenergies import NonEqConstantSelfEnergy, NonEqIntSelfEnergy

"""
Find the KS potential (self-energy) that produces
a fixed electron density (occupation numbers). 
"""

class ExactKohnSham(NonEqConstantSelfEnergy):
    def __init__(self, nonintgreenfunction, targetdensity, beta=1.0):
                 
        NonEqConstantSelfEnergy.__init__(self, nonintgreenfunction,
                                         name='exactkohnsham')

        self.rho0 = np.array(targetdensity, complex)
        self.pot = self.nonintgf.GetHamiltonianRepresentation().diagonal()
        self.beta = beta
        self.SetSigma(sigma = np.zeros(self.shape, complex))

    def Update(self, conv=False):
        diff = self.rho0 - self.nonintgf.GetOccupations()
        self.pot -= diff * self.beta
        sigma = self.GetSigma().copy()
        sigma.flat[::len(sigma)+1] = self.pot
        self.SetSigma(sigma)

    def GetPotentialDifference(self):
        v0 = self.nonintgf.GetHamiltonianRepresentation().diagonal()
        return self.pot - v0

    def GetDensityDifference(self):
        return self.nonintgf.GetOccupations() - self.rho0.real  

    def Unregister(self, gf=False, leadgf=False):
        self.sigma = None
        NonEqConstantSelfEnergy.Unregister(self, gf=False, leadgf=False)



