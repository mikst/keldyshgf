from __future__ import print_function
import numpy as np


class BaseFilter:
    def __init__(self, selfenergy, leftsize, rightsize=None):
        if rightsize is None:
            rightsize = leftsize
        #assert leftsize > 0 and rightsize > 0, 'filter sizes must be nonzero'
        self.se = selfenergy
        self.ls = leftsize
        self.rs = rightsize

        # Pass trivial self-energy methods
        for name in ('Active',
                     'SetActive',
                     'GetNonIntGreenFunction',
                     'GetName',
                     'Correlated',
                     'Unregister',
                     'Update',
                     'GetSigma',
                     'GetCurrentGrid',
                     'EvaluateCurrentBounds',
                     'GetV',
                     'GetRetardedGW'):
            try:
                setattr(self, name, getattr(selfenergy, name))
            except AttributeError:
                pass


class FilterEnd(BaseFilter):
    """Filter selfenergy.

    Replace first and last submatrix of selfenergy by left/right value.
    """
    def __init__(self, selfenergy, leftvalue, leftsize,
                 rightvalue=None, rightsize=None):
        BaseFilter.__init__(self, selfenergy, leftsize, rightsize)
        if rightvalue is None:
            rightvalue = leftvalue
        self.lv = leftvalue
        self.rv = rightvalue

    def GetRetarded(self, *args, **kwargs):
        ret = self.se.GetRetarded(*args, **kwargs)
        ret[:self.ls, :self.ls] = self.lv
        ret[-self.rs:, self.se.shape[0] - self.rs:] = self.rv
        return ret

    def GetLesser(self, *args, **kwargs):
        less = self.se.GetLesser(*args, **kwargs)
        ret[:self.ls, :self.ls] = 0.0
        ret[-self.rs:, self.se.shape[0] - self.rs:] = 0.0
        return less


class FilterZeroEnd(FilterEnd):
    def __init__(self, selfenergy, leftsize, rightsize=None):
        FilterEnd.__init__(self, selfenergy, 0.0, leftsize, 0.0, rightsize)


class FilterXCEnd(FilterEnd):
    def __init__(self, selfenergy, xc, leftsize, rightsize=None):
        if rightsize is None:
            rightsize = leftsize
        leftvalue = xc[:leftsize, :leftsize]
        norb = self.se.shape[0]
        rightvalue = xc[norb - rightsize:, norb - rightsize:]
        FilterEnd.__init__(self, selfenergy, leftvalue, leftsize,
                           rightvalue, rightsize)


class FilterCenter(BaseFilter):
    """Filter selfenergy.

    Replace everything but the central submatrix of selfenergy by value.
    """
    def __init__(self, selfenergy, value, leftsize, rightsize=None):
        BaseFilter.__init__(self, selfenergy, leftsize, rightsize)
        self.value = value
        self.center = [slice(self.ls, self.se.shape[0] - self.rs)] * 2

    def GetRetarded(self, *args, **kwargs):
        ret = self.value.copy()
        ret[self.center] = self.se.GetRetarded(*args, **kwargs)[self.center]
        return ret

    def GetLesser(self, *args, **kwargs):
        less = np.zeros_like(self.value)
        less[self.center] = self.se.GetLesser(*args, **kwargs)[self.center]
        return less


#class FilterExtend(BaseFilter):
#    def __init__(self, selfenergy, value, leftsize, rightsize=None):
#        BaseFilter.__init__(self, selfenergy, leftsize, rightsize)
#        self.value = value
#        self.center = [slice(self.ls, -self.rs)] * 2
#
#    def GetRetarded(self, *args, **kwargs):
#        ret = self.value.copy()
#        ret[self.center] = self.se.GetRetarded(*args, **kwargs)
#        return ret
#
#    def GetLesser(self, *args, **kwargs):
#        less = np.zeros_like(self.value)
#        less[self.center] = self.se.GetLesser(*args, **kwargs)
#        return less


class FilterZeroCenter(FilterCenter):
    def __init__(self, selfenergy, leftsize, rightsize=None):
        value = np.zeros(
            selfenergy.GetNonIntGreenFunction().GetMatrixDimension(), complex)
        FilterCenter.__init__(self, selfenergy, value, leftsize, rightsize)


class FilterXCCenter(FilterCenter):
    def __init__(self, selfenergy, xc, leftsize, rightsize=None):
        FilterCenter.__init__(self, selfenergy, xc, leftsize, rightsize)
