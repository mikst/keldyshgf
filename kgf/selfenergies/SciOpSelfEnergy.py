import numpy as np
from kgf.selfenergies import NonEqConstantSelfEnergy, NonEqIntSelfEnergy
la = np.linalg

"""
Scissors operator that only works on a subspace 
"""

class SciOp(NonEqConstantSelfEnergy):
    def __init__(self, nonintgreenfunction, bfsi, shifts):
        NonEqConstantSelfEnergy.__init__(self, nonintgreenfunction,
                                         name='SciOp')

        assert len(bfsi) == len(shifts)
        self.bfsi, self.shifts = bfsi, np.asarray(shifts)
        Hsub = self.nonintgf.GetHamiltonianRepresentation().take(bfsi, axis=0)
        Hsub = Hsub.take(bfsi, axis=1)
        Ssub = self.nonintgf.GetIdentityRepresentation().take(bfsi, axis=0)
        Ssub = Ssub.take(bfsi, axis=1) 
        eps, R = la.eig(la.solve(Ssub, Hsub))
        sort_list = eps.real.argsort()
        self.eps = eps[sort_list]
        R = R.take(sort_list, axis=1)
        norms_n = np.dot(np.dot(R.T.conj(), Ssub), R).diagonal()
        R /= np.sqrt(norms_n)
        SO = np.dot(Ssub, R) * self.shifts # multiply cols by shifts
        SO = np.dot(np.dot(SO, R.T.conj()), Ssub)
        self.eps_shifted = la.eigvals(la.solve(Ssub, Hsub + SO))
        self.eps_shifted.sort()
        assert abs(self.eps + self.shifts - self.eps_shifted).max() < 1.0e-10

        sigma = np.zeros(self.shape, complex)
        for i1, i in enumerate(bfsi): # fill in SO 
            for j1, j in enumerate(bfsi):
                sigma[i, j] = SO[i1, j1]
        
        self.SetSigma(sigma=sigma)

    def Unregister(self, gf=False, leadgf=False):
        assert 0, 'SciOp.Unregister called'
        self.sigma = None # Hmmm it will not work if this is called, I think...
        NonEqConstantSelfEnergy.Unregister(self, gf=False, leadgf=False)



