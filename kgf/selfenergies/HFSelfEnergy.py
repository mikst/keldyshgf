from __future__ import print_function

import numpy as np
from kgf.selfenergies import NonEqConstantSelfEnergy, NonEqIntSelfEnergy
from kgf.Tools import rotate, hartree_product, exchange_product

"""
The Coulomb interaction matrix V can be represented in different ways.

2) V_pp packed format: here pp corresponds to pp' in

   V_pp' = int dr int dr' / |r-r'| p*(r) p'(r')

   where p(r) = k*(r) i(r)
   and p'(r') = j*(r') l(r')

4) V_qq optimized packed format. Here, pairorbitals have been optimized and
   truncated to some extend. The V_pp matrix can be reconstructed from this
   using the matrix Usq_pq:

   V_pp = dot(Usq_pq, dot(V_qq, Usq_pq.T.conj()))


# WARNING: although all equations are implemented according to the
# above; the input V should be complex conjugated relative to this
# definition in all of the KGF code!
"""


def hartree_partial(D, V_ijij, V_ijji, V_iijj, V_iiij, V_ikjk=None):
    if type(D) == list:
        D = D[0] + D[1]
    else:
        D = 2 * D

    N = len(D)
    dtype = np.mintypecode([D.dtype.char, V_ijij.dtype.char])
    V_H = np.empty([N, N], dtype)
    if V_iijj is None:
        V_iijj = np.zeros([N, N], dtype)
    if V_iiij is None:
        V_iiij = np.zeros([N, N], dtype)
    for i in range(N):
        for j in range(N):
            if i == j:
                if V_ikjk is not None:
                    V_H[i, i] = np.sum(D * V_ikjk[:, :, i])
                else:
                    V_H[i, i] = (np.dot(D.diagonal(), V_ijij[i]) +
                                 np.dot(D[i], V_iiij[i]) +
                                 np.dot(D[:, i], V_iiij[i].conj()) -
                                 2 * D[i, i] * V_iiij[i, i])
            else:
                V_H[i, j] = (D[i, j] * V_iijj[i, j] +
                             D[j, i] * V_ijji[i, j] +
                             D[i, i] * V_iiij[i, j] +
                             D[j, j] * V_iiij[j, i].conj())
                if V_ikjk is not None:
                    V_H[i, j] += (np.dot(D.diagonal(), V_ikjk[i, j, :]) -
                                  D[i, i] * V_iiij[i, j] -
                                  D[j, j] * V_iiij[j, i].conj())
    return V_H


def fock_partial(D, V_ijij, V_ijji, V_iijj, V_iiij, V_ikjk=None):
    if type(D) == list:
        return [fock_partial(D[0], V_ijij, V_ijji, V_iijj, V_iiij, V_ikjk),
                fock_partial(D[1], V_ijij, V_ijji, V_iijj, V_iiij, V_ikjk)]
    N = len(D)
    dtype = np.mintypecode([D.dtype.char, V_ijij.dtype.char])
    V_F = np.empty([N, N], dtype)
    if V_iijj is None:
        V_iijj = np.zeros([N, N], dtype)
    if V_iiij is None:
        V_iiij = np.zeros([N, N], dtype)
    for i in range(N):
        for j in range(N):
            if i == j:
                V_F[i, i] = -(np.dot(D.diagonal(), V_ijji[i]) +
                              np.dot(D[i], V_iiij[i]) +
                              np.dot(D[:, i], V_iiij[i].conj()) -
                              2 * D[i, i] * V_iiij[i, i] +
                              D[i, i] * V_ijij[i, i] - D[i, i] * V_ijji[i, i])
            else:
                V_F[i, j] = -(D[j, i] * V_ijij[i, j] +
                              D[i, j] * V_iijj[i, j] +
                              D[i, i] * V_iiij[i, j] +
                              D[j, j] * V_iiij[j, i].conj())
                if V_ikjk is not None:
                    V_F[i, j] -= (
                        np.dot(D[:, i], V_ikjk[:, j, i]) -
                        D[i, i] * V_ikjk[i, j, i] - D[j, i] * V_ikjk[j, j, i] +
                        np.dot(D[j, :], V_ikjk[i, :, j]) -
                        D[j, j] * V_ikjk[i, j, j] - D[j, i] * V_ikjk[i, i, j])
    return V_F


class Hartree(NonEqConstantSelfEnergy):
    def __init__(self, nonintgreenfunction, V,
                 Usq_pq=None, initialhartree=None):
        NonEqConstantSelfEnergy.__init__(self, nonintgreenfunction,
                                         name='hartree')
        if V.ndim == 4:
            assert Usq_pq is None
            V = iiii2pp(V)
        self.V_qq = V
        self.Usq_pq = Usq_pq
        self.initialhartree = initialhartree

    def GetInitialHartree(self):
        if self.initialhartree is None:
            from gpaw.mpi import world
            if world.rank == 0:                
                print ("#######################################################")
                print ("### OBS: Setting initial Hartree potential to ###")
                print ("###      equilibrium Hartree potential ###")
                print ("#######################################################")
            mul = self.GetNonIntGreenFunction().GetLeftChemicalPotential()
            mur = self.GetNonIntGreenFunction().GetRightChemicalPotential()
            assert abs(mul - mur) < 1e-5, 'ERROR: You must set the chemical ' \
                   'potentials to zero before initializing Hartree potential'
            self.initialhartree = self.GetFullHartree()
        return self.initialhartree

    def GetFullHartree(self):
        D = self.GetNonIntGreenFunction().GetDensityMatrix()
        return hartree_product(D, self.V_qq, pre=2.0, rot=self.Usq_pq)

    def Update(self, conv=False):
        self.SetSigma(self.GetFullHartree() - self.GetInitialHartree())

    def Unregister(self, gf=False, leadgf=False):
        self.sigma = None
        NonEqConstantSelfEnergy.Unregister(self, gf=False, leadgf=False)


class Hartree2(Hartree):
    def __init__(self, nonintgreenfunction, V,
                 Usq_pq=None, initialhartree=None, ext_ls=None, ext_rs=None):
        Hartree.__init__(self, nonintgreenfunction, V,
                         Usq_pq, initialhartree)
       
        if ext_ls!=None and ext_rs==None:
            ext_rs = ext_ls
             
        self.center = [slice(ext_ls, -ext_rs)] * 2

    def GetFullHartree(self):
        D = self.GetNonIntGreenFunction().GetDensityMatrix()
        D = D[self.center].copy()
        wrk = np.zeros(self.shape, complex)
        wrk[self.center] = hartree_product(D, self.V_qq, pre=2.0, 
                                           rot=self.Usq_pq)
        return wrk


class HartreePartial(Hartree):
    def __init__(self, nonintgreenfunction, Vlst, initialhartree=None):
        N = self.shape[0]
        while len(Vlst) < 4:
            Vlst.append(np.zeros((N, N), complex))
        if len(Vlst) < 5:
            Vlst.append(None)
        self.vlst = Vlst
        NonEqConstantSelfEnergy.__init__(self, nonintgreenfunction,
                                         name='hartree')
        self.initialhartree = initialhartree

    def GetFullHartree(self):
        v1, v2, v3, v4, v5 = self.vlst
        D = self.GetNonIntGreenFunction().GetDensityMatrix()
        return hartree_partial(D, v1, v2, v3, v4, v5)


class Fock(NonEqConstantSelfEnergy):
    def __init__(self, nonintgreenfunction, V, Usq_pq=None, Fcore=None):
        NonEqIntSelfEnergy.__init__(self, nonintgreenfunction, 'fock')
        self.sigma = None
        self.Fcore = Fcore
        if V.ndim == 4:
            assert Usq_pq is None
            V = iiii2pp(V)
        self.V_qq = V
        self.Usq_pq = Usq_pq

    def SetSigma(self, sigma):
        if self.Fcore is None:
            return NonEqConstantSelfEnergy.SetSigma(self, sigma)
        return NonEqConstantSelfEnergy.SetSigma(self, sigma + self.Fcore)

    def Update(self, conv=False):
        D = self.GetNonIntGreenFunction().GetDensityMatrix()
        self.SetSigma(exchange_product(D, self.V_qq,
                                       pre=-1.0, rot=self.Usq_pq))
    
    def Unregister(self, gf=False, leadgf=False):
        self.sigma = None
        NonEqConstantSelfEnergy.Unregister(self, gf=False, leadgf=False)


class Fock2(Fock):
    def __init__(self, nonintgreenfunction, V, Usq_pq=None, Fcore=None,
                 ext_ls=None, ext_rs=None):
        Fock.__init__(self, nonintgreenfunction, V, Usq_pq, Fcore)
        
        if ext_ls!=None and ext_rs==None:
            ext_rs = ext_ls
        self.center = [slice(ext_ls, -ext_rs)] * 2

        tmp = np.zeros(self.shape)
        assert tmp[self.center].shape[0]**2 == Usq_pq.shape[0]
        del tmp

    def SetSigma(self, sigma):
        if self.Fcore is None:
            return NonEqConstantSelfEnergy.SetSigma(self, sigma)
        Fcore = np.zeros(self.shape, complex)
        Fcore[self.center] = self.Fcore
        return NonEqConstantSelfEnergy.SetSigma(self, sigma + Fcore)

    def Update(self, conv=False):
        D = self.GetNonIntGreenFunction().GetDensityMatrix()
        D = D[self.center].copy()
        wrk = np.zeros(self.shape, complex)
        wrk[self.center] = exchange_product(D, 
                                            self.V_qq, pre=-1.0, 
                                            rot=self.Usq_pq)
        self.SetSigma(wrk)


class FockPartial(Fock):
    def __init__(self, nonintgreenfunction, Vlst, Fcore=None):
        N = self.shape[0]
        while len(Vlst) < 4:
            Vlst.append(np.zeros((N, N), complex))
        if len(Vlst) < 5:
            Vlst.append(None)
        self.vlst = Vlst
        NonEqIntSelfEnergy.__init__(self, nonintgreenfunction, 'fock')
        self.sigma = None
        self.Fcore = Fcore        

    def Update(self,conv=False):
        v1, v2, v3, v4, v5 = self.vlst
        D = self.GetNonIntGreenFunction().GetDensityMatrix()
        self.SetSigma(fock_partial(D, v1, v2, v3, v4, v5))
