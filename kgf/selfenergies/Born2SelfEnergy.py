import numpy as np
from gpaw.mpi import world
from kgf.FourierTransform import fourierintegral, GetFourierData
from kgf.Tools import LinearInterpolation, gemm,\
     exchange_product, polarization_product
from kgf.selfenergies import NonEqIntSelfEnergy
from kgf.Parallel import \
     AssignOrbitals2Procs, SliceAlongFrequency, SliceAlongOrbitals


def IncreaseToPowerOfTwo(N):
    count = 0
    while N != 1:
        if N % 2 == 1:
            N += 1
        N /= 2
        count += 1
    return 2**count


class Born2SelfEnergy(NonEqIntSelfEnergy):
    def __init__(self, g0, V_qq, U_pq_seq=None, oversample=10,
                 polarization=True, cross=True):
        # NOTE: The energygrid of g0, should contain 0.0, so that translations
        # can be performed on the grid.
        #
        # Inut parameters:
        #
        # g0           A noninteracting GF
        # V_qq         Is the truncated interaction in the basis of
        #              optimized pair densities
        # U_pq_seq     is the rotation matrix that diagonalises S_pp
        #              multiplied by the squareroot of its eigenvalues: 
        #              U_pq_seq = sqrt(e_q)*U_pq,
        #              where dagger(U_pq) . S_pp . U_pq = e_q
        # oversample   The length of the extended grid will be approximately
        #              oversample * len(g0 energygrid). The actual length used
        #              is increased to be a power of two.
        # polarization Boolean specifying of the polarization diagram should
        #              be included
        # cross        Boolean specifying of the cross diagram should
        #              be included
        #
        #                _\|          _/
        #                  \ i      k /|
        #  Interaction:     xxxxxxxxxx  =     V_ij,kl
        #                 |/_j      l \_    (pair density repr.)
        #                 /           |\
        #
        #   V_ij,kl = \int \int dr dr' n_ij(r)^* n_kl(r') / |r-r'|
        #
        #   n_ij(r) = phi_i(r)^* phi_j(r)
        #
        # WARNING: although all equations are implemented according to the
        # above; the input V should be complex conjugated relative to this
        # definition in all of the KGF code!
        #
        #
        # The polarization diagram:
        #
        #              ____\___
        #             /    /   \
        #            /p        r\  
        #     t    xx            xx  t'
        #          x \q        s/ x
        #          x  \____/___/  x
        #          x       \      x
        #          x              x
        #          x______\_______x
        #        _/  k    /    l _\|
        #       i/|                \j
        #
        #
        # In Keldysh space: (factor two for spin)
        # P_ij(t, t') = 2 sum_klpqrs G_kl(t, t') G_pr(t, t') G_sq(t', t)
        #                                                V_ik,pq V_rs,jl
        #
        # In real space: (Using that G>_ij(-t) = -G>*_ji(t) )
        # P<_ij(t) =  2 sum_klpqrs G<_kl(t) G<_pr(t) G>_sq(-t) V_ik,pq V_rs,jl
        # P<_ij(t) = -2 sum_klpqrs G<_kl(t) G<_pr(t) G>*_qs(t) V_ik,pq V_rs,jl
        #
        #
        # The cross diagram:
        #   
        #    t'    s     q     t
        #        _____\_____
        #       x     /     x
        #    l|   x       x   |p
        #     |     x   x     |
        #    /|\      x      \|/
        #     |     x   x     |
        #    k|   x       x   |r
        # t     x           x    t'
        #     _/            _\|
        #    i/|              \j
        #
        # In Keldysh space: (no factor two for spin)
        # X_ij(t, t') = - sum_klpqrs G_kl(t, t') G_pr(t, t') G_sq(t', t)
        #                                                V_ik,pq V_ls,jr
        #
        # In real space: (Using that G>_ij(-t) = -G>*_ji(t) )
        # X<_ij(t) =  - sum_klpqrs G<_kl(t) G<_pr(t) G>_sq(-t) V_ik,pq V_rs,jl
        # X<_ij(t) =  + sum_klpqrs G<_kl(t) G<_pr(t) G>*_qs(t) V_ik,pq V_ls,jr
        #
        #
        # The self-energies can be combined to:
        #
        # S<_ij= P<_ij + X<_ij
        #      = - sum_klpqrs G<_kl G<_pr G>*_qs V_ik,pq ( 2 V_lp,js - V_ls,jr)
        #
        # and likewise for S>:
        #
        # S>_ij= P>_ij + X>_ij
        #      = - sum_klpqrs G>_kl G>_pr G<*_qs V_ik,pq ( 2 V_lp,js - V_ls,jr)
        #
        NonEqIntSelfEnergy.__init__(self, g0, name='Born2', correlated=True)

        # The interaction must be complex conjugated (for some reason!)
        #   (because the Hartree and Fock selfenergies are conjugated!)
        assert V_qq.dtype == complex
        self.V_qq = V_qq
        #self.V_qq = np.asarray(V_qq.conj(), complex)

        # Store the matrices:
        # U_pq . sqrt(e_q)  and  sqrt(e_q) . dagger(U_pq)
        if U_pq_seq is None:
            self.U_pq = None
            self.VU_qp = self.V_qq
        else:
            assert U_pq_seq.dtype == complex
            self.U_pq = U_pq_seq
            self.VU_qp = np.zeros(self.U_pq.T.shape, dtype=complex)
            gemm(1.0, self.U_pq, self.V_qq, 0.0, self.VU_qp, 'c')

        # Assign basis function orbitals to the different processes
        self.p2b = AssignOrbitals2Procs(g0.GetMatrixDimension()[0])

        # Analyse energy grid
        energies = self.GetFullEnergyList()
        self.zero_index = abs(energies).argmin() # Do not use np.searchsorted
                                                 # as zero might be a small neg
        if abs(energies[self.zero_index]) > 1e-7:
            raise IndexError('Energygrid must contain 0.0')
        self.oversample = oversample
        self.fourierdata = GetFourierData(len(self.GetExtendedGrid()))

        # Set active diagrams
        self.pol = polarization
        self.cross = cross

    def GetRetarded(self, index):
        if not hasattr(self, 'ret'):
            self.Update()
        return self.ret[index]

    def GetLesser(self, index):      
        if not hasattr(self, 'less'):
            self.Update()
        return self.less[index]

    def Unregister(self):
        if hasattr(self, 'ret'):
            del self.ret
        if hasattr(self, 'less'):
            del self.less

    def GetFullEnergyList(self):
        return self.GetNonIntGreenFunction().GetFullEnergyList()

    def GetExtendedGrid(self):
        if not hasattr(self, 'extgrid'):
            fullenergies = self.GetFullEnergyList()
            Ne_full = len(fullenergies)

            # Make sure that grid lenght is a power of 2
            Ne_ext = IncreaseToPowerOfTwo(self.oversample * Ne_full)

            begin_full = fullenergies[0]
            de_full = fullenergies[1] - begin_full
            extgrid = np.arange(Ne_ext) * de_full + begin_full

            self.extgrid = np.around(extgrid, 8)
        return self.extgrid

    def GetRetardedFromLesserAndGreater(self, less, great):
        # Calculates the retarded function from
        #
        # r(t) = theta(t)[ >(t) - <(t) ]
        #
        # less and great are in energy domain, and on the full energy grid
        # ret is returned in energy domain, on the nu energy grid
        nuindices = self.GetNonIntGreenFunction().GetNUGridIndices()
        fullenergies = self.GetFullEnergyList()
        extenergies = self.GetExtendedGrid()
        de_ext = extenergies[1] - extenergies[0]

        Ne_full = len(fullenergies)
        Ne_ext = len(extenergies)
        Ne_nu = len(nuindices)
        Nq = less.shape[1]
        assert len(less) == Ne_full
        
        ret = np.zeros((Ne_nu, Nq), complex)
        for i in range(Nq):
            # Make the difference (> - <)
            ret_i = great[:, i] - less[:, i]

            # Pad by zeros to extended grid, and FFT to time
            ret_i = np.fft.fft(ret_i, n=Ne_ext)

            # multiply by theta(t)
            # after fft, time is ordered such that if n is odd:
            #   index 0 is t=0, index 1:n/2+1 is t>0, and index n/2+1: is t<0
            # if n is even:
            #   index 0 is t=0, index 1:n/2 is t>0, and index n/2: is t<0
            ret_i[Ne_ext // 2:] = 0.0 # this will zero all t<0

            # iFFT back to energy using endpoint corrections
            #ret_i = np.fft.ifft(ret_i)#<- the equivalent without end-point cor
            ret_i = fourierintegral(ret_i, de_ext, sign=1,
                                    data=self.fourierdata) / (de_ext * Ne_ext)

            # Reduce ext grid to full grid and then take nu grid only to store
            ret[:, i] = ret_i[:Ne_full].take(nuindices, axis=0)
        return ret

    def Update(self):
        # Delete exissting lesser and retarded
        self.Unregister()

        # collect local variables
        Ne = len(self.GetFullEnergyList())
        Gl = self.GetNonIntGreenFunction().GetLesserList()
        Gg = self.GetNonIntGreenFunction().GetGreaterList()
        nuindices = self.GetNonIntGreenFunction().GetNUGridIndices()

        # Fourier transform Green functions to time domain
        Gl = SliceAlongFrequency(Gl, self.p2b)
        Gg = SliceAlongFrequency(Gg, self.p2b)
        Gl = np.fft.fft(LinearInterpolation(Gl, nuindices, Ne), axis=0)
        Gg = np.fft.fft(LinearInterpolation(Gg, nuindices, Ne), axis=0)
        Gl = SliceAlongOrbitals(Gl, self.p2b)
        Gg = SliceAlongOrbitals(Gg, self.p2b)

        # Make lesser and greater in time domain
        less, great = self.GetLesserAndGreaterInTime(Gl, Gg)

        # Fourier transform lesser and greater back to energy and get retarded
        less  = SliceAlongFrequency(less,  self.p2b)
        great = SliceAlongFrequency(great, self.p2b)
        less  = np.fft.ifft(less, axis=0)
        great = np.fft.ifft(great, axis=0)
        if world.rank == 0:
            print 'Calculating lesser and greater self-energy'
        ret = self.GetRetardedFromLesserAndGreater(less, great)
        del great

        # Convert to NU grid and store with parallelization over energy axis
        less = less.take(nuindices, axis=0).copy()
        self.less = SliceAlongOrbitals(less, self.p2b)
        self.ret  = SliceAlongOrbitals(ret, self.p2b)

    def GetLesserAndGreaterInTime(self, Gl, Gg):
        """Determine lesser and greater self-energy in time domain
        from lesser and greater Green functions in time domain.
        """
        energies = self.GetFullEnergyList()
        de = energies[1] - energies[0]
        Nt, Ni = Gl.shape[:2]
        if self.U_pq is None:
            V_ijkl = self.V_qq.reshape(Ni, Ni, Ni, Ni)
        else:
            V_ijkl = np.dot(self.U_pq, self.VU_qp).reshape(Ni, Ni, Ni, Ni)
        
        # Prefactor due to fft
        pre = (de / (2 * np.pi))**2

        # The self-energies
        less = np.zeros([Nt, Ni, Ni], complex)
        great = np.zeros([Nt, Ni, Ni], complex)

        if world.rank == 0:
            print 'Calculating lesser and greater self-energy'
        for t in range(Nt):
            for G_ii, Gd_ii, Sigma_ii in [(Gl[t], Gg[t].conj(), less[t]),
                                          (Gg[t], Gl[t].conj(), great[t])]:
                # The polarization and cross diagrams:
                # S<_ij = sum_klpqrs G<_kl G<_pr G>*_qs V_ik,pq (
                #                         -2 V_rs,jl    # polarization diagram
                #                         +1 V_ls,jr)   # cross diagram
                #
                # Greater follows by replacing < with > and vice-versa.

                # First do generic sum_kpq G<_kl G<_pr G>*_qs V_ik,pq 
                V_islr = np.dot(np.dot(np.dot(     # start with V_ikpq
                    V_ijkl, Gd_ii).swapaxes(1, 3), # sum q and swap => V_ispk
                            G_ii ).swapaxes(2, 3), # sum k and swap => V_islp
                            G_ii )                 # sum p          => V_islr

                # Now do term specific sums
                if self.pol:
                    Sigma_ii -= 2.0 * pre * np.dot(
                        V_islr.reshape(Ni, Ni**3),
                        V_ijkl.transpose(1, 3, 0, 2).reshape(Ni**3, Ni))
                if self.cross:
                    Sigma_ii += pre * np.dot(
                        V_islr.reshape(Ni, Ni**3),
                        V_ijkl.transpose(1, 0, 3, 2).reshape(Ni**3, Ni))

        return less, great


class Born2SelfEnergy_opt(Born2SelfEnergy):
    """This 'optimized' version overwrites GetLesserAndGreaterInTime to use
    optimized blas subroutines.
    """
    def GetLesserAndGreaterInTime(self, Gl, Gg):
        """Determine lesser and greater self-energy in time domain
        from lesser and greater Green functions in time domain.
        """
        energies = self.GetFullEnergyList()
        de = energies[1] - energies[0]
        U_pq = self.U_pq
        V_qp = self.VU_qp
        if U_pq is None:
            U_pq = np.identity(len(V_qp), complex)

        Nt, Ni = Gl.shape[:2]
        Np = Ni**2
        Nq = U_pq.shape[1]
        
        # Prefactor due to fft
        pre = de / (2 * np.pi)

        # The self-energies
        less = np.zeros([Nt, Ni, Ni], complex)
        great = np.zeros([Nt, Ni, Ni], complex)

        # Temporary work arrays
        T1_iiq = np.zeros([Ni, Ni, Nq], complex)
        T2_iiq = np.zeros([Ni, Ni, Nq], complex)
        if self.cross:
            T3_iii = np.zeros([Ni, Ni, Ni], complex)

        # Different contiguous views into work arrays
        U_iiq = U_pq.reshape(Ni, Ni, Nq)
        V_qii = V_qp.reshape(Nq, Ni, Ni)
        T1_pq = T1_iiq.reshape(Np, Nq)
        T2_pq = T2_iiq.reshape(Np, Nq)
        T1_qq = T1_iiq.reshape(-1)[:Nq**2].reshape(Nq, Nq)
        T2_iqi = T2_iiq.reshape(Ni, Nq, Ni)
        if self.cross:
            T1_qii = T1_iiq.reshape(Nq, Ni, Ni)
            T2_qii = T2_iiq.reshape(Nq, Ni, Ni)

        if world.rank == 0:
            print 'Calculating lesser and greater self-energy'

        for t in range(Nt):
            for G_ii, Gd_ii, Sigma_ii in [(Gl[t], Gg[t].conj(), less[t]),
                                          (Gg[t], Gl[t].conj(), great[t])]:
                # The polarization and cross diagrams:
                # S<_ij = sum_klpqrs G<_kl G<_pr G>*_qs V_ik,pq (
                #                         -2 V_rs,jl    # polarization diagram
                #                         +1 V_ls,jr)   # cross diagram
                #
                # Greater follows by replacing < with > and vice-versa.
                #
                # Including rotation from optimized representation, i.e.
                # using V_qq, and V_q,ij = sum_q' V_qq' Ud_q',ij, this becomes
                #
                # S<_ij = sum_klpqrs G<_kl G<_pr G>*_qs U_ik,m V_m,pq (
                #                    -2 U_rs,n V_n,jl    # polarization diagram
                #                    +1 U_ls,n V_n,jr)   # cross diagram
                if self.pol:
                    # T1_rqn = sum_s Gd_qs U_rs,n
                    for T1_iq, U_iq in zip(T1_iiq, U_iiq):
                        gemm(1.0, U_iq, Gd_ii, 0.0, T1_iq)

                    # T2_pqn = sum_r G_pr T1_rqn
                    gemm(1.0, T1_iiq, G_ii, 0.0, T2_iiq)

                    # T1_mn  = sum_pq V_mpq T2_pqn
                    gemm(1.0, T2_pq, V_qp, 0.0, T1_qq)

                    # T2_ikn = sum_m U_ikm T1_mn
                    gemm(1.0, T1_qq, U_pq, 0.0, T2_pq)

                    # T1_iln = sum_k G_kl T2_ikn
                    for T1_iq, T2_iq in zip(T1_iiq, T2_iiq):
                        gemm(1.0, T2_iq, G_ii.T.copy(), 0.0, T1_iq)

                    # T2_lnj = V_njl
                    T2_iqi[:] = V_qp.reshape(Nq, Ni, Ni).transpose(2, 0, 1)

                    # Sigma_ij -= 2 * sum_ln T1_iln T2_lnj
                    gemm(-2.0 * pre**2,
                         T2_iqi.reshape(Ni * Nq, Ni),
                         T1_iiq.reshape(Ni, Ni * Nq),
                         1.0, Sigma_ii)

                if self.cross:
                    # Sigma_ij = -sum_jklpqrsmo
                    #               G_kl G_rs Gd_qp U_ik,m V_m,rq U_lp,o V_o,js

                    # T1_mrp = sum_q V_mrq Gd_qp
                    gemm(1.0, Gd_ii, V_qii.reshape(Ni * Nq, Ni), 0.0,
                         T1_qii.reshape(Ni * Nq, Ni))     

                    # T2_mps = sum_r T1_mrp G_rs
                    for T1_ii, T2_ii in zip(T1_qii, T2_qii):
                        gemm(1.0, G_ii, T1_ii.T.conj(), 0.0, T2_ii)

                    for i, (T1_iq, U_iq) in enumerate(zip(T1_iiq, U_iiq)):
                        # T1_ilm = sum_k G_kl U_ikm
                        # T1_lm = sum_k G_kl U_ikm
                        gemm(1.0, U_iq, G_ii.T.copy(), 0.0, T1_iq)

                        # T3_lps = sum_m T1_lm T2_mps
                        gemm(1.0, T2_qii, T1_iq, 0.0, T3_iii)

                        # T1_so = sum_lp T3_lps U_lpo
                        gemm(1.0, U_pq, T3_iii.reshape(Ni**2, Ni).T.copy(),
                             0.0, T1_iq)

                        # T2_soj = V_ojs
                        T2_iqi[:] = V_qii.transpose(2, 0, 1)

                        # Sigma_i += - sum_so T1_so T2_soj
                        Sigma_ii[i] += pre**2 * np.dot(
                            T1_iq.reshape(Ni * Nq),
                            T2_iqi.reshape(Ni * Nq, Ni))
        return less, great


class Born2SelfEnergy_opt1(Born2SelfEnergy):
    """This 'optimized' version overwrites GetLesserAndGreaterInTime to use
    optimized blas subroutines.
    """
    def GetLesserAndGreaterInTime(self, Gl, Gg):
        """Determine lesser and greater self-energy in time domain
        from lesser and greater Green functions in time domain.
        """
        energies = self.GetFullEnergyList()
        de = energies[1] - energies[0]
        U_pq = self.U_pq
        V_qp = self.VU_qp
        if U_pq is None:
            U_pq = np.identity(len(V_qp), complex)

        Nt = len(Gl) # Time/energy points
        pre = de / (2 * np.pi) # Prefactor due to fft

        # The self-energies
        less = np.zeros([Nt, Ni, Ni], complex)
        great = np.zeros([Nt, Ni, Ni], complex)

        if world.rank == 0:
            print 'Calculating lesser and greater self-energy'

        for t in range(Nt):
            for G_ii, Gd_ii, Sigma_ii in [(Gl[t], Gg[t].conj(), less[t]),
                                          (Gg[t], Gl[t].conj(), great[t])]:
                if self.pol:
                    # Method 1 using predefined routines for the pol term
                    P_qq = polarization_product(
                        G_ii, Gd_ii, pre=2.j * pre, rot=U_pq)
                    W_qq = np.dot(self.V_qq, np.dot(P_qq, self.V_qq))
                    exchange_product(
                        G_ii, W_qq, pre=1.j*pre, out=Sigma_ii, rot=U_pq)
                if self.cross:
                    raise NotImplementedError
        return less, great


class Born2SelfEnergy_opt2(Born2SelfEnergy):
    """This 'optimized' version overwrites GetLesserAndGreaterInTime to use
    optimized blas subroutines.
    """
    def GetLesserAndGreaterInTime(self, Gl, Gg):
        """Determine lesser and greater self-energy in time domain
        from lesser and greater Green functions in time domain.
        """
        energies = self.GetFullEnergyList()
        de = energies[1] - energies[0]
        U_pq = self.U_pq
        V_qp = self.VU_qp
        if U_pq is None:
            U_pq = np.identity(len(V_qp), complex)

        Nt, Ni = Gl.shape[:2]
        Np = Ni**2
        Nq = U_pq.shape[1]
        
        # Prefactor due to fft
        pre = de / (2 * np.pi)

        # The self-energies
        less = np.zeros([Nt, Ni, Ni], complex)
        great = np.zeros([Nt, Ni, Ni], complex)

        # Temporary work arrays
        T1_iiq = np.zeros([Ni, Ni, Nq], complex)
        T2_iiq = np.zeros([Ni, Ni, Nq], complex)
        if self.cross:
            T3_iii = np.zeros([Ni, Ni, Ni], complex)

        # Different contiguous views into work arrays
        U_iiq = U_pq.reshape(Ni, Ni, Nq)
        V_qii = V_qp.reshape(Nq, Ni, Ni)
        T1_pq = T1_iiq.reshape(Np, Nq)
        T2_pq = T2_iiq.reshape(Np, Nq)
        T1_qq = T1_iiq.reshape(-1)[:Nq**2].reshape(Nq, Nq)
        T2_iqi = T2_iiq.reshape(Ni, Nq, Ni)
        if self.cross:
            T1_qii = T1_iiq.reshape(Nq, Ni, Ni)
            T2_qii = T2_iiq.reshape(Nq, Ni, Ni)

        if world.rank == 0:
            print 'Calculating lesser and greater self-energy'

        for t in range(Nt):
            for G_ii, Gd_ii, Sigma_ii in [(Gl[t], Gg[t].conj(), less[t]),
                                          (Gg[t], Gl[t].conj(), great[t])]:
                # The polarization and cross diagrams:
                # S<_ij = sum_klpqrs G<_kl G<_pr G>*_qs V_ik,pq (
                #                         -2 V_rs,jl    # polarization diagram
                #                         +1 V_ls,jr)   # cross diagram
                #
                # Greater follows by replacing < with > and vice-versa.
                #
                # Including rotation from optimized representation, i.e.
                # using V_qq, and V_q,ij = sum_q' V_qq' Ud_q',ij, this becomes
                #
                # S<_ij = sum_klpqrsmn G<_kl G<_pr G>*_qs U_ik,m V_m,pq (
                #                    -2 U_rs,n V_n,jl    # polarization diagram
                #                    +1 U_ls,n V_n,jr)   # cross diagram
                                    # T1_rqn = sum_s Gd_qs U_rs,n

                # Store in T2_mrs = sum_p G_pr (sum_q V_mpq Gd_qs)
                gemm(1.0, Gd_ii, V_qp.reshape(-1, Ni), 0.0, T1.reshape(-1, Ni))
                for t1, t2 in zip(T1.reshape(Nq, Ni, Ni),
                                  T2.reshape(Nq, Ni, Ni)):
                    gemm(1.0, t1, G_ii.T.copy(), 0.0, t2)

                # Store in T1_ilm = sum_k G_kl U_ikm
                for t1, u in zip(T1.reshape(Ni, Ni, Nq),
                                 U_pq.reshape(Ni, Ni, Nq)):
                    gemm(1.0, u, G_ii.T.copy(), 0.0, t1)

                # Now what is left is:
                # S_ij = sum_lrsmn T2_mrs T1_ilm (-2 U_rsn Vnjl + U_lsn V_njr)
                if self.pol:
                    T3 = np.dot(T2.reshape(Nq, -1), U_pq) #_mn
                    gemm(1.0, V_qp, T3, 0.0, T4.reshape(Nq, -1)) # T2_mjl
                    gemm(-2.0 * pre**2,
                         T4.reshape(Nq, Ni, Ni).transpose(
                        2, 0, 1).reshape(-1, Ni).copy(),
                         T1.reshape(Ni, -1), 1.0, Sigma_ii)
                if self.cross:
                    for l in range(Ni):
                        T3 = np.dot(T1.reshape(Ni, Ni, Nq)[:, l, :],
                                    T2.reshape(Nq, -1)) # _i,rs
                        T3.reshape(-1, N1)[:] = np.dot(
                            T3.reshape(-1, Ni),
                            U_pq.reshape(Ni, Ni, Nq)[l]) #_irn
                        gemm(+1.0 * pre**2,
                             V_qp.reshape(Nq, Ni, Ni).transpose(
                            2, 0, 1).reshape(-1, Ni).copy(),
                             T3.reshape(Ni, -1), 1.0, Sigma_ii)
                       
        return less, great







 
##     def LessGreatTMP(self):
##         Ne = len(self.GetFullEnergyList())
##         nuindices = self.GetNonIntGreenFunction().GetNUGridIndices()
##         energies = self.GetFullEnergyList()
##         de = energies[1] - energies[0]
##         U_pq = self.U_pq
##         Ud_qp = U_pq.T.conj()

##         Gl = self.GetNonIntGreenFunction().GetLesserList()
##         Gg = self.GetNonIntGreenFunction().GetGreaterList()

##         # FFT to time
##         Gl = SliceAlongFrequency(Gl, self.p2b)
##         Gg = SliceAlongFrequency(Gg, self.p2b)
##         Gl = np.fft.fft(LinearInterpolation(Gl, nuindices, Ne), axis=0)
##         Gg = np.fft.fft(LinearInterpolation(Gg, nuindices, Ne), axis=0)
##         Gl = SliceAlongOrbitals(Gl, self.p2b)
##         Gg = SliceAlongOrbitals(Gg, self.p2b)

##         Nt, Ni = Gl.shape[:2]
##         #Np = Ni**2
##         Nq = U_pq.shape[1]
##         pre = 1.j * de / (2 * np.pi)

##         # Make polarization
##         chil = np.zeros([Nt, Nq, Nq], complex)
##         chig = np.zeros([Nt, Nq, Nq], complex)
##         for t in range(Nt):
##             for G_ii, Gd_ii, chi_qq in [(Gl[t], Gg[t], chil[t]),
##                                         (Gg[t], Gl[t], chig[t])]:
##                 apply_kron2(chi_qq, 2 * pre, Ud_qp, G_ii, Gd_ii.conj(), U_pq)

##         # Make screended interaction
##         Wl = np.zeros([Nt, Nq, Nq], complex)
##         Wg = np.zeros([Nt, Nq, Nq], complex)
##         for t in range(Nt):
##             for W_qq, chi_qq in [(Wl[t], chil[t]),
##                                  (Wg[t], chig[t])]:
##                 W_qq[:] = np.dot(self.V_qq, np.dot(chi_qq, self.V_qq))

##         Wl = -Wl.conj()
##         # Make self-energy
##         less = np.zeros([Nt, Ni, Ni], complex)
##         great = np.zeros([Nt, Ni, Ni], complex)
##         for t in range(Nt):
##             for G_ii, Sigma_ii, W_qq in [(Gl[t], less[t], Wl[t]),
##                                          (Gg[t], great[t], Wg[t])]:
##                 exchange_product3(Sigma_ii, pre, G_ii, U_pq, W_qq, Ud_qp)

##         print 'zero index', self.zero_index
##         less = np.fft.ifft(less, axis=0)
##         great = np.fft.ifft(great, axis=0)
##         Wl = np.fft.ifft(Wl, axis=0)
##         Wg = np.fft.ifft(Wg, axis=0)
## ##         less = TranslateAlongAxis0(np.fft.ifft(less, axis=0), -self.zero_index)
## ##         great = TranslateAlongAxis0(np.fft.ifft(great, axis=0), -self.zero_index)
## ##         Wl = TranslateAlongAxis0(np.fft.ifft(Wl, axis=0), -self.zero_index)
## ##         Wg = TranslateAlongAxis0(np.fft.ifft(Wg, axis=0), -self.zero_index)

##         return less, great, Wl, Wg
