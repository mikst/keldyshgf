from __future__ import print_function

import time
import numpy as np
from gpaw.mpi import world
from kgf.FourierTransform import fourierintegral, GetFourierData
from kgf.Tools import LinearInterpolation, TranslateAlongAxis0, rotate, gemm,\
     exchange_product, polarization_product
from kgf.selfenergies import NonEqIntSelfEnergy
from kgf.Parallel import \
     AssignOrbitals2Procs, SliceAlongFrequency, SliceAlongOrbitals


def IncreaseToPowerOfTwo(N):
    count = 0
    while N != 1:
        if N % 2 == 1:
            N += 1
        N /= 2
        count += 1
    return 2**count


class NonEqGWSelfEnergy(NonEqIntSelfEnergy):
    """NOTE: How to treat the factor 1/2 in front of the Hamiltonian?

    The interacting Hamiltonian has the form:
    H=(1/2)\sum_{is,js'} V_{is,js'} c^d_{is} c^d_{js'} c_{js'} c_{is}
    V has dimension 2Nx2N where N is the number of orbitals in the central
    region, and the factor two is to include spin.
    V_{is,js'} = \int \int drdr' [|\phi_i(r)|^2*|\phi_j(r')|^2]/|r-r'|
                 -\delta_{ss'}*\int \int drdr' [
                           \phi_i^*(r)\phi_j^*(r')\phi_j(r)\phi_i(r')]/|r-r'|
    """
    def __init__(self, g0, V_qq, U_pq_seq=None,
                 oversample=10, polarization_only=False, translate=True):
        # NOTE: The energygrid of g0, should contain 0.0, so that translations
        # can be performed on the grid.
        #
        # Inut parameters:
        #
        # g0          A noninteracting GF
        # V_qq        Is the truncated interaction in the basis of
        #             optimized pair densities
        # U_pq_seq    is the rotation matrix that diagonalises S_pp
        #             multiplied by its eigenvalues: 
        #             U_pq_seq = sqrt(e_q)*U_pq,
        #             where dagger(U_pq) . S_pp . U_pq = e_q
        # oversample  The length of the extended grid will be approximately
        #             oversample * len(g0 energygrid). The actual length used
        #             is increased to be a power of two.
        # polarization_only
        #             Boolean making it possible to include only the second
        #             order polarization bubble, instead of the infinite order.
        #             This is useful for debugging, using the Born2SelfEnergy
        #             with cross=False should be more efficient.
        #
        #                _\|          _/
        #                  \ k      j /|
        #  Interaction:     xxxxxxxxxx  =     V_ijkl      =      V_ki,jl 
        #                 |/_i      l \_    (four index)   (pair density repr.)
        #                 /           |\
        #
        #   V_ki,jl = \int \int dr dr' n_ki(r)^* n_jl(r') / |r-r'|
        #
        #   n_ki(r) = phi_k(r)^* phi_i(r)
        #
        # Inverted: V_ij,kl = V_jkil
        #
        # WARNING: although all equations are implemented according to the
        # above; the input V should be complex conjugated relative to this
        # definition in all of the KGF code!
        #
        NonEqIntSelfEnergy.__init__(self, g0, 'GW', correlated=True)

        assert V_qq.dtype == complex
        self.V_qq = V_qq
        if U_pq_seq is not None:
            assert U_pq_seq.dtype == complex
            self.U_pq = U_pq_seq
            self.Ud_qp = np.ascontiguousarray(U_pq_seq.T.conj())
        else:
            self.U_pq = self.Ud_qp = None

        # Assign basis function orbitals to the different processes
        self.p2b = AssignOrbitals2Procs(g0.GetMatrixDimension()[0])

        # Assign (optimized) pair densities to the different processes 
        self.p2p = AssignOrbitals2Procs(len(V_qq))

        # Analyse energy grid
        energies = self.GetFullEnergyList()
        self.zero_index = abs(energies).argmin() # Do not use np.searchsorted
                                                 # as zero might be a small neg
        if abs(energies[self.zero_index]) > 1e-7:
            raise IndexError('Energygrid must contain 0.0')
        if not translate:
            # set translate flag to False to avoid
            # the translations back and forth
            # XXX should we translate or not?
            self.zero_index = 0
          
        self.oversample = oversample
        self.fourierdata = GetFourierData(len(self.GetExtendedGrid()))
        self.pol_only = polarization_only
        
    ########################################
    # Start Set/Get and Unregister methods #
    ########################################

    def GetV(self):
        return self.V_qq

    def GetGreaterChi(self):
        if not hasattr(self,'chinotgreat'):
            self.UpdateGreaterAndLesserChi()
        return self.chinotgreat

    def SetGreaterChi(self,chinotgreat):
        self.chinotgreat=chinotgreat

    def UnregisterGreaterChi(self):
        del self.chinotgreat

    def GetLesserChi(self):
        if not hasattr(self,'chinotless'):
            self.UpdateGreaterAndLesserChi()
        return self.chinotless

    def SetLesserChi(self,chinotless):
        self.chinotless=chinotless

    def UnregisterLesserChi(self):
        del self.chinotless

    def GetRetardedW(self):
        if not hasattr(self,'retW'):
            self.UpdateRetardedW()
        return self.retW

    def SetRetardedW(self,retW):
        self.retW=retW

    def UnregisterRetardedW(self):
        del self.retW

    def GetAdvancedW(self):
        return np.conj(self.GetRetardedW().swapaxes(1, 2))

    def GetGreaterW(self):
        if not hasattr(self,'greatW'):
            self.UpdateGreaterW()
        return self.greatW

    def SetGreaterW(self,greatW):
        self.greatW=greatW

    def UnregisterGreaterW(self):
        del self.greatW

    def GetLesserGW(self):
        if not hasattr(self,'lessGW'):
            self.UpdateRetardedAndLesserGW()
        return self.lessGW

    def SetLesserGW(self,lessGW):
        self.lessGW=lessGW

    def UnregisterLesserGW(self):
        del self.lessGW

    def GetRetardedGW(self):
        if not hasattr(self,'retGW'):
            self.UpdateRetardedAndLesserGW()
        return self.retGW

    def SetRetardedGW(self,retGW):
        self.retGW=retGW

    def UnregisterRetardedGW(self):
        del self.retGW

    def Update(self):
        self.Unregister()
        self.UpdateRetardedAndLesserGW()        

    def Unregister(self):
        if hasattr(self,'chinotgreat'):
            self.UnregisterGreaterChi()
        if hasattr(self,'chinotless'):
            self.UnregisterLesserChi()
        if hasattr(self,'retW'):
            self.UnregisterRetardedW()
        if hasattr(self,'greatW'):
            self.UnregisterGreaterW()
        if hasattr(self,'retGW'):
            self.UnregisterRetardedGW()
        if hasattr(self,'lessGW'):
            self.UnregisterLesserGW()

    #####################################
    # End of Get/Set/Unregister methods #
    #####################################

    def GetTempFactor(self):
        # This factor determines the size of the thermo-window
        return 10

    def GetEnergyList(self):
        return self.GetNonIntGreenFunction().GetEnergyList()

    def GetFullEnergyList(self):
        return self.GetNonIntGreenFunction().GetFullEnergyList()

    def GetExtendedGrid(self):
        if not hasattr(self, 'extgrid'):
            fullenergies = self.GetFullEnergyList()
            Ne_full = len(fullenergies)

            # Make sure that grid lenght is a power of 2
            Ne_ext = IncreaseToPowerOfTwo(self.oversample * Ne_full)

            begin_full = fullenergies[0]
            de_full = fullenergies[1] - begin_full
            extgrid = np.arange(Ne_ext) * de_full + begin_full

            self.extgrid = np.around(extgrid, 8)
        return self.extgrid

    def GetCurrentBounds(self):
        if not hasattr(self, 'currentbounds'):
            self.EvaluateCurrentBounds()
        return self.currentbounds

    def GetCurrentGrid(self):
        b0 = self.GetCurrentBounds()[0]
        b1 = self.GetCurrentBounds()[1]
        return self.GetNonIntGreenFunction().GetNUEnergyList()[b0:b1]

    def SetCurrentBounds(self, currentbounds):
        self.currentbounds = currentbounds

    def EvaluateCurrentBounds(self):
        """ Grid on which the retarded GW self energy is obtained from GW< - GW>
         by direct convolution in energy space. This is more accurate but time-consuming.
         Since it is only important for the current calculation this is only done at 
         a limited grid.

        NOTE: The following is only strictly true at T=0!!
              For finite T we add 10*T to the interval (emperically chosen). 
            To determine the energies for which the integrand in the current integral is non-zero,
            we use the following:
            1) G_l=G_r Sigma_l G_a (Keldysh for small infinitesimal)
            2) Sigma_l+Sigma_r-Sigma_a=Sigma_g
        UPPER LIMIT: In the GW approximation Sigma_l vanishes above 'upper_bound' defined below. 
        And since Sigma_L_l and Sigma_R_l also are zero above this value, it follows that G_l
        is also zero above 'upper_bound'. From the current integral it is then easy to see that
        this defines the upper limit.
        LOWER LIMIT: For energies below min(mu_L,mu_R) we can isolate G_g in the current formula
        using 2) above. Now, in the GWA Sigma_g vanishes below the 'lower_bound' defined below.
        Therefore G_g vanishes below this value, and this therefore determines the lower limit in 
        the current integration."""

        mu_l=self.GetNonIntGreenFunction().GetLeftChemicalPotential()
        mu_r=self.GetNonIntGreenFunction().GetRightChemicalPotential()
        efermi=self.GetNonIntGreenFunction().GetFermiLevel()
        T=self.GetNonIntGreenFunction().GetTemperature()
        upper_bound = max(mu_l + efermi, mu_r + efermi) + (
            abs(mu_l)+ abs(mu_r) + T * self.GetTempFactor())
        lower_bound = min(mu_l + efermi, mu_r + efermi) - (
            abs(mu_l) + abs(mu_r) + T * self.GetTempFactor())
        energies = self.GetNonIntGreenFunction().GetNUEnergyList()
        start = np.searchsorted(energies, lower_bound)
        end = np.searchsorted(energies, upper_bound)
        self.SetCurrentBounds([start, end])

    def RetardedTest(self, f):
        # Note, since we carry out the Hilbert transform in time-space, 
        # this method will in fact test whether the f IN TIME SPACE is a good
        # retarded function. It can thus fail for W^r even though this function
        # is a very good retarded function in energy space.
        # Remove dc component
        f = f - np.sum(f, axis=0) / len(f)
        im=f.imag
        re=f.real
        im2 = HilbertTransform(-re, self.GetEnergyList())
        re2 = HilbertTransform(im, self.GetEnergyList())
        val1=np.sum(abs(re2+1.0j*im2-f),axis=0)
        val2=np.sum(abs(f),axis=0)
        print ("Relative difference:",max((val1/val2).flat))

    def AdvancedTest(self, f):
        # Remove dc component
        f = f - np.sum(f, axis=0) / len(f)
        im=f.imag
        re=f.real
        im2 = HilbertTransform(re, self.GetEnergyList())
        re2 = HilbertTransform(-im, self.GetEnergyList())
        val1=np.sum(abs(re2+1.0j*im2-f),axis=0)
        val2=np.sum(abs(f),axis=0)
        print ("Relative difference:",max((val1/val2).flat))

    def GetRetardedFromLesserAndGreater(self, less, great):
        # Calculates the retarded function from
        #
        # r(t) = theta(t)[ >(t) - <(t) ]
        #
        # less and great are in energy domain, and on the full energy grid
        # ret is returned in energy domain, on the nu energy grid
        nuindices = self.GetNonIntGreenFunction().GetNUGridIndices()
        fullenergies = self.GetFullEnergyList()
        extenergies = self.GetExtendedGrid()
        de_ext = extenergies[1] - extenergies[0]

        Ne_full = len(fullenergies)
        Ne_ext = len(extenergies)
        Ne_nu = len(nuindices)
        Nq = less.shape[1]
        assert len(less) == Ne_full
        
        ret = np.zeros((Ne_nu, Nq), complex)
        for i in range(Nq):
            # Make the difference (> - <)
            ret_i = great[:, i] - less[:, i]

            # Pad by zeros to extended grid, and FFT to time
            ret_i = np.fft.fft(ret_i, n=Ne_ext)

            # multiply by theta(t)
            # after fft, time is ordered such that if n is odd:
            #   index 0 is t=0, index 1:n/2+1 is t>0, and index n/2+1: is t<0
            # if n is even:
            #   index 0 is t=0, index 1:n/2 is t>0, and index n/2: is t<0
            ret_i[Ne_ext // 2:] = 0.0 # this will zero all t<0

            # iFFT back to energy using endpoint corrections
            #ret_i = np.fft.ifft(ret_i)#<- the equivalent without end-point cor
            ret_i = fourierintegral(ret_i, de_ext, sign=1,
                                    data=self.fourierdata) / (de_ext * Ne_ext)

            # Reduce ext grid to full grid and then take nu grid only to store
            ret[:, i] = ret_i[:Ne_full].take(nuindices, axis=0)
        return ret

    def UpdateGreaterAndLesserChi(self):
        if world.rank==0:
            print (time.ctime().split()[-2], "Calculating chi< and chi>")
        energies = self.GetFullEnergyList()
        de = energies[1] - energies[0]
        Gl = self.GetNonIntGreenFunction().GetLesserList()
        Gg = self.GetNonIntGreenFunction().GetGreaterList()
        nuindices = self.GetNonIntGreenFunction().GetNUGridIndices()
        #################################
        # 1. slice along frequency axis #
        ################################# 
        Gl = SliceAlongFrequency(Gl, self.p2b)
        Gg = SliceAlongFrequency(Gg, self.p2b)
        #########################################
        # 2. make Fourier transform to get G(t) #
        #########################################
        Gl = np.fft.fft(LinearInterpolation(Gl,nuindices,len(energies)),axis=0)
        Gg = np.fft.fft(LinearInterpolation(Gg,nuindices,len(energies)),axis=0)
        ###############################
        # 3. slice along orbital axes #
        ###############################
        Gl = SliceAlongOrbitals(Gl, self.p2b)
        Gg = SliceAlongOrbitals(Gg, self.p2b)
        ##########################################
        # 4. create chi functions in time domain #
        ##########################################
        #
        #              ____\___
        #             /    /   \
        #          i /          \ k
        #   xxxxxxxxx            xxxxxxxxxx
        #          j \          / l
        #             \____/___/
        #                  \
        #
        #   In Keldysh space: chi_ij,kl(t,t') = -i G_ik(t,t')G_lj(t',t)
        #
        #   In real time:     chi^<_ij,kl(t,t') = -i G^<_ik(t,t')G^>_lj(t',t)
        #                     chi^>_ij,kl(t,t') = -i G^>_ik(t,t')G^<_lj(t',t)
        #
        #   Use that G<_ij(t) = -G<_ji(-t)^* and similar for G> to get
        #       chil_ijkl(t) = iGl_ik(t) * Gg_jl^*(t)
        #       chig_ijkl(t) = iGg_ik(t) * Gl_jl^*(t) = chil_jilk^*(t)
        #                     
        # kron(A, B).reshape(A.shape + B.shape)[ijkl] = A[i, k] * B[j, l]
        Nt, Ni = Gl.shape[:2]
        Nq = len(self.V_qq)
        chil = np.zeros([Nt, Nq, Nq], complex)
        chig = np.zeros([Nt, Nq, Nq], complex)

        # Prefactor: 2 for spin, j. from definition, and de / 2 pi for fft
        pre = 2.j * de / (2 * np.pi)

        # Temporary work arrays
        work1 = np.zeros(Ni**2 * Nq, dtype=complex)
        work2 = work1.copy()
        for t in range(Nt):
            for G_ii, Gd_ii, chi_qq in [(Gl[t], Gg[t], chil[t]),
                                        (Gg[t], Gl[t], chig[t])]:
                # chi< = pre * Ud (G< otimes G>.conj ) U
                # chi> = pre * Ud (G> otimes G<.conj ) U
                polarization_product(G_ii, Gd_ii.conj(),
                                     pre=pre, out=chi_qq,
                                     rot=self.U_pq, rotd=self.Ud_qp,
                                     work1=work1, work2=work2)
        del Gl, Gg, work1, work2
        ############################
        # 5. slice along time axis #
        ############################
        chil = SliceAlongFrequency(chil, self.p2p)
        chig = SliceAlongFrequency(chig, self.p2p)
        ###################################
        # 6. tranform to frequency domain #
        ###################################
        chil = TranslateAlongAxis0(np.fft.ifft(chil, axis=0), self.zero_index)
        chig = TranslateAlongAxis0(np.fft.ifft(chig, axis=0), self.zero_index)
        self.SetLesserChi(chil)
        self.SetGreaterChi(chig)

    def GetRetardedChi(self):
        # Determine retarded chi from the relation
        #
        #  chir(t) = theta(t)[ chi>(t) - chi<(t) ]
        nuindices = self.GetNonIntGreenFunction().GetNUGridIndices()
        chil = self.GetLesserChi()
        chig = self.GetGreaterChi()
        chi_ret = self.GetRetardedFromLesserAndGreater(chil, chig)
        del chil
        self.UnregisterLesserChi()
        chig = chig.take(nuindices, axis=0).copy()
        chig = SliceAlongOrbitals(chig, self.p2p)
        chi_ret = SliceAlongOrbitals(chi_ret, self.p2p)
        # Store chig on the non-uniform grid
        self.SetGreaterChi(chig)
        return chi_ret              

    def UpdateRetardedW(self):
        #
        #  Screened interaction (optimized repr.)
        #                                              __\___
        #                                             /  /   \    
        #  (q)XXXXXX(q')  =  (q)xxxxx(q')  + (q)xxxxxx        XXXXX(q')
        #                                             \__/___/    
        #                                                \
        #  In energy space:
        #  1: Wr = V  + V * chir * Wr = [I - V * chir]^{-1} * V
        #  2: W> = Wr * chi> * Wa  (Wa =  Wr^dagger)
        #  3: W< = W> - Wr + Wa
        V = self.GetV()
        chir_e = self.GetRetardedChi()

        # Step 1: calculate Wr
        if world.rank == 0:
            print (time.ctime().split()[-2], 'Calculating W^r')
        Wr_e = np.zeros_like(chir_e)
        Nq = len(V)
        for chir, Wr in zip(chir_e, Wr_e):
            Wr[:] = np.identity(Nq)
            if self.pol_only:
                gemm(1.0, chir, V, 1.0, Wr, 'n') # Wr += V * chir
                gemm(0.0, V, Wr, 1.0, chir, 'n') # Wr = (I + V chir) * V
                Wr[:] = chir
            else:
                gemm(-1.0, chir, V, 1.0, Wr, 'n') # Wr -= V * chir
                Wr[:] = np.linalg.solve(Wr, V) # Wr = (I - V chir)^-1 * V
        self.SetRetardedW(Wr_e)

    def UpdateGreaterW(self):
        #  W> = Wr * chi> * Wa  (Wa =  Wr^dagger)
        if world.rank == 0:
            print (time.ctime().split()[-2], 'Calculating W^>')
        Wr_e = self.GetRetardedW()
        chig_e = self.GetGreaterChi()
        work = np.zeros(Wr_e.shape[1:], complex)
        for chig, Wr in zip(chig_e, Wr_e): # loop over energies
            # Do chig <- Wr chig Wr^dag (overwrite Chi> with the values of W>)
            if self.pol_only:
                #chig[:] = np.dot(self.V_qq, np.dot(chig, self.V_qq))
                rotate(chig, self.V_qq, a=1., b=0., out_ii=chig, work_ij=work)
            else:
                rotate(chig, Wr, a=1.0, b=0.0, out_ii=chig, work_ij=work)
        self.SetGreaterW(chig_e)
        self.UnregisterGreaterChi()
        
    def GetLesserW(self):
        # W< = W> - Wr + Wa
        return self.GetGreaterW() - self.GetRetardedW() + self.GetAdvancedW()

    def UpdateRetardedAndLesserGW(self):
        #
        #                    
        #                       
        #  ---\ GW             
        #  \                i           j
        #   |    (t,t') =    XXXXXXXXXXX    
        #  /                k \        /l       
        #  ---/ ij             \__\___/
        #                         /
        #
        # In Keldysh space: GW_ij(t,t') = \sum_kl  W_ik,jl(t,t') . G_kl(t,t')
        #
        # In real time:     GW<_ij(t,t') = \sum_kl W<_ik,jl(t,t') . G<_kl(t,t')
        #                   GW>_ij(t,t') = \sum_kl W>_ik,jl(t,t') . G>_kl(t,t')
        #                    
        # To get W in terms of regular pair densities (p) from W in terms of
        # optimized pair densities (q):
        #
        #       W_p = U_pq . sqrt(e_q) . Wq . sqrt(e_q) . dagger(U_pq)
        #            \______  _______/        \_________  ___________/     
        #                   \/                          \/
        #                    = Us                       = dagger(Us)
        #
        # The problem: We cannot afford to unfold W_q to W_p representation on
        # the full frequency (or time) axis. Instead we calculate the ij
        # subblocks separately:
        # 
        #     W_i.,j. = Us[i,:] . Wq . dagger(Us)[:,j]
        #
        nuindices = self.GetNonIntGreenFunction().GetNUGridIndices()
        energies = self.GetFullEnergyList()
        de = energies[1] - energies[0]
        Ne = len(energies)
        Gg = self.GetNonIntGreenFunction().GetGreaterList()
        Gl = self.GetNonIntGreenFunction().GetLesserList()
        Wg = self.GetGreaterW()
        Wl = self.GetLesserW()
        self.UnregisterRetardedW()
        if world.rank == 0:
            print (time.ctime().split()[-2], "Calculating GW< and GW>"  )
        ##################################
        # 1. slice along frequency axis  #
        ##################################
        Gl = SliceAlongFrequency(Gl, self.p2b)
        Gg = SliceAlongFrequency(Gg, self.p2b)
        Wg = SliceAlongFrequency(Wg, self.p2p)
        Wl = SliceAlongFrequency(Wl, self.p2p)
        # We can perhaps utilize that: W_ij,kl(tau,tau') = W_lk,ji(tau',tau)
        #                         and: W<_ij,kl(t,t') = W>_lk,ji(t',t)
        # so we do not need W<.
        ############################################################
        # 2. make Fourier transform to get G<(t),G>(t), and W<(t)  #
        ############################################################
        Gl = np.fft.fft(LinearInterpolation(Gl, nuindices, Ne), axis=0)
        Gg = np.fft.fft(LinearInterpolation(Gg, nuindices, Ne), axis=0)
        Wg = np.fft.fft(LinearInterpolation(Wg, nuindices, Ne), axis=0)
        Wl = np.fft.fft(LinearInterpolation(Wl, nuindices, Ne), axis=0)
        ###############################
        # 3. slice along orbital axes #
        ###############################
        Gl = SliceAlongOrbitals(Gl, self.p2b)
        Gg = SliceAlongOrbitals(Gg, self.p2b)
        Wl = SliceAlongOrbitals(Wl, self.p2p)
        Wg = SliceAlongOrbitals(Wg, self.p2p)
        ######################################
        # 4. make GW< and GW> in time domain #
        ######################################
        gwl = np.zeros(Gl.shape, complex)
        gwg = np.zeros(Gl.shape, complex)
        Nq = len(self.V_qq)
        Nt, Ni = Gl.shape[:2]
        pre = 1.j * de / (2 * np.pi) # factor j. from def, de / 2 pi for fft

        work1 = np.zeros(Ni**2 * Nq, dtype=complex)
        work2 = work1.copy()
        for t in range(Nt): 
            for G_ii, W_qq, Sigma_ii in [(Gl[t], Wl[t], gwl[t]),
                                         (Gg[t], Wg[t], gwg[t])]:
                # Sigma_ij = i sum_klqq' G_kl U_ik,q W_qq' Ud_q',jl
                exchange_product(G_ii, W_qq,
                                 pre=pre, out=Sigma_ii, rot=self.U_pq,
                                 work1=work1, work2=work2)
        del Wg, Wl, work1, work2

        self.UnregisterGreaterW()
        ############################
        # 5. slice along time axis #
        ############################
        gwl = SliceAlongFrequency(gwl, self.p2b)
        gwg = SliceAlongFrequency(gwg, self.p2b)
        ###################################
        # 6. tranform to frequency domain #
        ###################################
        gwl = TranslateAlongAxis0(np.fft.ifft(gwl, axis=0), -self.zero_index)
        gwg = TranslateAlongAxis0(np.fft.ifft(gwg, axis=0), -self.zero_index)
        #####################################
        # 7. calculate GWr from GW< and GW> #
        #####################################
        if world.rank == 0:
            print (time.ctime().split()[-2], "Calculating GW^r")
        gwr = self.GetRetardedFromLesserAndGreater(gwl, gwg)
        del gwg
        gwl = gwl.take(nuindices, axis=0).copy()
        ###############################
        # 8. slice along orbital axes #
        ###############################
        gwr = SliceAlongOrbitals(gwr, self.p2b)
        gwl = SliceAlongOrbitals(gwl, self.p2b)
        self.SetRetardedGW(gwr)
        self.SetLesserGW(gwl)

    def GetRetarded(self, index):
        return self.GetRetardedGW()[index]

    def GetLesser(self, index):      
        return self.GetLesserGW()[index]
