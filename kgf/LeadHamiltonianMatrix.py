from __future__ import print_function

import numpy as np
from scipy.io.netcdf import netcdf_file as NetCDFFile

#from ase.io.pupynere import NetCDFFile

class LeadHamiltonianMatrix:
    """ Given the bulkhamiltonian this class returns the hamiltonian and
        overlapmatrix for the two leads."""
    def __init__(self, principallayer,
                 hamiltonianmatrix=None, overlapmatrix=None, filename=None):
        if hamiltonianmatrix is not None:
            self.SetHamiltonianMatrix(hamiltonianmatrix)
            self.SetOverlapMatrix(overlapmatrix)
        elif filename is not None:
            self.ReadHSMatrixFromNetCDFFile(filename)
        else:
            raise (RuntimeError, 'Must specify either Hamiltonian or filename')
        self.SetPrincipalLayerLength(principallayer)
#       self.NormalizeBasis()

    def NormalizeBasis(self):
        s = self.GetOverlapMatrix()
        d = np.reshape(1.0 / np.sqrt(abs(s.diagonal())), [1, s.shape[0]])
        normmatrix = np.dot(d.T, d)
        self.SetOverlapMatrix(s * normmatrix)
        self.SetHamiltonianMatrix(normmatrix * self.GetHamiltonianMatrix())
        
    def SetHamiltonianMatrix(self,hamiltonian):
        self.hmatrix=hamiltonian

    def GetHamiltonianMatrix(self):
        return self.hmatrix
        
    def SetOverlapMatrix(self,overlapmatrix):
        """If not specified the overlapmatrix is given by the
           identitymatrix."""
        if overlapmatrix is None:
            overlapmatrix = np.identity(len(self.hmatrix), complex)
        self.omatrix=overlapmatrix
            
    def GetOverlapMatrix(self):
        return self.omatrix

    def SetPrincipalLayerLength(self, principallayer):
        self.principallayer = principallayer

    def GetPrincipalLayerLength(self):
        return self.principallayer

    def ReadHSMatrixFromNetCDFFile(self,filename):
        """ As an alternative it is  possible to read in the leadhamiltonian
            from an netCDF file given as the output from an dacapocalculation.
            In bulk the Hamiltonen is given as;
                {h_l  t_l^d} , where d=dagger
            H_l={t_l    h_l}"""

        file=NetCDFFile(filename,'r')
        HSMatrix = file.variables['HSMatrix'][:]
        hmat = np.zeros([HSMatrix.shape[1],HSMatrix.shape[1]],complex)
        smat = np.zeros([HSMatrix.shape[1],HSMatrix.shape[1]],complex)
        hmat.real=HSMatrix[0,:,:,0]
        hmat.imag=HSMatrix[0,:,:,1]
        smat.real=HSMatrix[1,:,:,0]
        smat.imag=HSMatrix[1,:,:,1]
        self.SetHamiltonianMatrix(hmat)
        self.SetOverlapMatrix(smat)
        
    def WriteHSMatrixToNetCDFFile(self,filename):
        hmat=self.GetHamiltonianMatrix()
        smat=self.GetOverlapMatrix()

        file=NetCDFFile(filename,'w')
        file.createDimension("Complex",2)
        file.createDimension("NObjects",2)
        file.createDimension("NBasisFunctions",hmat.shape[0])

        HSMatrix=file.createVariable("HSMatrix", 'd',
                                     ("NObjects", "NBasisFunctions",
                                      "NBasisFunctions", "Complex"))
        HSMatrix[0,:,:,0]=hmat.real
        HSMatrix[0,:,:,1]=hmat.imag
        HSMatrix[1,:,:,0]=smat.real
        HSMatrix[1,:,:,1]=smat.imag
        file.sync()
        file.close()
    
    def SetH_l(self,Hl):
        self.hleftlead=Hl

    def GetH_l(self):
        prinlength=self.GetPrincipalLayerLength()
        return self.GetHamiltonianMatrix()[0:prinlength,0:prinlength]

    def GetS_l(self):
        prinlength=self.GetPrincipalLayerLength()
        return self.GetOverlapMatrix()[0:prinlength,0:prinlength]

    def GetT_l(self):
        pl = self.GetPrincipalLayerLength()
        return self.GetHamiltonianMatrix()[pl:2 * pl, 0:pl]

    def GetInteractionS_l(self):
        prinlength=self.GetPrincipalLayerLength()
        return self.GetOverlapMatrix()[prinlength:2*prinlength,0:prinlength]
    
    def GetH_r(self):
        prinlength=self.GetPrincipalLayerLength()
        return self.GetHamiltonianMatrix()[0:prinlength,0:prinlength]

    def GetS_r(self):
        prinlength=self.GetPrincipalLayerLength()
        return self.GetOverlapMatrix()[0:prinlength,0:prinlength]

    def GetT_r(self):
        pl = self.GetPrincipalLayerLength()
        return self.GetHamiltonianMatrix()[0:pl, pl:2 * pl]

    def GetInteractionS_r(self):
        prinlength=self.GetPrincipalLayerLength()
        return self.GetOverlapMatrix()[0:prinlength,prinlength:2*prinlength]
