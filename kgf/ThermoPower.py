import numpy as np
from ase.units import kB

def fd(energies, kbT=300*kB):
    energies2 = np.clip(energies / kbT, -50.0, 50.0)
    return 1.0 / (np.exp(energies2) + 1.0)

def fdp(energies, kbT=300*kB):
    energies2 = np.clip(energies / kbT, -50, 50.0) 
    return -1.0 / kbT * np.exp(energies2) / ((np.exp(energies2)+1)**2.0)

def get_IV2(energies, transmission, bmax, kBT=300*kB, symm=False):
    de = energies[1] - energies[0]
    biases = np.arange(0.0, bmax, 2*de)
    currents = []
    for bias in biases:
        mul =  bias*0.5
        mur = -bias*0.5
        fl = fd(energies - mul,kBT)
        fr = fd(energies - mur,kBT)
        I = np.sum(transmission * (fl - fr)) * de
        currents.append(I)

    currents = np.asarray(currents)
    return currents, biases

def get_IV(energies, transmission, bmax, kBT=300*kB, symm=False):
    de = energies[1] - energies[0]
    biases = np.arange(5*0.5*de, bmax, 5*de)
    currents = []
    for bias in biases:
        mul =  bias*0.5
        mur = -bias*0.5
        fl = fd(energies - mul,kBT)
        fr = fd(energies - mur,kBT)
        I = np.sum(transmission * (fl - fr)) * de
        currents.append(I)

    currents = np.asarray(currents)
    if symm:
        biasesml = (-biases).tolist()
        biasesl = biases.tolist()
        biasesml.reverse()
        currentsml = (-currents).tolist()
        currentsl = currents.tolist()
        currentsml.reverse()
        currents = np.asarray(currentsml+currentsl)
        biases = np.asarray(biasesml+biasesl)

    return currents, biases

def get_dIdV(energies, transmission, bmax, kBT, symm=False):
    currents, biases = get_IV(energies, transmission, bmax, kBT, symm)
    didv = np.diff(currents)/np.diff(biases)
    db = biases[1] - biases[0]
    return didv, biases[1:] - db * 0.5

def get_G(energies, transmission, kbT):
    # units of G0
    L0_e = -fdp(energies, kbT) * transmission
    return np.trapz(L0_e, energies)

def get_L(energies, transmission, kbT):
    # units of G0 * kB/e
    L1_e = -fdp(energies, kbT) * energies / kbT * transmission
    return np.trapz(L1_e, energies)
    
def get_S(energies, transmission, kbT):
    # units of kB/e (kB/e = 8.61 10^-5 V/K)
    G = get_G(energies, transmission, kbT)
    L = get_L(energies, transmission, kbT)
    S = -L/G
    return S

def get_S_e(energies, transmission, kbT, Efs):
    # units of kB/e
    S_e = np.asarray([get_S(energies-Ef, transmission, kbT) for Ef in Efs])
    return S_e

def get_S_smooth(energies, transmission, kbT):
    # units of kB/e XXX verify XXX
    i = abs(energies).argmin() 
    t = transmission[i]
    dt_de1 = (transmission[i+1]-transmission[i])/(energies[1]-energies[0])
    dt_de2 = -(transmission[i-1]-transmission[i])/(energies[1]-energies[0])
    dt_de = (dt_de1 + dt_de2)*0.5
    return - kbT * np.pi**2./3. * dt_de / t

def get_G_e(energies, transmission, kbT, Efs):
    return np.asarray([get_G(energies-Ef, transmission, kbT) for Ef in Efs])

def get_Powerf_e(energies, transmission, kbT, Efs):
    # units of kb^2/h (= 2.877e-13 (J/s)/K^2)
    S_e = get_S_e(energies, transmission, kbT, Efs)
    G_e = get_G_e(energies, transmission, kbT, Efs)
    PF_e = S_e**2 * G_e
    return PF_e

def get_S_smooth_e(energies, transmission, kbT, Efs):
    S_e = np.asarray(
          [get_S_smooth(energies-Ef, transmission, kbT) for Ef in Efs])
    return S_e

if __name__ == '__main__':
    import pylab as pl
    energies = np.arange(-3,2,0.00001)
    fermi = fd(energies)
    fermip = np.diff(fermi)/np.diff(energies)
    fermip2 = fdp(energies)
    gamma = 0.14
    E0 = 0.5
    t = gamma**2 / ((energies-E0)**2 + gamma**2)
    G = get_G(energies, t, 0.002)
    S = get_S(energies, t, 0.002)
    print 'S=%.3f muV/K' % (1.0e6 * S * kB)
    print 'S=%.3f kB/e' %  S 
    print get_S_smooth(energies, t, 0.002)
