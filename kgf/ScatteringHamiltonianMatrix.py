from __future__ import print_function

import numpy as np
from kgf.Tools import RotateMatrix, PermuteMatrix
from scipy.io.netcdf import netcdf_file as NetCDFFile
#from ase.io.pupynere import NetCDFFile


class ScatteringHamiltonianMatrix:
    """ Given the general Hamiltonian this class contains methods to slice out
        the relevant parts."""
    
    def __init__(self, leftprincipallayer, rightprincipallayer,
                 hamiltonianmatrix=None, overlapmatrix=None, filename=None):
        if hamiltonianmatrix is not None:
            self.SetHamiltonianMatrix(hamiltonianmatrix)
        if overlapmatrix is not None:
            self.SetOverlapMatrix(overlapmatrix)
        if filename is not None:
            self.ReadHSMatrixFromNetCDFFile(filename)
        self.SetLeftPrincipalLayerLength(leftprincipallayer)
        self.SetRightPrincipalLayerLength(rightprincipallayer)

    def NormalizeBasis(self,h=True,vxc=True):
        s=self.GetOverlapMatrix()
        d=np.reshape(1.0/np.sqrt(abs(np.diagonal(s))),[1,s.shape[0]])
        normmatrix=np.dot(np.transpose(d),d)
        self.SetOverlapMatrix(s*normmatrix)
        if h:
            self.SetHamiltonianMatrix(normmatrix*self.GetHamiltonianMatrix())
        if hasattr(self,'xc') and vxc:
            self.SetXCMatrix(normmatrix*self.GetXCMatrix())     
        self.SetNormalized(1)

    def SetNormalized(self,val):
        self.norm=val

    def IsNormalized(self):     
        if not hasattr(self,'norm'):
            return 0
        else:
            return self.norm

    def GetRotationMatrix(self):
        h=self.GetH_s()
        s=self.GetS_s()
        val,vec=np.linalg.eigenvectors(np.dot(inverse(s),h))
        indices=np.argsort(val.real)
        val=np.take(val.real,indices)
        vec=Numeric.take(vec,indices,axis=1)
        U=vec
        # Normalize eigenvectors
        T=RotateMatrix(s,U)
        U=U/np.sqrt(np.diagonal(T))
        return U

    def PermuteHamiltonian(self,indices):
        print ("** Both original and internal Hamiltonians must be rotated **")
        self.SetHamiltonianMatrix(PermuteMatrix(self.GetHamiltonianMatrix(),
                                                indices))
        self.SetOverlapMatrix(PermuteMatrix(self.GetOverlapMatrix(), indices))
        if hasattr(self,'hartree'):
            self.SetHartreeMatrix(PermuteMatrix(self.GetHartreeMatrix(),
                                                indices))
        if hasattr(self,'xc'):
            self.SetXCMatrix(PermuteMatrix(self.GetXCMatrix(), indices))

    def SetHamiltonianMatrix(self,hamiltonian):
        self.hmatrix=hamiltonian

    def GetHamiltonianMatrix(self):
        return self.hmatrix
        
    def SetOverlapMatrix(self,overlapmatrix):
        """If not specified the overlapmatrix is chosen to be the
           identitymatrix."""
        self.omatrix=overlapmatrix
           
    def GetOverlapMatrix(self):
        """If not specified the overlapmatrix is chosen to be the
           identitymatrix."""
        if not hasattr(self,'omatrix'):
            return np.identity(self.GetHamiltonianMatrix().shape[0],complex)
        else:
            return self.omatrix

    def GetEigenValues(self, start=0, end=None):
        """Return eigenvalues of scattering region.

        Subspace can be restricted by setting start and end orbital index
        values.
        """
        H = self.GetH_s()[start:end, start:end]        
        if not hasattr(self,'omatrix'):
            e = np.linalg.eigvalsh(H)
        else:
            S = self.GetS_s()[start:end, start:end]
            e =  np.linalg.eigvals(np.linalg.solve(S, H))
        return np.sort(e)

    def SetHartreeMatrix(self,hartree):
        self.hartree=hartree
        if self.IsNormalized():
            print ("#####################################################################################")
            print ("#### WARNING: Basis functions have previously been normalized.                  #####")
            print ("####          You should normalize only AFTER the Hartree matrix has been read. #####")
            print ("#####################################################################################")

    def GetHartreeMatrix(self):
        return self.hartree

    def SetXCMatrix(self,xc):
        self.xc=xc
        if self.IsNormalized():
            print ("################################################################################")
            print ("#### WARNING: Basis functions have previously been normalized.             #####")
            print ("####          You should normalize only AFTER the XC matrix has been read. #####")
            print ("################################################################################")

    def GetXCMatrix(self):
        return self.xc

    def SetLeftPrincipalLayerLength(self,principallayer):
        self.leftprincipallayer=principallayer

    def GetLeftPrincipalLayerLength(self):
        if not hasattr(self,'leftprincipallayer'):
            print ("Please specify the length of a left principallayer.")
        else:
            return self.leftprincipallayer

    def SetRightPrincipalLayerLength(self,principallayer):
        self.rightprincipallayer=principallayer

    def GetRightPrincipalLayerLength(self):
        if not hasattr(self,'rightprincipallayer'):
            print ("Please specify the length of a right principallayer.")
        else:
            return self.rightprincipallayer

    def ReadHSMatrixFromNetCDFFile(self,filename):
        """ As an alternative it is possible to specify the
         Scatteringhamiltonian directly from the netCDF file given by the
         code. Here the Hamiltonen is found from the dacapocalculation
         and the following calculation of the Wannierfunctions -
         this matrix is called the HSMatrix.
         In the scatteringarea the Hamiltonen is given as;
         {H_l  t_l^d    0}
         {t_l   H_s   t_r}
         {0    t_r^d  H_r} , where d=dagger"""
        file=NetCDFFile(filename,'r')
        HSMatrix=file.variables['HSMatrix'][:]
        hmat=np.zeros([HSMatrix.shape[1],HSMatrix.shape[1]],complex)
        smat=np.zeros([HSMatrix.shape[1],HSMatrix.shape[1]],complex)
        hmat.real=HSMatrix[0,:,:,0]   # The two outermost parameters tells
        hmat.imag=HSMatrix[0,:,:,1]   # whether it is the Hamiltonen, 0,
        smat.real=HSMatrix[1,:,:,0]   # or the overlapsmatrix,
        smat.imag=HSMatrix[1,:,:,1]   # 1, and whether the real, 0, 
        self.SetHamiltonianMatrix(hmat)  
        self.SetOverlapMatrix(smat)
        if HSMatrix.shape[0]==4:
            hartree=np.zeros([HSMatrix.shape[1],HSMatrix.shape[1]],complex)
            xc=np.zeros([HSMatrix.shape[1],HSMatrix.shape[1]],complex)
            hartree.real=HSMatrix[2,:,:,0]   
            hartree.imag=HSMatrix[2,:,:,1]   
            xc.real=HSMatrix[3,:,:,0]   
            xc.imag=HSMatrix[3,:,:,1] 
            self.SetHartreeMatrix(hartree)  # or imaginaere part, 1, is given.
            self.SetXCMatrix(xc)

    def WriteHSMatrixToNetCDFFile(self,filename):
        hmat=self.GetHamiltonianMatrix()
        smat=self.GetOverlapMatrix()

        file=NetCDFFile(filename,'w')
        file.createDimension("Complex",2)
        file.createDimension("NObjects",2)
        file.createDimension("NBasisFunctions",hmat.shape[0])

        HSMatrix=file.createVariable("HSMatrix",'d',("NObjects","NBasisFunctions","NBasisFunctions","Complex"))
        HSMatrix[0,:,:,0]=hmat.real
        HSMatrix[0,:,:,1]=hmat.imag
        HSMatrix[1,:,:,0]=smat.real
        HSMatrix[1,:,:,1]=smat.imag
        file.sync()
        file.close()


    def ReduceCoupling(self,factor):
        leftprinlength=self.GetLeftPrincipalLayerLength()
        rightprinlength=self.GetRightPrincipalLayerLength()
        length=self.GetHamiltonianMatrix().shape[0]
        self.GetHamiltonianMatrix()[leftprinlength:length-rightprinlength,0:leftprinlength]=factor*self.GetT_l()        
        self.GetHamiltonianMatrix()[leftprinlength:length-rightprinlength,length-rightprinlength:length]=factor*self.GetT_r()   
        self.GetOverlapMatrix()[leftprinlength:length-rightprinlength,0:leftprinlength]=factor*self.GetInteractionS_l()
        self.GetOverlapMatrix()[leftprinlength:length-rightprinlength,length-rightprinlength:length]=factor*self.GetInteractionS_r()

        
    # Methods with which it is possible to define and call the Hamiltonian
    # for the scatteringarea.    
    def SetH_s(self,Hs):
        H=self.GetHamiltonianMatrix()
        leftprinlength=self.GetLeftPrincipalLayerLength()
        rightprinlength=self.GetRightPrincipalLayerLength()
        length=H.shape[0]
        H[leftprinlength:length-rightprinlength,leftprinlength:length-rightprinlength]=Hs
        self.SetHamiltonianMatrix(H)

    def SetS_s(self,Ss):
        S=self.GetOverlapMatrix()
        leftprinlength=self.GetLeftPrincipalLayerLength()
        rightprinlength=self.GetRightPrincipalLayerLength()
        length=S.shape[0]
        S[leftprinlength:length-rightprinlength,leftprinlength:length-rightprinlength]=Ss
        self.SetOverlapMatrix(S)

    def GetH_s(self):
        leftprinlength=self.GetLeftPrincipalLayerLength()
        rightprinlength=self.GetRightPrincipalLayerLength()
        length=self.GetHamiltonianMatrix().shape[0]
        return self.GetHamiltonianMatrix()[leftprinlength:length-rightprinlength,leftprinlength:length-rightprinlength]

    def GetXC_s(self):
        leftprinlength=self.GetLeftPrincipalLayerLength()
        rightprinlength=self.GetRightPrincipalLayerLength()
        length=self.GetXCMatrix().shape[0]
        return self.GetXCMatrix()[leftprinlength:length-rightprinlength,leftprinlength:length-rightprinlength]
    
    def GetS_s(self):
        leftprinlength=self.GetLeftPrincipalLayerLength()
        rightprinlength=self.GetRightPrincipalLayerLength()
        length=self.GetOverlapMatrix().shape[0]
        return self.GetOverlapMatrix()[leftprinlength:length-rightprinlength,leftprinlength:length-rightprinlength]

    # Often one wants to make sure that the supercell for the scatteringarea
    # is chosen big enough so that the effective potential in the leads is
    # converged to the value in bulk.
    # For that reason it is also possible to call the two leadhamiltonians
    # defined in the dacapocalculation of the scatteringregion.
  
    def GetH_l(self):
        leftprinlength=self.GetLeftPrincipalLayerLength()
        return self.GetHamiltonianMatrix()[0:leftprinlength,0:leftprinlength]

    def GetS_l(self):
        leftprinlength=self.GetLeftPrincipalLayerLength()
        return self.GetOverlapMatrix()[0:leftprinlength,0:leftprinlength]

    def GetH_r(self):
        rightprinlength=self.GetRightPrincipalLayerLength()
        length=self.GetHamiltonianMatrix().shape[0]
        return self.GetHamiltonianMatrix()[length-rightprinlength:length,length-rightprinlength:length]
 
    def GetS_r(self):
        rightprinlength=self.GetRightPrincipalLayerLength()
        length=self.GetHamiltonianMatrix().shape[0]
        return self.GetOverlapMatrix()[length-rightprinlength:length,length-rightprinlength:length]

    # Methods used for defining and calling the interactionhamiltonian
    # between the scatteringmolecule and the two leads.
 
    def GetT_l(self):
        leftprinlength=self.GetLeftPrincipalLayerLength()
        rightprinlength=self.GetRightPrincipalLayerLength()
        length=self.GetHamiltonianMatrix().shape[0]
        return self.GetHamiltonianMatrix()[leftprinlength:length-rightprinlength,0:leftprinlength]

    def GetInteractionS_l(self):
        leftprinlength=self.GetLeftPrincipalLayerLength()
        rightprinlength=self.GetRightPrincipalLayerLength()
        length=self.GetHamiltonianMatrix().shape[0]
        return self.GetOverlapMatrix()[leftprinlength:length-rightprinlength,0:leftprinlength]

    def GetT_r(self):
        leftprinlength=self.GetLeftPrincipalLayerLength()
        rightprinlength=self.GetRightPrincipalLayerLength()
        length=self.GetHamiltonianMatrix().shape[0]
        return self.GetHamiltonianMatrix()[leftprinlength:length-rightprinlength,length-rightprinlength:length]
    
    def GetInteractionS_r(self):
        leftprinlength=self.GetLeftPrincipalLayerLength()
        rightprinlength=self.GetRightPrincipalLayerLength()
        length=self.GetHamiltonianMatrix().shape[0]
        return self.GetOverlapMatrix()[leftprinlength:length-rightprinlength,length-rightprinlength:length]
    
