"""Utilities to measure and estimate memory"""

# The functions  _VmB, memory, resident, and stacksize are based on
# Python Cookbook, recipe number 286222
# http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/286222

import os
import resource

_proc_status = '/proc/%d/status' % os.getpid()

_scale = {'kB': 1024.0, 'mB': 1024.0*1024.0,
          'KB': 1024.0, 'MB': 1024.0*1024.0}

def _VmB(VmKey):
    """Private."""
    global _proc_status, _scale
     # get pseudo file  /proc/<pid>/status
    try:
        t = open(_proc_status)
        v = t.read()
        t.close()
        # get VmKey line e.g. 'VmRSS:  9999  kB\n ...'
        i = v.index(VmKey)
    except:
        return 0.0  # non-Linux?

    v = v[i:].split(None, 3)  # whitespace
    if len(v) < 3:
        return 0.0  # invalid format?
     # convert Vm value to bytes
    return float(v[1]) * _scale[v[2]]


def memory(since=0.0):
    """Return memory usage in bytes."""
    return _VmB('VmSize:') - since


def resident(since=0.0):
    """Return resident memory usage in bytes."""
    return _VmB('VmRSS:') - since


def stacksize(since=0.0):
    """Return stack size in bytes."""
    return _VmB('VmStk:') - since


def maxrss():
    """Return maximal resident memory size in bytes."""
    # see http://www.kernel.org/doc/man-pages/online/pages/man5/proc.5.html

    # try to get it from rusage
    mm = resource.getrusage(resource.RUSAGE_SELF)[2]*resource.getpagesize()
    if mm > 0: return mm

    # try to get it from /proc/id/status
    mm = _VmB('VmHWM:') # Peak resident set size ("high water mark")
    if mm > 0: return mm

    # try to get it from /proc/id/status
    mm = _VmB('VmRss:') # Resident set size
    if mm > 0: return mm

    # try to get it from /proc/id/status
    mm = _VmB('VmPeak:') # Peak virtual memory size
    if mm > 0: return mm

    # Finally, try to get the current usage from /proc/id/status
    mm = _VmB('VmSize:') # Virtual memory size
    if mm > 0: return mm

    # no more ideas
    return 0.0
