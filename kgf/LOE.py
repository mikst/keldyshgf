import numpy as np
from kgf.Hilbert import hilbert, hilbert_kernel_interpolate
from kgf.Tools import FermiDistribution as Fermi


def EBose(energy, temperature):
    """Energy times Bose distribution, i.e.

                  E
            ------------
            exp(E/T) - 1
    """
    if temperature < 1e-6:
        x = np.sign(energy) * 1e6
    else:
        x = energy / temperature # the eXponent
    
    if x > 70:
        return 0.0
    elif x < -70:
        return -energy
    elif abs(x) < 1e-6:
        return temperature
    else:
        return energy / (np.exp(x) - 1)


def Box(mu1, mu2, energy, temperature):
    return Fermi(energy + mu1, temperature) - Fermi(energy + mu2, temperature)


def Multiply(*args):
    result = args[0]
    for arg in args[1:]:
        result = np.dot(result, arg)
    return result


class LOE:
    """Lowest Order Expansion of the electron-phonon interaction.

    See PRB 72, 201101(R) (2005) or PRB 75, 205413 (2007).
    """
    def __init__(self,
                 scatteringgreenfunction,
                 temperature,
                 phononcoupling,
                 frequencies,
                 energyrange,
                 fermienergy=0.0):
        """Initialize necessary attributes.

        scatteringgreenfunction is any object with the methods SetEnergy,
        GetRepresentation, GetLeftLeadSelfEnergy, and GetRightLeadSelfEnergy.
        """
        self.frequencies = frequencies
        self.phononcoupling = phononcoupling
        self.temperature = temperature
        self.energyrange = energyrange
        assert energyrange[0] < -max(frequencies) - 1.0
        assert energyrange[-1] > max(frequencies) + 1.0
        assert energyrange[1] - energyrange[0] <= 0.1

        gf = scatteringgreenfunction
        gf.SetEnergy(fermienergy)

        G = gf.GetRepresentation()
        Gd = G.T.conj()
        sigmal = gf.GetLeftLeadSelfEnergy()
        sigmar = gf.GetRightLeadSelfEnergy()
        del gf
        
        Gam1 = 1.j * (sigmal - sigmal.T.conj())
        Gam2 = 1.j * (sigmar - sigmar.T.conj())
        del sigmal, sigmar

        GGam2 = Multiply(G, Gam2)
        Gam2Gd = Multiply(Gam2, Gd)
        self.ballistic = Multiply(G, Gam1, Gd, Gam2).trace().real # 1<-> 2 ??
        del Gam2
        
        GdGam1G = Multiply(Gd, Gam1, G)
        A1 = Multiply(G, Gam1, Gd)
        A2 = Multiply(G, Gam2Gd)
        del G, Gd, Gam1

        self.symmetric = []
        self.asymmetric = []
        self.hilbert = []
        ker = hilbert_kernel_interpolate(2 * len(energyrange))
        for w, M in zip(self.frequencies, self.phononcoupling):
            box = Box(w, -w, energyrange, temperature)
            hil = hilbert(box, ker=ker)
            self.hilbert.append(hil.real)

            MA1M = Multiply(M, A1, M)
            MA2M = Multiply(M, A2, M)
            MAM = MA1M + MA2M
            MA2mA1M = MA2M - MA1M
            del MA1M

            sym = np.trace(Multiply(GdGam1G, MA2M + .5j * (
                Multiply(Gam2Gd, MAM) - Multiply(MAM, GGam2))))

            asym = np.trace(Multiply(GdGam1G, (Multiply(Gam2Gd, MA2mA1M) +
                                               Multiply(MA2mA1M, GGam2))))

            self.symmetric.append(sym.real)
            self.asymmetric.append(asym.real)

    def Current(self, V):
        I = self.Ballistic(V) + self.Symmetric(V) + self.Asymmetric(V)
        return I

    def Ballistic(self, V):
        return self.ballistic * V

    def Symmetric(self, V):
        T = self.temperature
        I = 0.0
        for w, sym in zip(self.frequencies, self.symmetric):
            I += (EBose(w - V, T) - EBose(w + V, T)) * sym
        return I

    def Asymmetric(self, V):
        I = 0.0
        for asym, hil in zip(self.asymmetric, self.hilbert):
            box = Box(0, -V, self.energyrange, self.temperature)
            I += .5 * asym * np.trapz(box * hil, self.energyrange)
        return I

    def DifferentialConductance(self, bias):
        I = np.array([self.Current(V) for V in bias])

        # Differential conductance -- central difference
        dG = np.empty(len(I), float)
        dG[1:-1] = (I[2:] - I[:-2]) / (bias[2:] - bias[:-2])
        dG[0] = (I[1] - I[0]) / (bias[1] - bias[0])
        dG[-1] = (I[-1] - I[-2]) / (bias[-1] - bias[-2])

        return dG
