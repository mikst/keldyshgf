from __future__ import print_function

import numpy as np
from gpaw.mpi import world
from kgf.memory import maxrss
from time import time

def SelfConsistent(intgf, tol=0.05, mix=0.6, history=3, log=None, doslog=None):
    # At the end of each iteration, the (pulay) *mixed* interacting GF is
    # stored in ``nonintgf``, while ``intgf`` represents the latest *unmixed*
    # interacting GF.
    if world.rank == 0:
        if log is None:
            from sys import stdout
            log = stdout
        elif isinstance(log, str):
            log = open(log, 'w')
    nonintgf = intgf.GetNonIntGreenFunction()
    count = 0
    test = 1.0e7
    ret_in = []
    less_in = []
    ret_out = []
    less_out = []
   
    # Find indices defining the Keldysh interval within which we need to
    # store the lesser GFs.
    nuenergies = nonintgf.GetNUEnergyList()
    bounds = intgf.GetKeldyshBounds()
    store_keldysh = not (nuenergies[0] > (bounds[1] - 1e-5) or 
                         nuenergies[-1] < (bounds[0] + 1e-5))
    if world.rank == 0:
        print ('Store Keldysh', store_keldysh, file=log)
    t0 = time()
    while test > tol or count < 2:
        if world.rank == 0:
            print ('Memory:', maxrss() / 1024**2, 'MB per CPU', file=log)
            log.flush()
        intgf.UpdateInteractingSelfEnergies()
        intgf.UpdateRetardedAndLesserLists()
        gr_out = intgf.GetRetardedList()
        
        # Store Gl only of it lies within Keldysh interval
        if store_keldysh:
            gl_out = intgf.GetLesserList()
        
        test = np.array([np.sum(abs(nonintgf.GetRetardedList() - gr_out)),
                         np.sum(abs(gr_out))])
        world.sum(test)
        test = test[0] / test[1]
        if world.rank == 0:
            print ('Difference between retarded GFs:', test, file=log)

        ret_in.append(nonintgf.GetRetardedList())
        if store_keldysh:
            less_in.append(nonintgf.GetLesserList())
        if len(ret_in) > history:
            ret_in = ret_in[1:]
            if store_keldysh:
                less_in = less_in[1:]

        ret_out.append(gr_out)
        if store_keldysh:
            less_out.append(gl_out)
        if len(ret_out) > history:
            ret_out = ret_out[1:]
            if store_keldysh:
                less_out = less_out[1:]
                    
        # Setting up Pulay matrix
        A = np.zeros([len(ret_out), len(ret_out)], complex)
        for i in range(len(ret_out)):
            for j in range(i, len(ret_out)):
                A[i,j] = np.dot((ret_out[i].imag - ret_in[i].imag).flat,
                                (ret_out[j].imag - ret_in[j].imag).flat) + \
                                np.dot((ret_out[i].real - ret_in[i].real).flat,
                                       (ret_out[j].real - ret_in[j].real).flat)
                # matrix is symmetric
                A[j,i] = A[i,j]

        world.sum(A)
        
        A_inv = np.linalg.inv(A)
        coeff = np.sum(A_inv, axis=0) / np.sum(np.array(A_inv).flat)
        dt = time() - t0
        t0 = time()
        if world.rank == 0:
            print ('Pulay mixing coefficients', coeff, file=log)
            print ('Iteration time:', dt, file=log)
        Gr_new = (1 - mix) * coeff[0] * ret_in[0] + mix * coeff[0] * ret_out[0]
        if store_keldysh:
            Gl_new = (1 - mix) * coeff[0] * less_in[0] + \
                     mix * coeff[0] * less_out[0]
        for i in range(1, len(coeff)):
            Gr_new += (1 - mix) * coeff[i] * ret_in[i] + \
                      mix * coeff[i] * ret_out[i]
            if store_keldysh:
                Gl_new += (1 - mix) * coeff[i] * less_in[i] + \
                          mix * coeff[i] * less_out[i]

        nonintgf.SetRetardedList(Gr_new)
        if store_keldysh:
            nonintgf.SetLesserList(Gl_new)
        else:
            if nuenergies[0] > bounds[1] - 1e-5:
                nonintgf.SetLesserList(np.zeros_like(Gr_new))
            else:
                nonintgf.SetLesserList(
                    -(Gr_new - np.conj(Gr_new.swapaxes(1, 2))))

        # Control quantities 
        # NOTE: ALL processors must calculate these quantities
        tot_par = nonintgf.GetTotalParticleNumber()
        occ = nonintgf.GetOccupations()
        int_dos = nonintgf.GetIntegratedDOS()
        if world.rank == 0:
            print ('Total number of electrons:', tot_par, file=log)
            print ('Occupations:\n', occ.real, file=log)
            print ('Integrated DOS\n', int_dos.real, file=log)
            log.flush()
        if doslog is not None:
            if isinstance(doslog, str):
                dospar = dict(filename=doslog + '%i.nc' % count,
                              diagonalize=True, spectral=True)
            else:
                dospar = doslog.copy()
                dospar['filename'] = doslog['filename'] % count
            intgf.WriteSpectralInfoToNetCDFFile(**dospar)
        count += 1
    if world.rank == 0:
        print ('Number of iterations', count, file=log)
