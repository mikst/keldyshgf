import numpy as np
import pickle

from time import localtime
from sys import stdout
from gpaw import GPAW
from gpaw.mpi import world, MASTER, serial_comm
from gpaw.utilities import pack, unpack
from gpaw.utilities.blas import rk, gemm
from gpaw.coulomb import CoulombNEW as Coulomb
from ase.units import Bohr, Hartree
from gpaw.lcao.tools import tri2full


def get_linear_ramp(gd, spos_ac=None, center=None, strength=1.0,
                    direction='z'):
    """ Linear ramp external potential
        Atomic units (Bohr and Hartree)

        Parameters
        ==========
        gd: GridDescriptor
        center: {None, (3,) float} optional
            Center of linear ramp potential
        strength: float
            Electric field strength in units of Hartree / Bohr
    """
    dir_v = np.zeros(3, float)
    dir_v['xyz'.index(direction)] = 1

    if center is None:
        center = gd.cell_cv.diagonal() * 0.5
    grid_vG = (gd.get_grid_point_coordinates().T - center).T.copy()
    v_G = -np.tensordot(grid_vG, dir_v, axes=(0, 0)) * strength
    if spos_ac is None:
        return v_G
    else:
        pos_av = np.dot(spos_ac, gd.cell_cv)
        v_a = -np.dot(pos_av - center, dir_v) * strength
        vp_av = -dir_v * strength
        vp_av = vp_av.repeat(len(v_a)).reshape(3, len(v_a)).T.copy()
        return v_G, v_a, vp_av


def get_potential_matrix(calc, center=None, strength=1.0, direction='z'):
    """ Linear ramp potential matrix
        Parameters
        ==========
        calc: GPAW lcao mode calculator
        center: {None, (3,) float} optional
            Center of linear ramp potential
            If None the center of the supercell is used.

    """
    if center is not None:
        center = center / Bohr
    strength *= Bohr / Hartree
    nao = calc.wfs.eigensolver.nao
    ns = calc.wfs.nspins
    nq = len(calc.wfs.kpt_u) // ns
    v_qMM = np.zeros((nq, nao, nao), calc.wfs.dtype)
    atoms = calc.get_atoms()
    spos_ac = atoms.get_scaled_positions()
    v_G, v_a, vp_av = get_linear_ramp(calc.wfs.gd,
                                      spos_ac,
                                      center,
                                      strength,
                                      direction)
    # atomic part
    p1 = np.sqrt(4 * np.pi)
    p2 = p1 / np.sqrt(3)
    P_aqMi = calc.wfs.P_aqMi
    setups = calc.wfs.setups
    for kpt in calc.wfs.kpt_u:
        if kpt.s == 1:
            continue
        calc.wfs.basis_functions.calculate_potential_matrix(v_G,
                                                            v_qMM[kpt.q],
                                                            kpt.q)
        P_aMi = {a: P_qMi[kpt.q] for a, P_qMi in P_aqMi.items()}
        for a, P_Mi in P_aMi.items():
            Delta_iiL = setups[a].Delta_iiL
            dV0_MM = P_Mi.conj() @ Delta_iiL[:, :, 0] @ P_Mi.T
            dV0_MM = dV0_MM * v_a[a] * p1
            dV1_MM = P_Mi.conj() @ Delta_iiL[:, :, 1] @ P_Mi.T * vp_av[a, 1]
            dV1_MM += P_Mi.conj() @ Delta_iiL[:, :, 2] @ P_Mi.T * vp_av[a, 2]
            dV1_MM += P_Mi.conj() @ Delta_iiL[:, :, 3] @ P_Mi.T * vp_av[a, 0]
            dV1_MM *= p2
            v_qMM[kpt.q] += dV0_MM
            v_qMM[kpt.q] += dV1_MM
        tri2full(v_qMM[kpt.q], 'L')

    return v_qMM * Hartree


def make_pairorbs_noopt(gpwfile='grid.gpw', orbitalfile='w_wG__P_awi.pckl',
                        pairorbfile='g_qG__P_aqp.pckl'):
    assert world.size == 1, 'Serial only.'
    calc = GPAW(gpwfile, txt=None)
    gd = calc.gd
    # setups = calc.wfs.setups
    del calc

    w_wG, P_awi = pickle.load(open(orbitalfile))
    Nw = len(w_wG)
    f_pG = gd.zeros(n=Nw**2)
    for p, (w1, w2) in enumerate(np.ndindex(Nw, Nw)):
        np.multiply(w_wG[w1], w_wG[w2], f_pG[p])
    P_app = dict([(a, np.array([
        pack(np.outer(P_wi[w1], P_wi[w2]), tolerance=1e3)
        for w1, w2 in np.ndindex(Nw, Nw)])) for a, P_wi in P_awi.items()])

    pickle.dump((f_pG, P_app), open(pairorbfile, 'wb'), 2)


def makeV_from_pairorbfile(gpwfile='grid.gpw', pairorbfile='g_qG__P_aqp.pckl',
                           coulombfile='V_qq.pckl'):
    calc = GPAW(gpwfile, txt=None, communicator=serial_comm)
    atoms = calc.get_atoms()
    spos_ac = atoms.get_scaled_positions() % 1.0
    coulomb = Coulomb(calc.gd, calc.wfs.setups, spos_ac)
    g_qG, P_aqp = pickle.load(open(pairorbfile))
    del calc

    Nq = len(g_qG)
    qq_i = [(q1, q2) for q1 in range(Nq) for q2 in range(q1, Nq)]

    Ntot = len(qq_i)
    V_i = np.zeros(Ntot)
    Nproc = int(round(Ntot / float(world.size)))
    istart = world.rank * Nproc
    iend = istart + Nproc
    if world.rank == world.size - 1:
        iend = Ntot
    for i in range(istart, iend):
        q1, q2 = qq_i[i]
        if world.rank == MASTER:
            T = localtime()
            done = i / float(Nproc) * 100
            print('%2.0f percent done at %02i:%02i:%02i' %
                  (done, T[3], T[4], T[5]))
            stdout.flush()

        P1_ap = dict([(a, P_qp[q1]) for a, P_qp in P_aqp.items()])
        P2_ap = dict([(a, P_qp[q2]) for a, P_qp in P_aqp.items()])
        V_i[i] = coulomb.calculate(g_qG[q1], g_qG[q2], P1_ap, P2_ap)

    world.sum(V_i, MASTER)
    if world.rank == MASTER:
        V_qq = unpack(V_i)
        V_qq.dump(coulombfile)


def makeU_simple(gpwfile='grid.gpw', orbitalfile='w_wG__P_awi.pckl',
                 rotationfile='eps_q__U_pq.pckl', tolerance=1e-5,
                 writeoptimizedpairs=False):
    # Tolerance is used for truncation of optimized pairorbitals
    calc = GPAW(gpwfile, txt=None)
    gd = calc.gd
    setups = calc.wfs.setups
    del calc

    # Load orbitals on master and distribute to slaves
    if world.rank == MASTER:
        wglobal_wG, P_awi = pickle.load(open(orbitalfile))
        Nw = len(wglobal_wG)
    else:
        wglobal_wG = None
        Nw = 0
    Nw = gd.comm.sum(Nw)
    w_wG = gd.empty(n=Nw)
    gd.distribute(wglobal_wG, w_wG)
    del wglobal_wG

    # Make pairorbitals
    f_pG = gd.zeros(n=Nw**2)
    for p, (w1, w2) in enumerate(np.ndindex(Nw, Nw)):
        np.multiply(w_wG[w1], w_wG[w2], f_pG[p])
    del w_wG

    # Make pairorbital overlap (lower triangle only)
    D_pp = np.zeros((Nw**2, Nw**2))
    rk(gd.dv, f_pG, 0., D_pp)
    gd.comm.sum(D_pp, MASTER)

    if world.rank == MASTER:
        # Add atomic corrections to pairorbital overlap
        for a, P_wi in P_awi.items():
            P_pp = np.array([pack(np.outer(P_wi[w1], P_wi[w2]), tolerance=1e3)
                             for w1, w2 in np.ndindex(Nw, Nw)])
            I4_pp = setups[a].four_phi_integrals()
            D_pp += np.dot(P_pp, np.dot(I4_pp, P_pp.T))

        # Determine eigenvalues and vectors
        eps_q, U_pq = np.linalg.eigh(D_pp, UPLO='L')
        del D_pp
        indices = np.argsort(-eps_q.real)
        eps_q = eps_q.real[indices]
        U_pq = U_pq[:, indices]

        # Truncate
        indices = eps_q > tolerance
        U_pq = U_pq[:, indices]
        eps_q = eps_q[indices]

        # Dump to file
        pickle.dump((eps_q, U_pq), open(rotationfile, 'wb'), 2)

    if writeoptimizedpairs is not False:
        assert world.size == 1  # works in parallel if U and eps are broadcast
        Uisq_qp = (U_pq / np.sqrt(eps_q)).T.copy()
        g_qG = gd.zeros(n=len(eps_q))
        gemm(1.0, f_pG, Uisq_qp, 0.0, g_qG)
        g_qG = gd.collect(g_qG)
        if world.rank == MASTER:
            P_app = dict([(a, np.array([pack(np.outer(P_wi[w1], P_wi[w2]),
                                             tolerance=1e3)
                                        for w1, w2 in np.ndindex(Nw, Nw)]))
                          for a, P_wi in P_awi.items()])
            P_aqp = dict([(a, np.dot(Uisq_qp, P_pp))
                          for a, P_pp in P_app.items()])
            pickle.dump((g_qG, P_aqp), open(writeoptimizedpairs, 'wb'), 2)


def makeU(gpwfile='grid.gpw', orbitalfile='w_wG__P_awi.pckl',
          rotationfile='eps_q__U_pq.pckl', tolerance=1e-5):
    # Tolerance is used for truncation of optimized pairorbitals
    calc = GPAW(gpwfile, txt=None)
    gd = calc.gd
    setups = calc.wfs.setups
    del calc

    # Load orbitals on master and distribute to slaves
    if world.rank == MASTER:
        wglobal_wG, P_awi = pickle.load(open(orbitalfile))
        Nw = len(wglobal_wG)
    else:
        wglobal_wG = None
        Nw = 0
    Nw = gd.comm.sum(Nw)
    w_wG = gd.empty(n=Nw)
    gd.distribute(wglobal_wG, w_wG)
    del wglobal_wG

    # unpack from compressed to wannier-wannier indices
    ww_c = [(w1, w2) for w1 in range(Nw) for w2 in range(w1, Nw)]
    Nc = Nw * (Nw + 1) // 2  # Number of compressed pair orbital indices

    # Make pairorbitals
    f_cG = gd.zeros(n=Nc)
    for f_G, (w1, w2) in zip(f_cG, ww_c):
        np.multiply(w_wG[w1], w_wG[w2], f_G)
    del w_wG

    # Make pairorbital overlap (lower triangle only)
    D_cc = np.zeros((Nc, Nc))
    rk(gd.dv, f_cG, 0., D_cc)
    gd.comm.sum(D_cc, MASTER)

    if world.rank == MASTER:
        # Add atomic corrections to pairorbital overlap
        for a, P_wi in P_awi.items():
            P_cp = np.array([pack(np.outer(P_wi[w1], P_wi[w2]), tolerance=1e3)
                             for w1, w2 in ww_c])
            I4_pp = setups[a].four_phi_integrals()
            D_cc += np.dot(P_cp, np.dot(I4_pp, P_cp.T))

        # Determine eigenvalues and vectors
        eps_q, U_cq = np.linalg.eigh(D_cc, UPLO='L')
        del D_cc
        indices = np.argsort(-eps_q.real)
        eps_q = eps_q.real[indices]
        U_cq = U_cq[:, indices]

        # Truncate
        indices = eps_q > tolerance
        U_cq = U_cq[:, indices]
        eps_q = eps_q[indices]
        Nq = len(eps_q)

        # Make rotation matrices and unpack the reverse rotation from
        # compressed format
        U_pq = np.empty((Nw**2, Nq))
        for c, U_q in enumerate(U_cq):
            i1 = int(Nw + .5 - np.sqrt((Nw - .5)**2 - 2 * (c - Nw)))
            i2 = c - i1 * (2 * Nw - 1 - i1) // 2
            U_pq[i1 + i2 * Nw] = U_pq[i2 + i1 * Nw] = U_q
        pickle.dump((eps_q, U_pq), open(rotationfile, 'wb'), 2)


def makeV(gpwfile='grid.gpw', orbitalfile='w_wG__P_awi.pckl',
          rotationfile='eps_q__U_pq.pckl', coulombfile='V_qq.pckl',
          log=stdout):

    if isinstance(log, str) and world.rank == MASTER:
        log = open(log, 'w')

    # Extract data from files
    calc = GPAW(gpwfile, txt=None, communicator=serial_comm)
    spos_ac = calc.get_atoms().get_scaled_positions() % 1.0
    coulomb = Coulomb(calc.gd, calc.wfs.setups, spos_ac)
    w_wG, P_awi = pickle.load(open(orbitalfile))
    eps_q, U_pq = pickle.load(open(rotationfile))
    del calc

    # Make rotation matrix divided by sqrt of norm
    Nq = len(eps_q)
    # Np = len(U_pq)
    Ni = len(w_wG)
    Uisq_qp = (U_pq / np.sqrt(eps_q)).T.copy()
    Uisq_qij = Uisq_qp.reshape(Nq, Ni, Ni)
    del eps_q, U_pq

    # Determine number of opt. pairorb on each cpu
    Ncpu = world.size
    nq, R = divmod(Nq, Ncpu)
    nq_r = nq * np.ones(Ncpu, int)
    if R > 0:
        nq_r[-R:] += 1

    # Determine number of opt. pairorb on this cpu
    nq1 = nq_r[world.rank]
    q1end = nq_r[:world.rank + 1].sum()
    q1start = q1end - nq1
    V_qq = np.zeros((Nq, Nq), float)

    def make_optimized(qstart, qend):
        g_qG = np.zeros((qend - qstart,) + w_wG.shape[1:], float)
        for w1, w1_G in enumerate(w_wG):
            gemm(1., w_wG * w1_G, Uisq_qij[qstart: qend, w1, :].copy(),
                 1.0, g_qG)
        P_aqp = {}
        for a, P_wi in P_awi.items():
            P_aqp[a] = np.dot(Uisq_qp[qstart: qend],
                              np.array([pack(np.outer(P_wi[w1], P_wi[w2]),
                                             tolerance=1e3)
                                        for w1, w2 in np.ndindex(Ni, Ni)]))
        return g_qG, P_aqp

    g1_qG, P1_aqp = make_optimized(q1start, q1end)
    for block, nq2 in enumerate(nq_r):
        if block == world.rank:
            g2_qG, P2_aqp = g1_qG, P1_aqp
            q2start, q2end = q1start, q1end
        else:
            q2end = nq_r[:block + 1].sum()
            q2start = q2end - nq2
            g2_qG, P2_aqp = make_optimized(q2start, q2end)

        for q1, q2 in np.ndindex(nq1, nq2):
            P1_ap = dict([(a, P_qp[q1]) for a, P_qp in P1_aqp.items()])
            P2_ap = dict([(a, P_qp[q2]) for a, P_qp in P2_aqp.items()])
            V_qq[q1 + q1start,
                 q2 + q2start] = coulomb.calculate(g1_qG[q1], g2_qG[q2],
                                                   P1_ap, P2_ap)
            if q2 == 0 and world.rank == MASTER:
                T = localtime()
                log.write('Block %i/%i is %4.1f percent done' +
                          ' at %02i:%02i:%02i\n' % (block, world.size,
                                                    100.0 * q1 / nq1,
                                                    T[3], T[4], T[5]))
                log.flush()

    world.sum(V_qq, MASTER)
    if world.rank == MASTER:
        # V can be slightly asymmetric due to numerics
        V_qq = 0.5 * (V_qq + V_qq.T)
        V_qq.dump(coulombfile)
