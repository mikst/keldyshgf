import sys
import numpy as np
from ase.io.pupynere import NetCDFFile 
#from Scientific.IO.NetCDF import NetCDFFile
#from Dacapo import NetCDF

level_set = []
eta_set = []
for filename in sys.argv[1:]:
    nc = NetCDFFile(filename,'r')
    eta = nc.variables['Imaginary'][0]
    orb = nc.variables['Orbitals'][:]
    dos = nc.variables['tot_dos'][:]
    energies = nc.variables['Energies'][:]
    nc.close()
    #eta = NetCDF.Entry(name='Imaginary').ReadFromNetCDFFile(file).GetValue()[0]
    #orb = NetCDF.Entry(name='Orbitals').ReadFromNetCDFFile(file).GetValue()
    #dos = NetCDF.Entry(name='tot_dos').ReadFromNetCDFFile(file).GetValue()
    #energies = NetCDF.Entry(name='Energies').ReadFromNetCDFFile(file).GetValue()

    levels = []
    de = energies[1] - energies[0]
    for n in orb:
        m = np.max(dos)
        index = np.argmax(dos)
        en = energies[index]
        m_left, m_right = dos[index-1], dos[index+1]
        if m_left>m_right:
        # Maximum is to the left of m
            t1 = m_left - m_right
            t2 = m - m_left
            x0 = en - 0.5 * de * (t1 / (t2 + t1))  
        else:
            t1 = m_right - m_left
            t2 = m - m_right
            x0 = en + 0.5 * de * (t1 / (t2 + t1))
        levels.append(x0)
        lorentz = eta / ((energies - x0)**2 + eta**2)
        dos = dos - lorentz

    levels = np.array(levels)
    levels = np.sort(levels)
    level_set.append(levels.copy())
    eta_set.append(eta)

if len(sys.argv)>2:
    a = (level_set[1] - level_set[0])/(eta_set[1] - eta_set[0])
    if eta_set[0]<eta_set[1]:
        b = level_set[0] - a * eta_set[0]
    else:
        b = level_set[1] - a * eta_set[1]
    print "Level positions extrapolated to eta = 0:"
    print b
else:
    print "Level positions:"
    print levels
